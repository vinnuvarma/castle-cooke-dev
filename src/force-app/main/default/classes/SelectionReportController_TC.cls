@IsTest
public class SelectionReportController_TC
{
    @TestSetup
    public static void TestData(){
         Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Homebuyer').getRecordTypeId();
       
       Account acc = new Account(LastName ='Test Account',FirstName = 't',personEmail = 'testAccount@gmail.com',RecordTypeId = devRecordTypeId );
       insert acc;
        
        List<Division__c> div = TestDataUtil.divisionCreation(1);
        div[0].name = 'test division';
        Insert div;
       
        List<Community__c> com = TestDataUtil.communityCreation(1);
        com[0].name = 'test community';
        com[0].Division__c = div[0].Id;
        Insert com;
          
        list<Opportunity> opp = TestDataUtil.opportunitiesCreation(1);
        opp[0].Name = 'Spec Connection';opp[0].Community__c = com[0].Id; opp[0].AccountId=acc.Id;
        Insert opp;
        
        Product2 modelProduct = TestDataUtil.modelProductCreation();
        Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry pb = [Select id from PricebookEntry where Product2Id =: modelProduct.Id and PriceBook2Id =: pricebookId];
        system.debug('test01'+pb);
        
        Product2 lotProduct = TestDataUtil.lotProductCreation();
        Id LotRecordType = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Lot').getRecordTypeId();
        Product2 LotProduct1 = New Product2(Name='ASC', Productcode='Lot', recordtypeId = LotRecordType, Community__c = com[0].id, IsActive = True, Status__c = 'Open');
        Insert LotProduct1;
    
        List<Quote> quot  = TestDataUtil.quotesCreation(1);
        quot[0].Name = 'Test';
        quot[0].OpportunityId = opp[0].id;
        quot[0].Community__c = com[0].Id;
        quot[0].Division__c = div[0].Id; 
        //quot[0].Cancel_Reason__c = 'Convert to Spec';        
        Insert quot;
        
        List<quotelineitem> quotline = TestDataUtil.quoteslineCreation(1);
        quotline[0].QuoteId = quot[0].id;
        quotline[0].PricebookEntryId = pb.Id;
        quotline[0].Type__c = 'Model Options';
        quotline[0].Quantity = 35;
        quotline[0].UnitPrice = 34;
        quotline[0].Cancelled__c = false;       //quotline[0].Group__r.Name= 'Test';
        insert quotline;
   
    }
    
 public static testmethod void SelectionReportControllerOne()
 {
      Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Homebuyer').getRecordTypeId();
       
       Account acc = new Account(LastName ='Test Account',FirstName = 't',personEmail = 'testAccount@gmail.com',RecordTypeId = devRecordTypeId );
       insert acc;

      
    Product2 modelProduct = TestDataUtil.modelProductCreation();
    Id pricebookId = Test.getStandardPricebookId();
    PricebookEntry pb = [Select id from PricebookEntry where Product2Id =: modelProduct.Id and PriceBook2Id =: pricebookId];
      
    List<Division__c> div = TestDataUtil.divisionCreation(1);
        div[0].name = 'test division';
        Insert div;
       
        List<Community__c> com = TestDataUtil.communityCreation(1);
        com[0].name = 'test community';
        com[0].Division__c = div[0].Id;
        Insert com;
          
        list<Opportunity> opp = TestDataUtil.opportunitiesCreation(1);
        opp[0].Name = 'Spec Connection';opp[0].Division__c = div[0].Id; opp[0].Community__c = com[0].Id; opp[0].AccountId=acc.Id;
        Insert opp;
      
    List<Quote> quot  = TestDataUtil.quotesCreation(1);
        quot[0].Name = 'Test';
        quot[0].OpportunityId = opp[0].id;
        quot[0].Community__c = com[0].Id;
        quot[0].Division__c = div[0].Id; 
        //quot[0].Cancel_Reason__c = 'Convert to Spec';        
        Insert quot;
       
    List<quotelineitem> quotline1 = TestDataUtil.quoteslineCreation(1);
        quotline1[0].QuoteId = quot[0].id;
        quotline1[0].PricebookEntryId = pb.Id;
        quotline1[0].Type__c = 'Model Options';
        quotline1[0].Quantity = 35;
        quotline1[0].UnitPrice = 34;
        quotline1[0].Cancelled__c = true;
        insert quotline1;
     
    Quote_Group__c GroupLine1 = New Quote_Group__c();
    GroupLine1.Name = '--';
    Insert GroupLine1;  
    
    List<quotelineitem> quotline12 = TestDataUtil.quoteslineCreation(1);
        quotline12[0].QuoteId = quot[0].id;
        quotline12[0].PricebookEntryId = pb.Id;
        quotline12[0].Type__c = 'Model Options';
        quotline12[0].Quantity = 35;
        quotline12[0].UnitPrice = 34;
        quotline12[0].Cancelled__c = false;
        quotline12[0].Group__c=GroupLine1.id;
        insert quotline12;
   
    List<quotelineitem> quotline13 = TestDataUtil.quoteslineCreation(1);
        quotline13[0].QuoteId = quot[0].id;
        quotline13[0].PricebookEntryId = pb.Id;
        quotline13[0].Type__c = 'Model Options';
        quotline13[0].Quantity = 35;
        quotline13[0].UnitPrice = 34;
        quotline13[0].Cancelled__c = false;
        quotline13[0].Group__c=null;
        insert quotline13;
     
   Quote_Group__c GroupLine = New Quote_Group__c();
   GroupLine.Name = 'Group1';
   Insert GroupLine;  
     
   List<quotelineitem> quotline14 = TestDataUtil.quoteslineCreation(1);
        quotline14[0].QuoteId = quot[0].id;
        quotline14[0].PricebookEntryId = pb.Id;
        quotline14[0].Type__c = 'Model Options';
        quotline14[0].Quantity = 35;
        quotline14[0].UnitPrice = 34;
        quotline14[0].Cancelled__c = false;
        quotline14[0].Group__c=GroupLine.id;
        insert quotline14;
   
    ApexPages.currentPage().getParameters().put('qid', quot[0].id);
    SelectionReportController srcon1 = New SelectionReportController();
    quotline1[0].Group__c = GroupLine.Id;
    Update quotline1[0];   
   
    SelectionReportController srcon = New SelectionReportController(); 
 }
 
}