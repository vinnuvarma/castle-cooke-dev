@isTest public class PrintScenarioEmailControllerTest{
    @testSetup static void insertData(){
       // Id Agencyid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Agency').getRecordTypeId();
        //List<Account> acc=TestDataUtil.PersonAccountCreation(1);
        //acc[0].RecordTypeId=Agencyid;
       // insert acc;
        
       // Id Hbrecordtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Homebuyer').getRecordTypeId();
        List<Account> con=TestDataUtil.PersonAccountCreation(1);
        //con[0].Type__c='Lead';        
        insert con;
        
        List<Division__c> divlist = TestDataUtil.divisionCreation(1);
        insert divlist;
        
        List<Community__c> comlist = TestDataUtil.communityCreation(2);
        comlist[0].Division__c = divlist[0].Id;
        insert comlist;
        
        Pricebook2 pb = new Pricebook2();
        pb.Name = 'Dheeraj';
        insert pb;
        
        List<Opportunity> conlist = TestDataUtil.opportunitiesCreation(1);
        conlist[0].AccountId=con[0].id;
        conlist[0].StageName = 'Lead';
        conlist[0].CloseDate = System.today();
        conlist[0].Division__c=divlist[0].Id;
        conlist[0].Community__c=comlist[0].Id;
        conlist[0].Prospect_Rating__c='A'; 
        conlist[0].Pricebook2Id = pb.id;
        insert conlist; 
        
        Id RegScenarioId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Regular').getRecordTypeId();
        List<Quote> scenario= TestDataUtil.quotesCreation(1);
        scenario[0].OpportunityId = conlist[0].id;
        scenario[0].Community__c = comlist[0].Id;
        Scenario[0].RecordTypeId = RegScenarioId;
        Scenario[0].pricebook2id = pb.Id;
        scenario[0].Division__c = divlist[0].Id;
        insert scenario;
        
        //Id recordtypeidlot = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Lot').getRecordTypeId();
        Product2 lot = TestDataUtil.lotProductCreation();
        lot.Name = 'lot';
        lot.Community__c = comlist[0].Id;
        update lot;  
        
        PricebookEntry pbe = new PricebookEntry();
        pbe.Product2Id = lot.id;
        pbe.UnitPrice = 100;
        pbe.Pricebook2Id = pb.id;
        pbe.UseStandardPrice=false;
        pbe.isActive = true;
        insert pbe;
        
        QuoteLineItem qli=new QuoteLineItem();
        qli.UnitPrice=20;
        qli.Quantity=10;
        qli.QuoteId=scenario[0].id;
        qli.type__c='lot';
        qli.PriceBookEntryID=pbe.id;
        insert qli;         
        
         //Id recordtypeidlot = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Lot').getRecordTypeId();
        Product2 model = TestDataUtil.modelProductCreation();
        model.Name='model';
        model.Community__c = comlist[0].Id;
        update model;
        
        Pricebook2 pb1 = new Pricebook2();
        pb1.Name = 'Dheeraj';
        insert pb1;
        
        PricebookEntry pbe1 = new PricebookEntry();
        pbe1.Product2Id = model.id;
        pbe1.UnitPrice = 100;
        pbe1.Pricebook2Id = pb1.id;
        pbe1.UseStandardPrice=false;
        pbe1.isActive = true;
        insert pbe1;
        
        QuoteLineItem qli1=new QuoteLineItem();
        qli1.UnitPrice=20;
        qli1.Quantity=10;
        qli1.QuoteId=scenario[0].id;
        qli1.type__c='Model';
        qli1.PriceBookEntryID=pbe.id;
        insert qli1;
        
            }
    @isTest static void testData(){
        List<Quote> scen = new List<Quote>([select id,community__c,OpportunityID,division__c,RecordTypeId,Cancel_Reason__c from Quote]);
        PageReference pageRef = Page.PrintScenarioEmail; // Add your VF page Name here
        pageRef.getParameters().put('id', String.valueOf(scen[0].id));
        Test.setCurrentPage(pageRef);
            //String t = Label.printscenarioemail;
       // system.assertNotEquals('/apex/'+t+'?id='+scen[0].id, '');
        PrintScenarioEmailController peqc = new PrintScenarioEmailController();
        peqc.sendEmail();
    }
}