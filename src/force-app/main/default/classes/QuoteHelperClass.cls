public Class QuoteHelperClass
{
    //To update the standard PriceBook Id
    public Static void quotePriceBookUpdate(List<Quote> Quotes)
    {
        Pricebook2 PriceBook = New Pricebook2();
        if(!Test.isRunningTest())
            PriceBook = [SELECT id FROM Pricebook2 WHERE IsActive = true AND IsStandard = true];
        else
            PriceBook.id =  Test.getStandardPricebookId();
        List<Opportunity> opptyRecords = new List<Opportunity>();
        for(Quote Qt: Quotes){
            if(Qt.PriceBook2Id == Null)
                Qt.PriceBook2Id = PriceBook.Id;
            if(qt.OpportunityId <> null) opptyRecords.add(new Opportunity(Id = qt.OpportunityId, Quote_Created__c = True));
        }
        system.debug('opptyRecords::::@@'+opptyRecords);
        if(!opptyRecords.isEmpty())
            Update opptyRecords;
    }
    //Purpose: To update the quote as sync quote on opportunity, when quote is approved 
    public Static void quoteSyncUpdate(List<Quote> Quotes, Map<Id,Quote> OldQuotes)
    {
        Id SpecRecordType = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Spec Quote').getRecordTypeId();
        Map<Id,Id> SyncMap = New Map<Id,Id>();
        List<Product2> lotProducts = new List<Product2>();
        for(Quote Qt: Quotes){
            if(Qt.Status == 'Approved' && Qt.OpportunityId <> Null && OldQuotes.ContainsKey(Qt.Id) && OldQuotes.get(Qt.Id).Status <> 'Approved'){
                if(Qt.RecordTypeId <> SpecRecordType){
                    SyncMap.Put(Qt.OpportunityId, Qt.Id); 
                    lotProducts.add(new Product2(Id = qt.Lot__c, Status__c = 'Closed'));   
                } 
                else{
                    lotProducts.add(new Product2(Id = qt.Lot__c, Status__c = 'Spec'));
                }   
            } 
        }
        if(!lotProducts.isEmpty()) update lotProducts;
        if(!SyncMap.IsEmpty())
            ID jobID = System.enqueueJob(new SyncQuotWithOpportunity(SyncMap));
    }
    public Static void CancelOldQuotes(Map<Id, Quote> newQuotes,Map<Id, Quote> oldQuotes)
    {
        Set<Id> oppIds=new set<Id>();   
        Id SpecRecordType = RecordTypeUtil.scenariospecrecordtypename();
        for(Quote Qt : newQuotes.values()){
            if(Qt.Status=='Approved' && Qt.Status <> oldQuotes.get(Qt.Id).Status)
                oppIds.add(Qt.Opportunityid);  
        }
        List<Opportunity> updateCon=new List<Opportunity>();
        List<Quote> updateScenario=new List<Quote>();
     List<Opportunity> opp=[Select Id,Name,Primary_Quote__c,(Select Id,Name,Status,Cancelled__c,RecordTypeId from Quotes where RecordTypeId!=:SpecRecordType) from Opportunity where Id IN:oppIds];
        for(Opportunity c:opp){
            for(Quote sc:c.Quotes){
                if(sc.Status!='Approved'){
                    sc.Status='Cancelled';
                    sc.Cancelled__c=true;
                    updateScenario.add(sc); 
                }   
                else if(sc.Status=='Approved'){
                    c.Primary_Quote__c=sc.Id;
                    updateCon.add(c);
                }
            }
        }
        update updateScenario;
        update updateCon;
    }
    
}