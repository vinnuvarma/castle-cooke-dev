public class TestDataUtil
{
    public static User createTestUser(Id roleId, Id profID, String fName, String lName)
    {
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
       
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        User tuser = new User(  firstname = fName,
                                lastName = lName,
                                email = uniqueName + '@test' + orgId + '.org',
                                Username = uniqueName + '@test' + orgId + '.org',
                                EmailEncodingKey = 'ISO-8859-1',
                                Alias = uniqueName.substring(18, 23),
                                TimeZoneSidKey = 'America/Los_Angeles',
                                LocaleSidKey = 'en_US',
                                LanguageLocaleKey = 'en_US',
                                ProfileId = profId,
                                UserRoleId = roleId);
        return tuser;
    }
    //Accounts test data
    public static List<Account> accountsCreation(Integer numOfRecords)
    {
        List<Account> accts = new List<Account>();
        for(Integer i=0; i<numOfRecords; i++) {
            accts.add(new Account(Name='TestAccount' + i));
        }
        return accts;
    }
     public static List<Account> PersonAccountCreation(Integer numOfRecords)
    {
       ID personrecordtype=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Homebuyer').getRecordTypeId();
        List<Account> accts = new List<Account>();
        for(Integer i=0; i<numOfRecords; i++) {
            accts.add(new Account(LastName='TestAccount' + i,PersonEmail='PersonTest'+i+'@gmail.com',RecordTypeid=personrecordtype));
             //Id PersonAccountType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Homebuyer').getRecordTypeId();
        }
        return accts;
    }
    public static list<Account> createAccounts(Integer numAccts, string recordType) {
        List<Account> accts = new List<Account>();        
        for(Integer i=0;i<numAccts;i++) {
            Account a = new Account(Name='Test Account' + i, recordTypeId= string.isNotBlank(recordType) ? recordType : '');
            accts.add(a);
        }       
        return accts;
    }
    //Contacts test data
    public static List<Contact> contactsCreation(Integer numOfRecords)
    {
        List<Contact> Conts = new List<Contact>();
        for(Integer i=0; i<numOfRecords; i++) {
            Conts.add(new Contact(lastName='Test'+i, FirstName = 'Contact'+i, email = 'test'+i+'@gmail.com'));
        }
        return Conts;
    } 
    //opportunities test data
    
    //Community test data
    public static List<Community__c> communityCreation(Integer numOfRecords)
    {
        List<Community__c> com = new List<Community__c>();
        for(Integer i=0; i<numOfRecords; i++) {
            com.add(new Community__c(Name='Test Community'+i,Active__c=true));
        }
        return com;
    }
    //Division test data
    public static List<Division__c> divisionCreation(Integer numOfRecords)
    {
        List<Division__c> div = new List<Division__c>();
        for(Integer i=0; i<numOfRecords; i++) {
            div.add(new Division__c(Name='Test Division'+i));
        }
        return div;
    }
    
    public static List<Opportunity> opportunitiesCreation(Integer numOfRecords)
    {    Division__c div=new Division__c();
         div.Name='Test Division123';
         insert div; 
          
         Community__c com=new Community__c();
         com.Name='Test Community123'; 
         com.Active__c=true; 
         insert com;
        
        //List<Division__c> div=TestDataUtil.divisionCreation(1);
       // List<Community__c> comm=TestDataUtil.communityCreation(1);
        List<Opportunity> Opps= new List<Opportunity>();
        for(Integer i=0; i<numOfRecords; i++) {
           opportunity op=new opportunity();
           op.Name='Test'+i;
           op.StageName = 'Prospect';
           op.CloseDate = System.Today();
           op.Division__c =div.id;
           op.Community__c=com.id;
           Opps.add(op);
          //  Opps.add(new Opportunity(Name='Test'+i, StageName = 'Prospect', CloseDate = System.Today(),Division__c =TestDataUtil.divisionCreation(1)));
        }
        return Opps;
    }
    
    //Quotes test data
    public static List<Quote> quotesCreation(Integer numOfRecords)
    {
        List<Quote> Quotes = new List<Quote>();
        for(Integer i=0; i<numOfRecords; i++) {
            Quotes.add(new Quote(Name='Test Quote'));
        }
        return Quotes;
    } 
    
    //Quote Line Helper
    public static List<QuoteLineItem> quoteslineCreation(Integer numOfRecords)
    {
        List<QuoteLineItem> Quotesline = new List<QuoteLineItem>();
        for(Integer i=0; i<numOfRecords; i++) {
            Quotesline.add(new QuoteLineItem());
        }
        return Quotesline;
    }
    
    //Model Products
    public static Product2 modelProductCreation()
    {
        //Model Product
        Id ModelRecordType = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Model').getRecordTypeId();
        Product2 ModelProduct = New Product2(Name='Model Products', Productcode='Model', recordtypeId = ModelRecordType, IsActive = True);
        Insert ModelProduct;
        return ModelProduct;
    }
    public static Product2 lotProductCreation()
    {
        //Model Product
        Id LotRecordType = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Lot').getRecordTypeId();
        Product2 LotProduct = New Product2(Name='Lot Products', Productcode='Lot', recordtypeId = LotRecordType, IsActive = True);
        Insert LotProduct;
        return LotProduct;
    }
    public static List<Product2> optionProductCreation(Integer numOfRecords)
    {
        //Model Product
        Id OptionRecordType = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Options').getRecordTypeId();
        List<Product2> OptionProducts = New List<Product2>();
        for(Integer i=0; i<numOfRecords; i++) {
            OptionProducts.add(New Product2(Name='Option Products'+i, Productcode='Options'+i, recordtypeId = OptionRecordType, Price__c = 100+i, IsActive = True));
        }
        Insert OptionProducts;
        return OptionProducts;
    }
    public static List<Product2> modelOptionCreation(Integer numOfRecords)
    {
        //Model Product
        Id modelOptionRecordType = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Model Options').getRecordTypeId();
        List<Product2> modelOptionProducts = New List<Product2>();
        for(Integer i=0; i<numOfRecords; i++) {
            modelOptionProducts.add(New Product2(Name='Option Products'+i, Productcode='Options'+i, recordtypeId = modelOptionRecordType, Price__c = 100+i, IsActive = True));
        }
        Insert modelOptionProducts;
        return modelOptionProducts;
    }
 /*
    public static List<Registration_Card__c> createReg(Integer numOfReg, String accId, String optId, String conId){
        List<Registration_Card__c> reg = new List<Registration_Card__c>();            
        for(Integer i=0;i<numOfReg;i++){
            Registration_Card__c card = new Registration_Card__c(
            Connection__c = String.isNotBlank(optId) ? optId : null,
            Homebuyer__c = String.isNotBlank(conId) ? conId : null,
            How_did_you_hear_about_us__c = 'Email/Eblast' ,
            I_plan_to_purchase_a_home__c = 'Less than 30 days' );
            reg.add(card);
            }
            return reg;        
        }*/
   
   public static List<Sales__c> Createsales(Id Userid)
    {
        List<Sales__c> sasles = new List<Sales__c>();
        sasles.add(new Sales__c(User__c = Userid));
        return sasles;
    } 
}