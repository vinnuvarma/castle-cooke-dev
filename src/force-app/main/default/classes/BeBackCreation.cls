/*
Description: Beback creation and validations
*/
/*Public Class BeBackCreation
{
    @AuraEnabled
    Public Static string createBeBack(String RecordId)
    {
    	try
        {
            Id RegRecordType=RecordTypeUtil.trafficRegisteredRecordTypeName();
            Opportunity connection = New Opportunity();
            connection = [Select Id, Community__c, (Select Id, CreatedDate from Registration_Cards1__r limit 1), (select Id from Traffic__r where RecordType.DeveloperName = 'BeBack' and createdDate = Today) from Opportunity where Id =: RecordId];
            if(!connection.Registration_Cards1__r.IsEmpty())
            {
                Registration_Card__c RegCard = New Registration_Card__c();
                RegCard = connection.Registration_Cards1__r[0];
                if(connection.Traffic__r.IsEmpty() && RegCard.CreatedDate.date() <> system.Today())
                {
                    Traffic__c Beback = New Traffic__c();
                    Beback.Community__c = connection.Community__c;
                    Beback.Connection__c = connection.Id;
                    Beback.Count__c = 1;
                    Beback.RecordTypeId = Schema.SObjectType.Traffic__c.getRecordTypeInfosByDeveloperName().get('BeBack').getRecordTypeId();
                    Insert Beback;
                    return 'Success';
                }
                else
                {    
                    if(RegCard.CreatedDate.date() == system.Today())
                        return 'Sorry, you can\'t create RegCard and BeBack on same day';
                    else
                    	return 'Sorry, already BeBack has been created for today';
                }    
            }
            else
                return 'Sorry, you are not allowed to create a BeBack with out Registration Card';
        }
        catch(exception e)
        {
            return e.getMessage();
        }
    }
} */
/*
Description: Beback creation and validations
*/
Public without sharing Class BeBackCreation
{
 @AuraEnabled
    Public Static string createBeBack(String RecordId)
    {
        try
        {
            Id RegRecordType=RecordTypeUtil.trafficRegisteredRecordTypeName();
            Opportunity connection = New Opportunity();
            connection = [Select Id, Community__c, (select Id, Createddate,Date__c from Traffic__r where RecordTypeId =: RegRecordType) from Opportunity where Id =: RecordId];
            if(!connection.Traffic__r.IsEmpty())
            {
                Traffic__c RegCard = New Traffic__c();
                RegCard = connection.Traffic__r[0];
                Opportunity connection1 = New Opportunity();
            connection1 = [Select Id, Community__c, (select Id from Traffic__r where RecordType.DeveloperName = 'Beback' and Date__c = Today) from Opportunity where Id =: RecordId];
                if(connection1.Traffic__r.IsEmpty() && RegCard.Date__c <> system.Today())
                {
                    Traffic__c Beback = New Traffic__c();
                     if(Schema.sObjectType.Traffic__c.fields.Community__c.isUpdateable())
                    Beback.Community__c = connection.Community__c;
                     if(Schema.sObjectType.Traffic__c.fields.Connection__c.isUpdateable())
                    Beback.Connection__c = connection.Id;
                     if(Schema.sObjectType.Traffic__c.fields.Count__c.isUpdateable())
                    Beback.Count__c = 1;
                    Beback.Date__c = system.Today();
                     if(Schema.sObjectType.Traffic__c.fields.RecordTypeId.isUpdateable())
                    Beback.RecordTypeId = RecordTypeUtil.trafficRecordTypeName();                   
                     if(Schema.sObjectType.Traffic__c.isCreateable())
                    Insert Beback;
                    return 'Success';
                }
                else
                {    
                    if(RegCard.Date__c== system.Today())
                        return 'Sorry, you can\'t create RegCard and BeBack on same day';
                    else
                        return 'Sorry, already BeBack has been created for today';
                }    
            }
            else
                return 'Sorry, you are not allowed to create a BeBack with out Registration Card';
        }
        catch(exception e)
        {
            return e.getMessage();
        }
    }
}