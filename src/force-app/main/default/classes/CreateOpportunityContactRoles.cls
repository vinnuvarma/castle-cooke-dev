public class CreateOpportunityContactRoles{
    Public static void CobuyerCreation(List<Opportunity> ConnectionList,Map<Id, Opportunity> OldMapConnections)
    {
        
        Map<Id,List<Id>> oppCons=new Map<Id,List<Id>>();     
        Set<Id> CobuyerIds=new Set<Id>();
        List<OpportunityContactRole> newCoBuyers=new List<OpportunityContactRole>();
        Set<Id> allAcc=new Set<Id>();
        for(Opportunity con:ConnectionList){
            List<Id> perAcc=new List<Id>();           
            if(con.Co_Buyer_1__c<> NULL && con.Co_Buyer_1__c<>OldMapConnections.get(con.Id).Co_Buyer_1__c){ 
                perAcc.add(con.Co_Buyer_1__c);
                if(OldMapConnections.get(con.Id).Co_Buyer_1__c<>NULL){
                    CobuyerIds.add(OldMapConnections.get(con.Id).Co_Buyer_1__c);
                }
            }else if(OldMapConnections.get(con.Id).Co_Buyer_1__c<>NULL && con.Co_Buyer_1__c== NULL)
                CobuyerIds.add(OldMapConnections.get(con.Id).Co_Buyer_1__c);
            
            if(con.Co_Buyer_2__c<> NULL && con.Co_Buyer_2__c<>OldMapConnections.get(con.Id).Co_Buyer_2__c){
                perAcc.add(con.Co_Buyer_2__c);
                if(OldMapConnections.get(con.Id).Co_Buyer_2__c<>NULL){
                    CobuyerIds.add(OldMapConnections.get(con.Id).Co_Buyer_2__c);
                }
            }else if(OldMapConnections.get(con.Id).Co_Buyer_2__c<>NULL && con.Co_Buyer_2__c== NULL)
                CobuyerIds.add(OldMapConnections.get(con.Id).Co_Buyer_2__c);
            
            if(con.Co_Buyer_3__c<> NULL && con.Co_Buyer_3__c<>OldMapConnections.get(con.Id).Co_Buyer_3__c){
                perAcc.add(con.Co_Buyer_3__c);
                if(OldMapConnections.get(con.Id).Co_Buyer_3__c<>NULL){
                    CobuyerIds.add(OldMapConnections.get(con.Id).Co_Buyer_3__c);
                }
            }else if(OldMapConnections.get(con.Id).Co_Buyer_3__c<>NULL && con.Co_Buyer_3__c== NULL)
                CobuyerIds.add(OldMapConnections.get(con.Id).Co_Buyer_3__c);
            
            if(con.Co_Buyer_4__c<> NULL && con.Co_Buyer_4__c<>OldMapConnections.get(con.Id).Co_Buyer_4__c){
                perAcc.add(con.Co_Buyer_4__c);
                if(OldMapConnections.get(con.Id).Co_Buyer_4__c<>NULL){
                    CobuyerIds.add(OldMapConnections.get(con.Id).Co_Buyer_4__c);
                }
            }else if(OldMapConnections.get(con.Id).Co_Buyer_4__c<>NULL && con.Co_Buyer_4__c== NULL)
                CobuyerIds.add(OldMapConnections.get(con.Id).Co_Buyer_4__c);
            
            if(con.Co_Buyer_5__c<> NULL && con.Co_Buyer_5__c<>OldMapConnections.get(con.Id).Co_Buyer_5__c){
                perAcc.add(con.Co_Buyer_5__c);
                if(OldMapConnections.get(con.Id).Co_Buyer_5__c<>NULL){
                    CobuyerIds.add(OldMapConnections.get(con.Id).Co_Buyer_5__c);
                }
            }else if(OldMapConnections.get(con.Id).Co_Buyer_5__c<>NULL && con.Co_Buyer_5__c== NULL)
                CobuyerIds.add(OldMapConnections.get(con.Id).Co_Buyer_5__c);
            if(!perAcc.isEmpty()){
                oppCons.put(con.ID,perAcc);   
                allAcc.addAll(perAcc);
            }  
            if(!CobuyerIds.isEmpty()){
                allAcc.addAll(CobuyerIds); 
            }
        }
        Id PersonRecordType=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Homebuyer').getRecordTypeId();
        Map<Id,Account> perCon=new Map<Id,Account>([Select Id, PersonContactId from Account where ID In:allAcc AND RecordTypeId=:PersonRecordType]);
        for(Id opp:oppCons.keyset()){
            for(Id acc:oppCons.get(opp)){
                OpportunityContactRole ocr=new OpportunityContactRole();
                ocr.OpportunityId=opp;
                ocr.ContactId=perCon.get(acc).PersonContactId  ;
                ocr.Role='Co-Buyer';
                newCoBuyers.add(ocr);                    
            }    
        }
        Set<ID> delperConList=new Set<ID>();
        for(Account accid:perCon.values()){
            if(CobuyerIds.contains(accid.Id))
                delperConList.add(accid.PersonContactId);    
        }
        List<OpportunityContactRole> deleteCb=new List<OpportunityContactRole>();
        List<OpportunityContactRole> cblist=[Select Id,ContactId,OpportunityId from OpportunityContactRole where ContactId In:delperConlist AND Role='Co-Buyer'];
        
        for(Opportunity con:ConnectionList){
            for(OpportunityContactRole cob:cblist){
                if(OldMapConnections.get(con.Id).Co_Buyer_1__c<>NULL && con.Co_Buyer_1__c<> OldMapConnections.get(con.Id).Co_Buyer_1__c && (cob.OpportunityId==con.Id && cob.ContactId==perCon.get(OldMapConnections.get(con.Id).Co_Buyer_1__c).PersonContactId)){
                    deleteCb.add(cob);  
                }
                else if(OldMapConnections.get(con.Id).Co_Buyer_2__c<>NULL && con.Co_Buyer_2__c<> OldMapConnections.get(con.Id).Co_Buyer_2__c && (cob.OpportunityId==con.Id && cob.ContactId==perCon.get(OldMapConnections.get(con.Id).Co_Buyer_2__c).PersonContactId)){
                    deleteCb.add(cob);  
                }
                else if(OldMapConnections.get(con.Id).Co_Buyer_3__c<>NULL && con.Co_Buyer_3__c<> OldMapConnections.get(con.Id).Co_Buyer_3__c && (cob.OpportunityId==con.Id && cob.ContactId==perCon.get(OldMapConnections.get(con.Id).Co_Buyer_3__c).PersonContactId)){
                    deleteCb.add(cob);  
                }
                else if(OldMapConnections.get(con.Id).Co_Buyer_4__c<>NULL && con.Co_Buyer_4__c<> OldMapConnections.get(con.Id).Co_Buyer_4__c && (cob.OpportunityId==con.Id && cob.ContactId==perCon.get(OldMapConnections.get(con.Id).Co_Buyer_4__c).PersonContactId)){
                    deleteCb.add(cob);  
                }
                else if(OldMapConnections.get(con.Id).Co_Buyer_5__c<>NULL && con.Co_Buyer_5__c<> OldMapConnections.get(con.Id).Co_Buyer_5__c && (cob.OpportunityId==con.Id && cob.ContactId==perCon.get(OldMapConnections.get(con.Id).Co_Buyer_5__c).PersonContactId)){
                    deleteCb.add(cob);  
                }
            }
        }
        
        delete deleteCb;
        insert newCoBuyers;   
        
    }
    Public static void AgentCreation(List<Opportunity> ConnectionList,Map<Id, Opportunity> OldMapConnections)
    {
        Set<Id> ConRoleIds=new Set<Id>();
        List<OpportunityContactRole> newContactRoles=new List<OpportunityContactRole>();
        for(Opportunity con:ConnectionList){
            OpportunityContactRole cb=new OpportunityContactRole();
            if(con.Agent__c<> NULL && con.Agent__c<>OldMapConnections.get(con.Id).Agent__c){
                cb.Contactid=con.Agent__c;
                cb.OpportunityId=con.Id; 
                cb.Role='Agent';    
                newContactRoles.add(cb);
                if(OldMapConnections.get(con.Id).Agent__c<>NULL){
                    ConRoleIds.add(OldMapConnections.get(con.Id).Agent__c);
                }
            }else if(OldMapConnections.get(con.Id).Agent__c<>NULL && con.Agent__c== NULL)
                ConRoleIds.add(OldMapConnections.get(con.Id).Agent__c);
        }
        List<OpportunityContactRole> deleteCb=new List<OpportunityContactRole>();
        List<OpportunityContactRole> cblist=[Select Id,ContactId,OpportunityId from OpportunityContactRole where ContactId In:ConRoleIds AND Role='Agent'];
        for(Opportunity con:ConnectionList){
            for(OpportunityContactRole cob:cblist){
                if(OldMapConnections.get(con.Id).Agent__c<>NULL && con.Agent__c<> OldMapConnections.get(con.Id).Agent__c && (cob.OpportunityId==con.Id && cob.ContactId==OldMapConnections.get(con.Id).Agent__c)){
                    deleteCb.add(cob);  
                }                 
            }
        }
        delete deleteCb;
        insert newContactRoles;  
    }
    
}