Public Class EditQuoteLines
{
    
    @AuraEnabled
    Public Static Quote selectedQuote(String RecordId)
    {	system.debug('Recorid :::In::selectedQuote:::'+RecordId);
        Quote Qtdetails = New Quote();
        Qtdetails = [Select Id, Lot__c, Model__c, status, Community__c from Quote where Id =: RecordId];
        system.debug('********'+Qtdetails);
        return Qtdetails;
    } 
    @AuraEnabled 
    Public Static QuoteLineItem selectedLot(String RecordId, String LotId)
    {	system.debug('RecordId:::selectedLot'+RecordId+'>>>');system.debug('LotId:::selectedLot'+LotId+'>>>');
        QuoteLineItem lotProduct = New QuoteLineItem();
     	if(LotId<>null){
        	lotProduct = [Select Id, Product2Id, Product2.Name, Product_Code__c, Notes__c, Product2.ProductCode, ListPrice, UnitPrice, Quantity, Subtotal, Discount, TotalPrice from QuoteLineItem where QuoteId =: RecordId and Product2Id =: LotId];
            return lotProduct;
        }
     	else return lotProduct;
    }
    @AuraEnabled
    Public Static QuoteLineItem selectedModel(String RecordId, String ModelId)
    {	
        QuoteLineItem modelProduct = New QuoteLineItem();
        if(ModelId!=null){
        	modelProduct = [Select Id, Product2Id, Product2.Name, Product_Code__c, Notes__c, Product2.ProductCode, ListPrice, UnitPrice, Quantity, Subtotal, Discount, TotalPrice from QuoteLineItem where QuoteId =: RecordId and Product2Id =: ModelId];
        }
        return modelProduct;
    }
 /*   @AuraEnabled
    Public Static QuoteLineItem selectedPackage(String RecordId, String PackageId)
    {
        QuoteLineItem packageProduct = new QuoteLineItem();
        if(PackageId <> null)  packageProduct = [Select Id, Product2Id, Product2.Name, Product_Code__c, Notes__c, Product2.ProductCode, ListPrice, UnitPrice, Quantity, Subtotal, Discount, TotalPrice from QuoteLineItem where QuoteId =: RecordId and Product2Id =: PackageId];
        system.debug('***** Package Product *****'+packageProduct);
        return packageProduct;
    } */
    @AuraEnabled
    Public Static List<QuoteLineItem> selectedOptions(String RecordId, String ModelId)
    {
        List<QuoteLineItem> selectedOptions  = New List<QuoteLineItem>();
        selectedOptions = [Select Id, Product2Id, Product2.Name,  Notes__c, Cancelled__c, Product2.ProductCode, ListPrice, UnitPrice, Quantity, Subtotal, Discount, TotalPrice from QuoteLineItem where QuoteId =: RecordId and ((Required_By__c =: ModelId and Product2.recordtype.developername='Model_Options') OR Product2.Global__c = True)];
        System.debug('@@@selectedoptions'+selectedOptions);
        return selectedOptions;
    }
    @AuraEnabled
    Public Static List<WapperClass> availableLots(String RecordId, String CommunityId)
    {	system.debug('RecordId::::'+RecordId+':::CommunityId:::'+CommunityId);
        List<Product2> lotProducts = New List<Product2>();
        lotProducts = [Select Id, Name, Price__c, Status__c,Street__c, ProductCode, Recordtype.DeveloperName, Community__c, Category__c, SubCategory__c from Product2 where IsActive = True and recordtype.developername = 'Lot' and Community__c =: CommunityId and Status__c <> 'Closed' ORDER BY Name];
        system.debug('lotProducts:::'+lotProducts);
     	List<WapperClass> WapperOptions = New List<WapperClass>();
        for(Product2 Prod: lotProducts)
        {
            WapperClass Wc= New WapperClass();
            Wc.ProductDetails = Prod;
            Wc.Checkbox = False;
            Wc.UnitPrice = Prod.Price__c;
            Wc.Discount = 0;
            Wc.Quantity = 1;
            WapperOptions.add(Wc);
        }system.debug('WapperOptions:::'+WapperOptions);
        return WapperOptions;
    }
    @AuraEnabled
    Public Static List<WapperClass> availableModels(String RecordId, String CommunityId)
    {
        List<Product2> modelProducts = New List<Product2>();
        modelProducts = [Select Id, Name, Status__c, Price__c, ProductCode, Recordtype.DeveloperName, Community__c, Category__c, SubCategory__c from Product2 where IsActive = True and recordtype.developername = 'Model' and  Community__c =: CommunityId ORDER BY Name];
        List<WapperClass> WapperOptions = New List<WapperClass>();
        for(Product2 Prod: modelProducts)
        {
            WapperClass Wc= New WapperClass();
            Wc.ProductDetails = Prod;
            Wc.UnitPrice = Prod.Price__c;
            Wc.Checkbox = False;
            Wc.Discount = 0;
            Wc.Quantity = 1;
            WapperOptions.add(Wc);
        }
        return WapperOptions;
    }
/*    @AuraEnabled
    Public Static List<WapperClass> availablePackages(String RecordId, String ModelId, String CommunityId){
        List<WapperClass> WapperOptions = New List<WapperClass>();
        List<Product2> packageProducts = [Select id, Name, Price__c from Product2 where IsActive = True and RecordType.DeveloperName = 'Package' and Community__c =: CommunityId and ID In (Select Package__c from Model_Package__c where Model__c =: ModelId)];
        System.debug('**********'+packageProducts);
        for(Product2 prd: packageProducts){
            WapperClass Wc= New WapperClass();
            Wc.ProductDetails = Prd;
            Wc.UnitPrice = Prd.Price__c;
            Wc.Checkbox = False;
            Wc.Discount = 0;
            Wc.Quantity = 1;
            WapperOptions.add(Wc);
        }
        return WapperOptions;
    } */
    @AuraEnabled
    Public Static List<WapperClass> availableOptions(String RecordId, String CommunityId, String ModelId, String Category, String ProductName)
    {
        List<Product2> modelOptions = New List<Product2>();
        String RecordType = 'Model_Options';
        String qry = 'Select Id, Name, Price__c, ProductCode, Recordtype.DeveloperName,RecordTypeId, Community__c, Category__c,Model__c,Global__c, SubCategory__c from Product2 where IsActive = True'; 
       if(ModelId <> null) 		
            qry += ' and ((RecordType.DeveloperName = \''+String.escapeSingleQuotes(RecordType)+'\' and Model__c = \''+String.escapeSingleQuotes(ModelId)+'\' and Option__r.IsActive = True) OR (Community__c = \''+CommunityId+'\' and Global__c = True))'; 		
        else		
            qry += ' and Community__c = \''+String.escapeSingleQuotes(CommunityId)+'\' and Global__c = True'; 			
		if(Category <> null)
        qry += ' and Category__c = \''+Category+'\'';
        if(ProductName <> Null && ProductName <> '')
            qry +=  ' and (Name Like \'%'+ProductName+'%\' Or ProductCode Like \'%'+ProductName+'%\')';
        System.debug('@@@'+qry);
        modelOptions = Database.Query(qry);
         System.debug('@@@options'+modelOptions);
        List<WapperClass> WapperOptions = New List<WapperClass>();
        for(Product2 Prod: modelOptions)
        {
            WapperClass Wc= New WapperClass();
            Wc.ProductDetails = Prod;
            Wc.UnitPrice = Prod.Price__c;
            Wc.Discount = 0;
            Wc.Quantity = 1;
            Wc.Checkbox = False;
            WapperOptions.add(Wc);
        }
        return WapperOptions;
    }
    @AuraEnabled
    Public Static List<Wapperclass> selectedOptions(String RecordId, List<WapperClass> Options)
    {
        List<QuoteLineItem> LineItems = New List<QuoteLineItem>();
        for(WapperClass Wc: Options)
        {
            QuoteLineItem Line = New QuoteLineItem();
            Line.QuoteId = RecordId;
            Line.Product2Id = Wc.ProductDetails.Id;
            Line.Quantity = Wc.Quantity;
            LineItems.add(Line);
        }
        if(!LineItems.IsEmpty())
            Insert LineItems;
        return Null;
    }
    @AuraEnabled
    Public Static QuoteLineItem updateLine(QuoteLineItem Line)
    {
        QuoteLineItem QtLine= New QuoteLineItem();
        QtLine.Id = Line.id;
        QtLine.UnitPrice = Line.UnitPrice;
        Update QtLine;
        return QtLine;
    }
    @AuraEnabled
    Public Static List<QuoteLineItem> updateOptionsLines(List<QuoteLineItem> Lines, String LineId)
    {
        List<QuoteLineItem> QuoteLines = New List<QuoteLineItem>();
        for(QuoteLineItem QtLine: Lines)
        {
            if(QtLine.Id == LineId)
            {
              QuoteLineItem Line = New QuoteLineItem();
              Line.Id = QtLine.Id;
              Line.Quantity = QtLine.Quantity;
              Line.Notes__c = QtLine.Notes__c;
              if(QtLine.Cancelled__c)
                  Line.Cancelled__c = True;
              QuoteLines.add(Line);    
            }
        }
        if(!QuoteLines.IsEmpty())
            Update QuoteLines;
        return Lines;
    }
    @AuraEnabled
    Public Static String deletelotLine(QuoteLineItem Line)
    {	system.debug('>>>Line:::In QuoteLineItem method'+Line+'>>>>>');
        try
        {
            QuoteLineItem qlineitemlist=[select id,QuoteId from QuoteLineItem where id=:Line.id];
            system.debug('qlineitemlist:::'+qlineitemlist);
            Quote qte=[select id,Lot__c,Model__c from Quote where id=:qlineitemlist.QuoteId];
             system.debug('qte:::'+qte);
             //if(qte.Lot__c!=null)
            	qte.Lot__c=null;
            update qte;
            system.debug('qte:::'+qte);
            Delete Line;
            return 'Success';
        }
        catch(Exception e){
            return e.getMessage();
        }    
    }
    @AuraEnabled
    Public Static String deleteModelLine(String RecordId, QuoteLineItem Line)
    {	// recordid is quoteid
        //Line is 
        try
        {
            List<QuoteLineItem> Quotelines = New List<QuoteLineItem>();
            QuoteLines = [Select Id, QuoteId, Required_By__c from QuoteLineItem where Required_By__c =: Line.Id];
            QuoteLines.add(Line);
            List<Quote_Group__c> QtGroups = New List<Quote_Group__c>();
            QtGroups = [Select Id from Quote_Group__c where Quote__c =: RecordId and Quote__c <> Null];
            Delete QuoteLines;
            if(!QtGroups.isEmpty())
                Delete QtGroups;
           Quote qte=[select id,Lot__c,Model__c from Quote where id=:RecordId];
            if(qte.Model__c!=null)
                qte.Model__c=null;
            update qte;
            return 'Success';
        }
        catch(Exception e){
            return e.getMessage();
        }    
    }
    @AuraEnabled
    Public Static String deleteOptionLine(List<QuoteLineItem> Lines, String LineId)
    {
        List<QuoteLineItem> QuoteLine = New List<QuoteLineItem>();
        try
        {
            QuoteLine = [Select Id from QuoteLineItem where Id =: LineId];
            if(!QuoteLine.IsEmpty())
                Delete QuoteLine;
            return 'Success';
        }
        catch(Exception e){
            return e.getMessage();
        }
    }
    @AuraEnabled
    Public Static String insertLotModelPackage(String LineItem, String RecordId)
    {	System.debug('RecordId:::'+RecordId+'>>>');
        System.debug('Surya Wapper String'+LineItem);
        List<WapperClass> WapperLines = (List<WapperClass>)JSON.deserialize(LineItem, List<WapperClass>.class);
        System.debug('Surya Models'+WapperLines);
        List<QuoteLineItem> InsertQuoteLines = New List<QuoteLineItem>();
        Set<Id> ProductIds = New Set<Id>();
        Map<Id,Id> PriceEntries = New Map<Id,Id>();
        String Type;
        Boolean Spec = False;
        try
        {
            for(WapperClass Wc: WapperLines)
            {
                if(Wc.Checkbox)
                {
                    if(Wc.ProductDetails.RecordType.DeveloperName == 'Model')
                        Type = 'Model';
                    else if(Wc.ProductDetails.RecordType.DeveloperName == 'Lot')
                    {  
                        Type = 'Lot';
                        if(Wc.ProductDetails.Status__c == 'Spec')
                        	{ 
                                Spec = True;
                                List<QuoteLineItem> qu = [select id,QuoteId from QuoteLineItem where QuoteId =: RecordId];
                               if(qu<>null)
                                   delete qu;
                            } 
                    }
              //      else if(Wc.ProductDetails.RecordType.DeveloperName == 'Package')
              //          Type = 'Package';
                    else
                        Type = 'Model Options';
                    InsertQuoteLines.add(New QuoteLineItem(QuoteId = RecordId, Product2Id = Wc.ProductDetails.Id, UnitPrice = Wc.UnitPrice, Discount = Wc.Discount, Quantity = Wc.Quantity, Type__c = Type));       
                    ProductIds.add(Wc.ProductDetails.Id);
                    system.debug('>>>ProductIds:::'+ProductIds+'>>>');    
                }          
            }
            if(Spec && !ProductIds.IsEmpty())
            {               
                List<Quote> SpecQuote = New List<Quote>();
                SpecQuote = [Select Id, (Select Id, Product2Id, PriceBookEntryId, ListPrice, Notes__c, Quantity, QuoteId, UnitPrice, Type__c from QuoteLineItems) from Quote where Lot__c IN: ProductIds and RecordType.DeveloperName = 'Spec_Quote' and Status <> 'Converted'];
                system.debug('SpecQuote:::'+SpecQuote+'>>>');
                QuoteLineItem ModelLine = New QuoteLineItem();
                QuoteLineItem LotLine = New QuoteLineItem();
               // QuoteLineItem PackageLine = New QuoteLineItem();
                List<QuoteLineItem> OptionLines = New List<QuoteLineItem>();
                for(Quote Qt: SpecQuote)
                {	system.debug('Qt.QuoteLineItems'+Qt.QuoteLineItems+'>>>>');
                    for(QuoteLineItem Line: Qt.QuoteLineItems)
                    {
                        if(Line.Type__c == 'Model Options')
                        {
                            QuoteLineItem QLine = New QuoteLineItem();
                            QLine = Line.Clone();
                            QLine.QuoteId = RecordId;
                            OptionLines.add(QLine);
                        }
                        else if(Line.Type__c == 'Model')
                        {
                           ModelLine = Line.Clone();
                           ModelLine.QuoteId = RecordId;
                        }
                        else if(Line.Type__c == 'Lot')
                        {
                           LotLine = Line.Clone();
                           LotLine.QuoteId = RecordId;
                        }
              /*          else if(Line.Type__c == 'Package')
                        {
                           PackageLine = Line.Clone();
                           PackageLine.QuoteId = RecordId;
                        } */
                    }
                }
                Quote QtDetails = New Quote();    
                QtDetails.Id = RecordId;system.debug('>>>LotLine::::'+LotLine+'>>>');
                if(LotLine <> Null)
                {	system.debug('>HELLO Lotline not null');
                 try{
                    Insert LotLine;
                 }
                 catch(Exception e){
                    system.debug('e.getmessage'+e.getmessage()); 
                 }
                     system.debug('>>>LotLine::::'+LotLine+'>>>');
                    QtDetails.Lot__c = LotLine.Product2Id;
                }    
                if(ModelLine <> Null)
                {	system.debug('>>>ModelLine:::+not null::'+ModelLine+'>>>');
                    Insert ModelLine;system.debug('>>>ModelLine:::'+ModelLine+'>>>');
                    QtDetails.Model__c = ModelLine.Product2Id;
                }
                QtDetails.Status = 'Draft';
                Update QtDetails;
                if(ModelLine.Id <> Null && !OptionLines.IsEmpty())  
                {
                    for(QuoteLineItem QLines: OptionLines)
                    {
                        QLines.Required_By__c = ModelLine.Id;
                    }
                    System.debug('Surya Options:'+OptionLines.Size());system.debug('OptionLines'+OptionLines);
                    Insert OptionLines;system.debug('>>>OptionLines:::'+OptionLines+'>>>');
                }      
            }
            else
             {
                List<PriceBookEntry> Entrylist = New List<PricebookEntry>();
                Entrylist = [SELECT Id, ProductCode, Name, UnitPrice, product2id FROM PricebookEntry where Product2id IN: ProductIds];
                for(PriceBookEntry entry: Entrylist)
                {
                    PriceEntries.Put(entry.Product2id, entry.Id);
                }
                for(QuoteLineItem Lines: InsertQuoteLines)
                {
                    if(PriceEntries.ContainsKey(Lines.Product2Id))
                        Lines.PriceBookEntryId = PriceEntries.get(Lines.Product2Id);
                }system.debug('>>>InsertQuoteLines:::'+InsertQuoteLines);
                Insert InsertQuoteLines;
                System.debug(InsertQuoteLines[0].Id);
                if(Type == 'Model' || Type == 'Lot' )
                {
                    Quote UpdateQuote = New Quote();
                    UpdateQuote.Id = RecordId;
                    if(Type == 'Model')
                        UpdateQuote.Model__c = InsertQuoteLines[0].Product2Id;
                    else if(Type == 'Lot')
                        UpdateQuote.Lot__c = InsertQuoteLines[0].Product2Id;
             /*       else 
                        UpdateQuote.Package__c = InsertQuoteLines[0].Product2Id;*/
                    Update UpdateQuote; 
                }
            }
            return 'Success';
        }
        catch(Exception e){
            return e.getMessage();
        }
    }
    @AuraEnabled
    Public Static String insertOptions(String LineItem, String RecordId, String ModelId)
    {
        System.debug('Surya Wapper String'+LineItem);
        List<WapperClass> WapperLines = (List<WapperClass>)JSON.deserialize(LineItem, List<WapperClass>.class);
        System.debug('Surya Models'+WapperLines);
        List<QuoteLineItem> InsertQuoteLines = New List<QuoteLineItem>();
        Set<Id> ProductIds = New Set<Id>();
        Map<Id,Id> PriceEntries = New Map<Id,Id>();
        try
        {
            for(WapperClass Wc: WapperLines)
            {
                if(Wc.Checkbox)
                {
                    InsertQuoteLines.add(New QuoteLineItem(Required_By__c = ModelId, QuoteId = RecordId, Product2Id = Wc.ProductDetails.Id, UnitPrice = Wc.UnitPrice, Discount = Wc.Discount, Quantity = Wc.Quantity, Type__c = 'Model Options'));       
                    ProductIds.add(Wc.ProductDetails.Id);
                }          
            }
            if(!InsertQuoteLines.IsEmpty())
            {
                List<PriceBookEntry> Entrylist = New List<PricebookEntry>();
                Entrylist = [SELECT Id, ProductCode, Name, UnitPrice, product2id FROM PricebookEntry where Product2id IN: ProductIds];
                for(PriceBookEntry entry: Entrylist)
                {
                    PriceEntries.Put(entry.Product2id, entry.Id);
                }
                for(QuoteLineItem Lines: InsertQuoteLines)
                {
                    if(PriceEntries.ContainsKey(Lines.Product2Id))
                        Lines.PriceBookEntryId = PriceEntries.get(Lines.Product2Id);
                }
                Insert InsertQuoteLines;
                System.debug('@@@'+InsertQuoteLines);
            }
            return 'Success';
        }
        catch(Exception e){
            return e.getMessage();
        }
    }
    @AuraEnabled
    Public Static List<PicklistWrap> categorypicklist(String RecordId, String ModelId)
    {
        Quote sc = [select id, Name, Community__c,OpportunityId	from Quote where Id =: RecordId];
        AggregateResult[] groupedResults = [SELECT Category__c, Count(Id) recordCount from Product2 where IsActive = True and Community__c =: sc.Community__c and ((recordtype.developername = 'Model_Options' and Model__c =: ModelId) OR (Community__c =: sc.Community__c and Global__c = True)) Group By Category__c];
        Set<String> availableCategories = New Set<String>();
        List<PicklistWrap> categoryOptions = new List<PicklistWrap>();
        for(AggregateResult ar : groupedResults)
        {
            if(String.Valueof(ar.get('Category__c')) <> Null && String.Valueof(ar.get('Category__c')) <> '' && Integer.valueOf(ar.get('recordCount')) > 0)
            {
               availableCategories.add(String.ValueOf(ar.get('Category__c'))); 
            }
        }
        categoryOptions.add(new picklistWrap('--None-',''));
        for(String Str: availableCategories)
        {
            categoryOptions.add(new picklistWrap(Str,Str));
        }
        return categoryOptions;
    }
    @AuraEnabled
    Public Static List<InnerClass> selectedlines(String RecordId, String ModelId)
    {
        List<QuoteLineItem> selectedOptions  = New List<QuoteLineItem>();
        List<InnerClass> InnerLines = New List<InnerClass>();
        Map<String, List<QuoteLineItem>> MapLines = New Map<String, List<QuoteLineItem>>();
         selectedOptions = [Select Id, Product2Id, Product2.Name,  Notes__c, Cancelled__c, Product2.ProductCode, ListPrice, UnitPrice, Quantity, Subtotal, Discount, TotalPrice, Group__c, Group__r.Name from QuoteLineItem where QuoteId =: RecordId and ((Required_By__c =: ModelId and Product2.recordtype.developername='Model_Options') OR Product2.Global__c = True) Order by Group__r.createddate Asc Nulls Last];
        for(QuoteLineItem Line: selectedOptions)
        {
            String GroupName;
            if(!Line.Cancelled__c)
            {
                if(Line.Group__c == Null)
                    GroupName = '--';  
                else
                    GroupName = Line.Group__r.Name;      
                if(MapLines.ContainsKey(GroupName))
                {
                    List<QuoteLineItem> Existing = New List<QuoteLineItem>();
                    Existing.addAll(MapLines.get(GroupName));
                    Existing.add(Line);
                    MapLines.Put(GroupName, Existing);
                }
                else
                {
                    List<QuoteLineItem> Existing = New List<QuoteLineItem>();
                    Existing.add(Line);
                    MapLines.Put(GroupName,Existing);
                }
            }
            else
            {
                if(MapLines.ContainsKey('Cancelled'))
                {
                    List<QuoteLineItem> Existing = New List<QuoteLineItem>();
                    Existing.addAll(MapLines.get('Cancelled'));
                    Existing.add(Line);
                    MapLines.Put('Cancelled', Existing);
                }
                else
                {
                    List<QuoteLineItem> Existing = New List<QuoteLineItem>();
                    Existing.add(Line);
                    MapLines.Put('Cancelled',Existing);
                }
            }    
        }
        if(!MapLines.KeySet().isEmpty())
        {
            for(String Str: MapLines.KeySet())
            {
               InnerClass InnerDetails = New InnerClass();
               InnerDetails.GroupName = Str;
               List<QuoteLineItem> Existing = New List<QuoteLineItem>();
               Existing.addAll(MapLines.get(Str));
               List<WapperClass> Wapper = New List<WapperClass>();
               for(QuoteLineItem QLine: Existing)
               {
                   WapperClass Wp = New WapperClass();
                   Wp.LineDetails = QLine;
                   Wapper.add(Wp);
               }
               InnerDetails.InnerWapperList.addAll(Wapper); 
               InnerLines.add(InnerDetails); 
            }
        }
        return InnerLines;
    }
    @AuraEnabled
    Public Static String UpdateLineOptions(String LineItems, String LineId)
    {
        try
        {
            List<InnerClass> InnerWapperLines = (List<InnerClass>)JSON.deserialize(LineItems, List<InnerClass>.class);
            QuoteLineItem QtLine = New QuoteLineItem(); system.debug('InnerWapperLines:'+InnerWapperLines);
            for(InnerClass Ic: InnerWapperLines)
            {
                List<WapperClass> WapperLines = New List<WapperClass>();
                WapperLines.addAll(Ic.InnerWapperList);
                for(WapperClass Wc: WapperLines)
                {
                    if(Wc.LineDetails.Id == LineId)
                    {
                        QtLine.Id = Wc.LineDetails.Id;
                        QtLine.Quantity = Wc.LineDetails.Quantity;
                        QtLine.Notes__c = Wc.LineDetails.Notes__c;
                        QtLine.Cancelled__c = Wc.LineDetails.Cancelled__c;
                        if(QtLine.Cancelled__c )
                        {
                            QtLine.Group__c = Null;
                            QtLine.Cancelled_Date__c = System.Today();
                        }
                        break;
                        break;
                    }
                }
            }
            Update QtLine;
            return 'Success';
        }
        catch(Exception e){
            System.Debug('test001'+e);
            return e.getMessage();
        }
    }
    @AuraEnabled
    Public Static List<PicklistWrap> groupPicklist(String RecordId)
    {
        List<Quote_Group__c> GroupLines = New List<Quote_Group__c>();
        GroupLines = [Select Id, Name from Quote_Group__c where Quote__c =: RecordId order by Name Asc];
        List<PicklistWrap> groupPicklist = new List<PicklistWrap>();
        groupPicklist.add(New picklistWrap('','--None--'));
        groupPicklist.add(New picklistWrap('Create Group','Create Group'));
        for(Quote_Group__c Grp : GroupLines)
        {
            if(!String.IsBlank(Grp.Name))
            {
                groupPicklist.add(new picklistWrap(Grp.Id,Grp.Name));
            }
        }
        return groupPicklist;
    }
    @AuraEnabled
    Public Static List<WapperClass> ungroupedQuoteLines(String RecordId, String ModelId)
    {
        List<WapperClass> Wapperlist = New List<WapperClass>();
        List<QuoteLineItem> QtLines = New List<QuoteLineItem>();
        QtLines = [Select Id, Product2Id, Product2.Name,  Notes__c, Cancelled__c, Product2.ProductCode, ListPrice, UnitPrice, Quantity, Subtotal, Discount, TotalPrice from QuoteLineItem where QuoteId =: RecordId and Required_By__c =: ModelId and Group__c = Null and Product2.recordtype.developername='Model_Options'];
        for(QuoteLineItem line: QtLines)
        {
            WapperClass Wc = New WapperClass();
            Wc.LineDetails = line;
            Wc.Checkbox = False;
            Wapperlist.add(Wc);
        }
        return Wapperlist;
    }
    @AuraEnabled
    Public Static String tieToGroups(String RecordId, String GroupId, String Name, String Lines)
    {
        try
        {
            List<WapperClass> WapperLines = (List<WapperClass>)JSON.deserialize(Lines, List<WapperClass>.class);
            if(Name <> Null && Name <> '')
            {
                List<Quote_Group__c> Groupslist = New List<Quote_Group__c>();
                Groupslist = [Select Id, Name from Quote_Group__c where Name =: Name and Quote__c =: RecordId];
                if(Groupslist.IsEmpty())
                {
                    Quote_Group__c QtGroup = New Quote_Group__c();
                    QtGroup.Name = Name;
                    QtGroup.Quote__c = RecordId;
                    Insert QtGroup;
                    GroupId = QtGroup.Id;
                }
                else
                    GroupId = Groupslist[0].Id;
            }
            List<QuoteLineItem> QtLines = New List<QuoteLineItem>(); 
            for(WapperClass Wc: WapperLines)
            {
                QuoteLineItem QLines = New QuoteLineItem();
                If(Wc.Checkbox)
                {    
                    QLines = Wc.LineDetails;
                    QLines.Group__c = GroupId;
                    QtLines.add(QLines);  
                }      
            }
            if(!QtLines.IsEmpty())
                Update QtLines; 
            return 'Success';
        }
        catch(Exception e){
            system.debug('test001'+e);
            return e.getMessage();
        }
    }
    @AuraEnabled
    Public Static String removeFromGroup(String LineId)
    {
        List<QuoteLineItem> Line = New List<QuoteLineItem>();
        try
        {
            String GroupId;
            String QuoteId;
            Line = [Select Id, QuoteId, Group__c from QuoteLineItem where Id =: LineId];
            for(QuoteLineItem QtLine: Line)
            {
                GroupId = QtLine.Group__c;
                QuoteId = QtLine.QuoteId;
                QtLine.Group__c = Null;
            }
            if(!Line.IsEmpty())
                Update Line;
            List<QuotelineItem> QtLines = New List<QuotelineItem>(); 
            QtLines = [Select Id from QuoteLineItem where Group__c =: GroupId and QuoteId =: QuoteId];
            if(QtLines.IsEmpty())
            {
                Quote_Group__c QtGroup = New Quote_Group__c();
                QtGroup.Id = GroupId;
                Delete QtGroup;    
            }   
            return 'Success';
        }
        catch(Exception e){
            return e.getMessage();
        }
    }
    Public Class WapperClass
    {
        @AuraEnabled Public Boolean Checkbox{get;set;}
        @AuraEnabled Public Product2 ProductDetails{get;set;}
        @AuraEnabled Public Decimal UnitPrice{get;set;}
        @AuraEnabled Public Integer Discount{get;set;}
        @AuraEnabled Public Integer Quantity{get;set;}
        @AuraEnabled Public QuoteLineItem LineDetails{get;set;}
        Public WapperClass()
        {
            Checkbox = False;
            ProductDetails = New Product2();
            Discount = 0;
        }
    }
    Public Class InnerClass
    {
        @AuraEnabled Public String GroupName{get;set;}
        @AuraEnabled Public List<WapperClass> InnerWapperList{get;set;}
        Public InnerClass()
        {
            InnerWapperList = New List<WapperClass>();
        }
    }
    public class PicklistWrap{
        @AuraEnabled public string labelVal;
        @AuraEnabled public string optionVal;
        public PicklistWrap(string option, string label){
          this.labelVal = label;
          this.optionVal = option;
        }
    }
}