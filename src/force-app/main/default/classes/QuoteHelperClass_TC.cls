@IsTest()
Public Class QuoteHelperClass_TC
{
 
 public static testmethod void QuoteHelperClass()
 {
   List<Division__c> div = TestDataUtil.divisionCreation(1);
   Insert div;
 
   List<Community__c> com = TestDataUtil.communityCreation(1);
   com[0].Division__c = div[0].Id;
   Insert com;
     
   Product2 PlanProduct = TestDataUtil.ModelProductCreation();
   PlanProduct.Community__c = com[0].Id;
   PlanProduct.Type__c = 'Village';
   Update PlanProduct;
   List<Product2> prod = New List<Product2>();
   prod.add(PlanProduct);
   PricebookEntry PlanEntry = [select Id, name from PricebookEntry where isActive = True and Product2Id =: PlanProduct.Id];
  
 RecordType personAccountRecordType =  [SELECT Id FROM RecordType WHERE Name = 'Homebuyer' and SObjectType = 'Account'];
         Account acc = new Account();
         acc.FirstName = 'Fred';
         acc.LastName = 'Smith';
         acc.RecordType = personAccountRecordType;
         insert acc;
     
   List<Opportunity> op = TestDataUtil.opportunitiesCreation(1);
   op[0].StageName = 'Lead';
     op[0].accountid=acc.id;
   Insert op;
   
   Id RecordTypeIdContact = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Regular').getRecordTypeId();
       
   List<Quote> qo = TestDataUtil.quotesCreation(1);
   qo[0].Cancelled__c = False;
   qo[0].Model__c= PlanProduct.Id;
   qo[0].Lot__c = Planproduct.Id;
   qo[0].OpportunityId = op[0].Id;
   qo[0].RecordTypeId = RecordTypeIdContact;
   qo[0].Override_Deposits__c = true;
   qo[0].Status =  'Draft';
   Insert qo;
   
   List<Quote> qo1 = TestDataUtil.quotesCreation(1);
   qo1[0].Cancelled__c = False;
   qo1[0].Model__c= PlanProduct.Id;
   qo1[0].Lot__c = Planproduct.Id;
   qo1[0].OpportunityId = op[0].Id;
   qo1[0].RecordTypeId = RecordTypeIdContact;
   qo1[0].Override_Deposits__c = true;
   Insert qo1;
  
   Deposit__c dep = New Deposit__c();
   dep.Deposit_Type__c = 'House';
   dep.Status__c = 'Received';
   dep.Quote__c = qo[0].id;
   dep.Amount__c = 10;
   Insert dep;
  
   Deposit__c dep1 = New Deposit__c();
   dep1.Deposit_Type__c = 'Options';
   dep1.Status__c = 'Received';
   dep1.Quote__c = qo[0].id;
   dep1.Amount__c = 10;
   Insert dep1;
   
   op[0].SyncedQuoteId = qo[0].Id;
 
   op[0].StageName = 'Closed Won';
  // update op;
   
   Test.Starttest();
   qo[0].OpportunityId = op[0].Id;
   qo[0].Status = 'Approved';
   update qo;
   Test.Stoptest();
   
   QuoteHelperClass clsRef = new QuoteHelperClass();
   QuoteHelperClass.quotePriceBookUpdate(qo);
   Map<Id,Quote> OldQuotes = new Map<Id,Quote>(qo);
   QuoteHelperClass.quoteSyncUpdate(qo,OldQuotes);
   
 }
  
}