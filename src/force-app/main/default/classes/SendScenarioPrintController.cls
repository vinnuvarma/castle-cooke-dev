public with sharing class SendScenarioPrintController {
    public string Subject{set;get;}
    public string toAddress{set;get;}
    public String ccAddress{set;get;}
    public String body{set;get;}
    public ID Quoteid{set;get;}
    public SendScenarioPrintController() {
         Quoteid=ID.valueof(Apexpages.currentpage().getparameters().get('Id')); 
    }
    public PageReference sendEmail(){
        if(String.isBlank(Subject)){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Please enter Subject'));
            return null;
        }
        else if(String.isBlank(toAddress)){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Please enter To Address'));
            return null;
        }
         Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
         efa.setFileName('Quote.pdf');
         efa.body=Test.isRunningTest() ? Blob.valueOf('Test') : new PageReference('/apex/SelectionReport?qid='+Quoteid).getContentAsPDF();
         
         Messaging.SingleEmailMessage mail=new Messaging.SingleEmailMessage();
         
        if(!String.isBlank(toAddress)){
         List<String> toAddresslist=new List<String>();
         toAddresslist=toAddress.split(',');
         mail.setToAddresses(toAddresslist);
        }
        if(!String.isBlank(ccAddress)){
         List<String> ccAddresslist=new List<String>();
         ccAddresslist=ccAddress.split(',');
         mail.setCcAddresses(ccAddresslist);
        }
        mail.setSubject(Subject);
        mail.setHtmlBody(body);
        mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
        PageReference pgref=new PageReference('/'+Quoteid);
        pgref.setRedirect(True);
        return pgref;
    }
    public PageReference Cancel(){
        PageReference pgref=new PageReference('/'+Quoteid);
        pgref.setRedirect(True);
        return pgref;
    }
}