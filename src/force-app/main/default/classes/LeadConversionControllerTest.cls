@isTest
public class LeadConversionControllerTest {
    @testSetup static void methodName() {
        Division__c division = new Division__c();
        division.Name = 'Atlanta';
        division.Division_ID__c = '7';
        division.State__c = 'AZ';
        division.Active__c = true;
        insert division;        
        
        List<Community__c> Communitys = new List<Community__c>();
        Communitys.add(new Community__c(Name = 'Test Community', Division__c = division.id, Active__c = true));      
        Communitys.add(new Community__c(Name = 'Test Community1', Division__c = division.id, Active__c = true));      
        Communitys.add(new Community__c(Name = 'Test Community2', Division__c = division.id, Active__c = true));      
        insert Communitys;
         
        Lead ld = new Lead(LastName = 'Test lead', FirstName = 'lead1', Company = 'B' , Status = 'New',  LeadSource = 'Website', 
                            State_ID__c = 'tx,ca',Price_Range__c = '$100,000 - $300,000',
                            Appointment__c = '20171007T000000', OwnerId = UserInfo.getUserId(), IsRealtor__c = false); 
        
        Database.DMLOptions dml = new Database.DMLOptions();
        dml.DuplicateRuleHeader.AllowSave = true; 
        Database.SaveResult sr = Database.insert(ld, dml); 
        
        List<Community_Interest__c> CommunityInterests = new List<Community_Interest__c>();
        CommunityInterests.add(new Community_Interest__c(Community__c = Communitys[0].id, Lead__c = ld.id));
        CommunityInterests.add(new Community_Interest__c(Community__c = Communitys[1].id, Lead__c = ld.id));
        CommunityInterests.add(new Community_Interest__c(Community__c = Communitys[2].id, Lead__c = ld.id));
        insert CommunityInterests;
    }
    @isTest static void leadWithOutConnection(){
        List<Community__c> Communitys = [select id, Name, Division__c, Active__c from Community__c];
        Lead ld = [select id, LastName, FirstName, Company, Status, LeadSource, State_ID__c, Price_Range__c, Appointment__c, OwnerId, IsRealtor__c from Lead Limit 1]; 
        ld.Email='test@test123.com';
        update ld;
        List<Community_Interest__c> CommunityInterests = [select id, Community__c, Lead__c from Community_Interest__c];
        LeadConversionController.getCommunityInterests(ld.id);
        LeadConversionController.convertLeadInfo(ld.id, CommunityInterests[0].Id, CommunityInterests);
    }
    @isTest static void leadWithConnection(){
        List<Community__c> Communitys = [select id, Name, Division__c, Active__c from Community__c];
        Division__c div = [select id, Name from Division__c limit 1];
        Lead ld = [select id, LastName, FirstName, Company, Email, Status, LeadSource, State_ID__c, Price_Range__c, Appointment__c, OwnerId, IsRealtor__c from Lead Limit 1]; 
        List<Community_Interest__c> CommunityInterests = [select id, Community__c, Lead__c from Community_Interest__c];
        
        Account ac = new Account(FirstName = ld.FirstName, LastName = ld.LastName, PersonEmail = ld.Email, RecordTypeId = schema.SObjectType.Account.getRecordTypeInfosByName().get('Homebuyer').getRecordTypeId());
        Database.DMLOptions dml = new Database.DMLOptions();
        dml.DuplicateRuleHeader.AllowSave = true;
        Database.SaveResult accountResults = Database.insert(ac, dml);
        
        Opportunity op = new Opportunity(Name = 'Opportunity1', AccountId = ac.Id, StageName = 'Lead', CloseDate = System.Today().addDays(15), Division__c = div.Id, Community__c = CommunityInterests[0].Community__c);
        insert op;
        
        LeadConversionController.getCommunityInterests(ld.id);
        LeadConversionController.convertLeadInfo(ld.id, CommunityInterests[1].Id+';'+CommunityInterests[2].Id, CommunityInterests);
    }   
}