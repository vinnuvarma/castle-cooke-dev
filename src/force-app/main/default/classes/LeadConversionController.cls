/*
Class Name : LeadConversionController
Description: Can be invoked from the LeadConversion lightning component and used to convert the lead from the existing 
Community interest records on Lead record.
Created By : Apostletech Dev 
*/

public without sharing class LeadConversionController{
    
    // To pull the Community interest records with respect to the lead record coming from the UI
    @AuraEnabled
    public static List<Community_Interest__c> getCommunityInterests(string leadId){
        List<Community_Interest__c> interestList = [select id, Lead__c, Community__c,Community__r.Name from 
                                                    Community_Interest__c where Lead__c =: leadId and Active__c = True];
        return interestList;
    }
    
    //Can be invoked from the lightning component to convert the lead record
    @AuraEnabled
    public static acknowledgeWrap convertLeadInfo(string leadId, string selectedInterest,List<Community_Interest__c> interestList){        
        DescribeSObjectResult leadDescribeResult = Lead.getSObjectType().getDescribe();
        List<String> leadFieldNames = new List<String>(leadDescribeResult.fields.getMap().keySet());
        String leadQuery = ' SELECT '  + String.join( leadFieldNames, ',' ) + ', Owner.Profile.Name FROM ' + leadDescribeResult.getName() +' WHERE ' +' Id = \''+leadId+'\''; 
        Lead lead_info  = database.Query(leadQuery);
        Database.DMLOptions dml = new Database.DMLOptions();
        dml.DuplicateRuleHeader.allowSave = true; //Controlls bypass
        dml.DuplicateRuleHeader.runAsCurrentUser = true;
        if(lead_info.Company != NULL || !String.isBlank(lead_info.Company)){
            lead_info.Company = NULL;
            Database.Update(lead_info, dml);
            //update lead_info;
        }                
        Savepoint sp = Database.setSavepoint();
        List<Database.LeadConvert> lcList = new List<Database.LeadConvert>();
        List<Task> taskList = new List<Task>();
        Database.LeadConvert lc = new database.LeadConvert();
        lc.setLeadId(id.valueOf(leadId));
        lc.setOwnerId(userInfo.getUserId());
        lc.ConvertedStatus = 'Closed - Converted';
        lc.setDoNotCreateOpportunity(true);
        Database.LeadConvertResult lcr;
        
        try{
            //Chanage request --> 8th Jan 2019 -- start
            set<Id> cInterestIds = new set<Id>();
            set<Id> existingConnectionCommunitys = new set<Id>();
            set<Id> newConnectionCommunitys = new set<Id>();
            if(!string.isBlank(selectedInterest) && selectedInterest.contains(';')){
                string[] aftersplit = selectedInterest.split(';');
                for(string s : aftersplit){
                    cInterestIds.add(s);
                }
            }
            else{
                cInterestIds.add(selectedInterest);
            }
            
            system.debug('****'+cInterestIds);
            List<Community_Interest__c> ci = [select id,Community__c from Community_Interest__c where id in: cInterestIds];
            set<Id> CommunityIds = new set<Id>();
            for(Community_Interest__c cin : ci){
                Communityids.add(cin.Community__c);
            }
            //--end
            String accountQuery = 'select id, Name, PersonContactId from Account where isPersonAccount = true';
            List<Account> existAccount = new List<Account>();
            if(lead_info.email <> null){
                accountQuery = accountQuery + ' and PersonEmail = \''+lead_info.email+'\'';
                existAccount = database.Query(accountQuery);
            }
            else if(lead_info.Name <> null){
                accountQuery = accountQuery + ' and Name like \'%'+lead_info.Name+'%\'';
                existAccount = database.Query(accountQuery);
            }    
            
            system.debug('existAccount:'+existAccount);
            //To check the account with email id for the existing account or not
            SystemUtil.skipCommunityValid = False;
            if(existAccount.size()>0){
                lc.setAccountId(existAccount[0].id);
                lc.setContactId(existAccount[0].personcontactId);
                if(lc.OwnerId == null) lc.setOwnerId(userInfo.getUserId());
                lcr = Database.convertLead(lc,dml);
                
                LeadConversionController.convertLeads(lead_info, existAccount[0].Id, existAccount[0].personcontactId, Communityids);
                LeadConversionController.afterLeadConversion(cInterestIds,interestList,existAccount[0].Id,leadId);
                return new acknowledgeWrap(false,'Success',existAccount[0].Id);
            }
            //brand new account and connection record
            else{
                lcr = Database.convertLead(lc);
                Id accountId = lcr.getAccountId();
                Id contactId = lcr.getContactId();
                LeadConversionController.convertLeads(lead_info, accountId, contactId, Communityids);
                LeadConversionController.afterLeadConversion(cInterestIds,interestList,accountId,leadId);
                return new acknowledgeWrap(false,'Success',accountId);
            }
            
        }
        catch(Exception e){
            Database.rollback(sp);
            return new acknowledgeWrap(true,e.getMessage(),'');
        }
    }
    
    //Wrapper class to be used to return the acknowledgement whether it is success or fail
    public class acknowledgeWrap{
        @AuraEnabled public boolean isError;
        @AuraEnabled public string ackMsg;
        @AuraEnabled public string successId;
        public acknowledgeWrap(boolean isError,string ackMsg,string successId){
            this.isError = isError;
            this.ackMsg = ackMsg;
            this.successId = successId;
        }
    }
    
    //To create multiple lead conversion for given account and CommunityIds
    public static void convertLeads(Lead leadInfo, String accountId, String contactId, Set<Id> CommunityIds){
        List<Opportunity> newOppList = new List<Opportunity>();
        List<OpportunityTeamMember> opportunityTeamMembers = new List<OpportunityTeamMember>();
        List<Task> taskList = new List<Task>();
        set<Id> existingConnectionCommunitys = new set<Id>();
        Account accObj = [SELECT Id,Name, ownerid FROM Account where Id=:accountId];
        Map<Id,Community__c> CommunityMap = new Map<Id,Community__c>([SELECT Id,Name,Division__c FROM Community__c WHERE Id in :CommunityIds]);
        
        for(Id commId : CommunityIds){
            Opportunity oppty = new Opportunity();
            oppty.Name = CommunityMap.get(commId).Name+ '-' + System.Today().format() + '-' + accObj.Name;
            oppty.StageName = 'Prospect';
            oppty.CloseDate = Date.today();
            oppty.AccountId = accountId;
            oppty.Division__c = CommunityMap.get(commId).Division__c;
            oppty.Community__c = commId;
            // oppty.Agency__c = leadInfo.Agency__c;
            // oppty.Agent__c = leadInfo.Agent__c;
            // oppty.Are_you_currently_working_with_a_Realtor__c = leadInfo.Are_you_currently_working_with_a_Realtor__c;
            // oppty.Did_You_Visit_Another_ViewHomes_Location__c = leadInfo.Did_You_Visit_Another_ViewHomes_Location__c;
            // oppty.Have_You_Visited_Our_Website__c = leadInfo.Have_You_Visited_Our_Website__c;
            // oppty.Have_You_Met_with_a_Lender__c = leadInfo.Have_You_Met_with_a_Lender__c;
            // oppty.How_Many_Bedrooms_Do_You_Need__c = leadInfo.How_Many_Bedrooms_Do_You_Need__c;
            oppty.Price_Range__c = leadInfo.Price_Range__c;
            // oppty.Plan_preference_Ranch_2_Story__c = leadInfo.Plan_Preference__c;
            // oppty.What_Type_of_Home_Interests_You__c  = leadInfo.What_Type_of_Home_Interests_You__c;
            // oppty.When_Would_You_Like_to_Buy__c = leadInfo.When_Would_You_Like_to_Buy_a_Home__c;
            // oppty.Do_You_Have_to_Sell__c = leadInfo.Do_You_Have_to_Sell__c;
            // oppty.Homesite_Type__c = leadInfo.Homesite_Type__c;
            // oppty.How_Did_You_Hear_About_Us__c  = leadInfo.How_Did_You_Hear_About_Us__c;
            // oppty.Current_Housing_Situation__c = leadInfo.Current_Housing_Situation__c;
            // oppty.OSC__c = leadInfo.OSC__c;
            oppty.OwnerId = userInfo.getUserId();
            newOppList.add(oppty);
        }
        if(!newOppList.isEmpty()){
            insert newOppList;
        }    
    }
    
    //This can be invoked after lead conversion to update the opportunity with a Community and then Community interests with Home buyer
    public static void afterLeadConversion(set<Id> comInterestId, List<Community_Interest__c> migrateList,string accountId,string leadId){
        Account acc = [select id,recordType.Name from account where id =: accountId];
        set<Id> comInterestIds = new set<Id>();
        set<Id> CommunityIds = new set<Id>();
        for(Community_Interest__c ci : migrateList){
            comInterestIds.add(ci.id);
            CommunityIds.add(ci.Community__c);
        }
        
        system.debug('****'+leadId);
        system.debug('****'+comInterestIds);
        //To update the Community interests to home buyers for non-selected 
        List<Community_Interest__c> toMigrateList = [select id,Community__c,Community__r.Division__c,Homebuyer__c from 
                                                     Community_Interest__c where id not in: comInterestId and 
                                                     Lead__c =: leadId];
        
        //To delete the home buyers interest records
        List<Community_Interest__c> deleteHomeBuyersList = [select id from Community_Interest__c where Community__c in: CommunityIds and 
                                                            homebuyer__c =: accountId];
        if(deleteHomeBuyersList.size()>0)
            delete deleteHomeBuyersList;
        
        //To delete the lead converted interest records
        List<Community_Interest__c> deleteLeadssList = [select id from Community_Interest__c where id in: comInterestId and 
                                                        Lead__c =: leadId];
        if(deleteLeadssList.size()>0)
            delete deleteLeadssList;
        
        List<Community_Interest__c> cIntToHomBuy = new List<Community_Interest__c>();
        for(Community_Interest__c ci : toMigrateList){
            //if(!comInterestId.contains(ci.id)){
            Community_Interest__c ciUpdate = new Community_Interest__c();
            ciUpdate = ci;
            ciUpdate.Homebuyer__c = accountId;
            cIntToHomBuy.add(ciUpdate);
            //}
        } 
        if(cIntToHomBuy.size()>0)
            update cIntToHomBuy;
    } 
}