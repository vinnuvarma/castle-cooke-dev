public class NewConnectionController {
 @AuraEnabled
    public static String createConnection(String homebuyer,String div,String communityid,Date closingdate){
        try
        {
            Date d = System.today();
            if(d <= closingdate){
            Community__c com=[Select Id,Name,Division__c,Division__r.ID from Community__c where Id=:communityid];
           // Account acc=[Select Id,Name,RecordTypeId, from Account where Id=:homebuyer];     
           // Contact agent=[Select Id,Name,RecordTypeId from Account where Id=:contact.id];    
            Opportunity con=new Opportunity();
            con.Name=com.Name+'-'+Date.today().format()+'-'+contact.Name;
            //con.Homebuyer__c=homebuyer;
            ID homebuyerid=Id.valueof(homebuyer);
            con.AccountId=homebuyerid;
            con.Division__c=div;
            con.Community__c=communityid;
            con.StageName='Lead';
            if(closingdate <> NULL)
               con.CloseDate=closingdate;
            insert con;
            return con.Id;
            }
            return 'Error';
        }
        catch(exception e)
        {
            return e.getMessage();
        }
    } 
    @AuraEnabled
    public Static String recordname(String Accounid){
        Account acc = [select id,Name from Account where Id =: Accounid];
        return acc.Name;
    }

}