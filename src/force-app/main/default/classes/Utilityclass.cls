public class Utilityclass{
 public static Trigger_Handler__mdt checkmetadataValues(string Metadatalabel){
        List<Trigger_Handler__mdt> metadata = new List<Trigger_Handler__mdt>();
        metadata = [SELECT Id,IsActive__c,isAfter__c,isBefore__c,isDelete__c,isInsert__c,isUpdate__c,Label FROM Trigger_Handler__mdt where Label =: Metadatalabel];
        return metadata.size()>0 ? metadata[0] : null;
    }
    }