global with sharing class unregisteredController{

    public unregisteredController(ApexPages.StandardController controller) {
        recordTypeIdString = Schema.SObjectType.Traffic__c.getRecordTypeInfosByName().get('Unregistered').getRecordTypeId();
        recordTypeNameString = Schema.SObjectType.Traffic__c.getRecordTypeInfosByName().get('Unregistered').getName();
        traffic = new Traffic__c();
    }

    
    public Traffic__c traffic{get;set;}
    public String recordTypeIdString{get;set;}
    public String recordTypeNameString{get;set;}
    
    //Used to fetch Communities for lookup using auto complete
    @RemoteAction
    global static List<Community__c> queryCommunities(String keyword) {
        List<Community__c> commList = new List<Community__c>();
        if (keyword != null && keyword.trim() != '') {
            keyword = '%' + keyword + '%';
            commList = [Select Id,Name from Community__c where Name like :keyword limit 5];
        }
        return commList;
    }

    
    @RemoteAction
    global static string insertTrfic(string cmmId, string dte, string count) {
        Id recTypeId = Schema.SObjectType.Traffic__c.getRecordTypeInfosByName().get('Unregistered').getRecordTypeId();
        Date date1=Date.parse(dte);
        Traffic__c tempTrfc = new Traffic__c();
        tempTrfc.Date__c = string.isNotBlank(dte) ? date1 : null;
        tempTrfc.Count__c = string.isNotBlank(count) ? integer.valueOf(count) : null;
        tempTrfc.Community__c = string.isNotBlank(cmmId) ? cmmId : null;
        tempTrfc.recordTypeId = recTypeId;
        
        try{
            
             insert tempTrfc;
             return tempTrfc.id;
        }
        catch(exception ex){

            system.debug('--getCause----'+ex.getCause());
            system.debug('--getLineNumber---'+ex.getLineNumber());
            system.debug('--getMessage---'+ex.getMessage());
            system.debug('--getStackTraceString---'+ex.getStackTraceString());
            system.debug('--getTypeName---'+ex.getTypeName());
            return '';
        }

       
        
    }
    
    
}