//Spec Quotes List View and Spec Quote Creation
Public Class SpecQuotesListView
{
    //Spec List View
    @AuraEnabled
    Public Static List<Quote> specQuotes(String CommunityId, String DivisionId)
    {
        String queryString = 'Select Id, Name, QuoteNumber, Status, Community__c, Community__r.Name, Division__c, Division__r.Name, Lot__c, Lot__r.Name, Model__c, Model__r.Name, Net_Amount__c from Quote where RecordType.DeveloperName = \'Spec_Quote\' and Status = \'New\'';
      //  if(!String.IsBlank(Status))
      //      queryString += ' and Status = \''+Status+'\'';
        if(!String.IsBlank(DivisionId))
            queryString += ' and Division__c = \''+DivisionId+'\'';
        if(!String.IsBlank(CommunityId))
            queryString += ' and Community__c = \''+CommunityId+'\'';        
        List<Quote> Quotes = New List<Quote>();
        System.debug('Search Query: '+queryString);
        Quotes = Database.Query(queryString);
        return Quotes;
    }
    //Spec Creation
    @AuraEnabled
    Public Static Quote newSpecQuote(Quote NewSpec, String CommunityId, String DivisionId)
    {

            Opportunity Opp = New Opportunity();
            List<Opportunity> opptyRecords = [Select Id from Opportunity where Name ='Spec Connection'];
            if(opptyRecords.isEmpty()){
               Opp = New Opportunity();
               Opp.Name = 'Spec Connection';
               Opp.CloseDate = System.Today();
               Opp.StageName = 'Lead';
               insert opp;
            }  
            else{
               Opp = [Select Id from Opportunity where Name ='Spec Connection'];
            } 
            Id RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Spec Quote').getRecordTypeId();
            Quote Spec = New Quote();
            Spec.Name = 'Spec';
            Spec.Status = 'New';
            Spec.RecordTypeId = RecordTypeId;
            Spec.OpportunityId = Opp.Id;
            Spec.Community__c = CommunityId;
            Spec.Division__c = DivisionId;
            Insert Spec;
            return Spec;

    }
    //To get Quote Status
 /*   @AuraEnabled
    public static List<picklistWrap> getStatus()
    {
      List<picklistWrap> StatusOptions = new List<picklistWrap>();
      Schema.DescribeFieldResult fieldResult = Quote.Status.getDescribe();
      List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
      for( Schema.PicklistEntry f : ple)
      {
          if(f.getValue() <> 'Sent via DocuSign' && f.getValue() <> 'Cancelled')
              StatusOptions.add(new picklistWrap(f.getLabel(), f.getValue()));
      }       
      return StatusOptions;
    } */
    //To get divisions list
    @AuraEnabled
    public static List<picklistWrap> getDivisions()
    {
      List<Division__c> divisionList = New List<Division__c>();
      divisionList = [Select Id, Name from Division__c];
      List<picklistWrap> availableDivisions = new List<picklistWrap>();
      for(Division__c div: divisionList)
      {
          availableDivisions.add(new picklistWrap(div.Id, div.Name));
      }       
      return availableDivisions;
    }
    //To get communities list based on selected division
    @AuraEnabled
    public static List<picklistWrap> getCommunities(String DivisionId)
    {
      List<Community__c> communityList = New List<Community__c>();
      communityList = [Select Id, Name from Community__c where Division__c =: DivisionId and Active__c = True];
      List<picklistWrap> availableCommunities = new List<picklistWrap>();
      for(Community__c comm: communityList)
      {
         availableCommunities.add(new picklistWrap(comm.Id, comm.Name));
      }       
      return availableCommunities;
    }
    public class picklistWrap {
        @AuraEnabled public string labelval;
        @AuraEnabled public string selectedVal;
        public picklistWrap(string labelval, string selectedVal){
          this.labelval = labelval;
          this.selectedVal = selectedVal;
        }
    }
}