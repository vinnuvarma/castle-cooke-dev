// Test Class for Controller : WetSignature

@isTest
private class WetSignature_TC{
    
    public static testmethod void CancelQuote() {
        Id pricebookId = Test.getStandardPricebookId();
        Product2 ModelProduct = TestDataUtil.modelProductCreation();
        
        PriceBookEntry pbe = [select id from PriceBookEntry where Product2Id =: modelProduct.Id limit 1];
        
          Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Homebuyer').getRecordTypeId();
       
       Account acc = new Account(LastName ='Test Account',FirstName = 't',personEmail = 'testAccount@gmail.com',RecordTypeId = devRecordTypeId );
       insert acc;
        
        List<Division__c> div = TestDataUtil.divisionCreation(1);
        div[0].name = 'test division';
        Insert div;
        
        List<Community__c> com = TestDataUtil.communityCreation(1);
        com[0].name = 'test community';
        com[0].Division__c=div[0].Id;
        Insert com;
        
        list<Opportunity> opp = TestDataUtil.opportunitiesCreation(1);
        opp[0].Name = 'Spec Connection';
        opp[0].Community__c=com[0].Id;
        opp[0].Division__c = div[0].Id;
        opp[0].AccountId=acc.Id;
        Insert opp;
        system.debug('testname'+opp[0]);
        
        List<Quote> quot  = TestDataUtil.quotesCreation(1);
        quot[0].OpportunityId = opp[0].id;
        quot[0].Community__c = com[0].Id;
        quot[0].Division__c = div[0].Id;
        quot[0].RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Regular').getRecordTypeId();
        quot[0].PriceBook2Id = pricebookId;
        Insert quot;
        
        
        List<QuoteLineItem> quotlineitem = TestDataUtil.quoteslineCreation(1);
        quotlineitem[0].QuoteId=quot[0].id;
        quotlineitem[0].Type__c = 'Model';
        quotlineitem[0].PricebookEntryId =pbe.Id;
        quotlineitem[0].Quantity= 10;
        quotlineitem[0].UnitPrice=23;
        quotlineitem[0].Product2Id = ModelProduct.Id;
        Insert quotlineitem;
        
        List<QuoteLineItem> quotlineitem1 = TestDataUtil.quoteslineCreation(1);
        quotlineitem1[0].QuoteId=quot[0].id;
        quotlineitem1[0].Type__c = 'Lot';
        quotlineitem1[0].PricebookEntryId =pbe.Id;
        quotlineitem1[0].Quantity= 10;
        quotlineitem1[0].UnitPrice=23;
        quotlineitem1[0].Product2Id = ModelProduct.Id;
        Insert quotlineitem1;
        
        List<QuoteLineItem> quotlineitem2 = TestDataUtil.quoteslineCreation(1);
        quotlineitem2[0].QuoteId=quot[0].id;
        quotlineitem2[0].Type__c = 'Model Options';
        quotlineitem2[0].PricebookEntryId =pbe.Id;
        quotlineitem2[0].Quantity= 10;
        quotlineitem2[0].UnitPrice=23;
        quotlineitem2[0].Product2Id = ModelProduct.Id;
        Insert quotlineitem2;
        
        WetSignature Ws = New WetSignature();
        WetSignature.SignatureType();
        WetSignature.WetSignaturevalidations(quot[0].Id,'Wet Signature');
        WetSignature.updateQuote(quot[0].Id,'Wet Signature');
        WetSignature.AllowWetsignature(quot[0].Id);
        
    }
    @istest
    public Static void unittest1(){
        WetSignature.WetSignaturevalidations('','');
    }
    @istest
    public Static void unittest2(){
        WetSignature.WetSignaturevalidations('123','');
    }
}