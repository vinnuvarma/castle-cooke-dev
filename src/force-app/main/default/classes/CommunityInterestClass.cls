public class CommunityInterestClass{
    @AuraEnabled
    public static List<String> getDivisions(){
        List<Division_Sales_Team__c> DivTeam=[Select Id, Division__c, User__c from Division_Sales_Team__c where User__c =: UserInfo.getUserId()];  
        List<ID> div = new List<ID>();   
        for(Division_Sales_Team__c dst : DivTeam){
            div.add(dst.Division__c);   
        }
        if(div.isEmpty()){
            string errAccId = 'No Divisions';
            AuraHandledException ex = new AuraHandledException(errAccId);
            ex.setMessage(errAccId);
            throw ex;
        }
        System.debug('Division SalesTeam Divisions'+div);
        return div;
    }
    @AuraEnabled
    public static string coi_insert(String leadId, String CommunityId, String accountId){
        List<Community_Interest__c> existInterests=new List<Community_Interest__c>();
        if(leadId==NULL){
            existInterests=[Select Id,Name,homebuyer__c,Community__c,Lead__c from Community_Interest__c where homebuyer__c=:accountId AND Community__c=:CommunityId];            
        }
        else if(accountId==NULL){
            existInterests=[Select Id,Name,homebuyer__c,Community__c,Lead__c from Community_Interest__c where Lead__c=:leadId AND Community__c=:CommunityId];            
        }
        if(existInterests.isEmpty()){
            Community_Interest__c coi = new Community_Interest__c();
            coi.Lead__c = leadId;
            coi.Community__c = CommunityId;
            coi.Homebuyer__c = accountId;
            coi.Active__c=true;
            insert coi;
            return coi.id;
        }
        else{
           string errAccId = 'Community Interest already exists';
            AuraHandledException ex = new AuraHandledException(errAccId);
            ex.setMessage(errAccId);
            throw ex;
        }
    }
}