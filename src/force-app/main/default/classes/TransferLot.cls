/*
Class Name : Transfer Lot
Description: Need to transfer lot on the quotes.
Test Class : TransferLot_TC
*/
public class TransferLot{
    @AuraEnabled 
    public static string lotTransfer(String QuoteId, string NewLotId){
        string message = '';
        Quote sc=new Quote();
        Savepoint sp = Database.setSavepoint();
        try{
            
            Quote qt = [Select id, Lot__c, Community__c,Status, Cancelled__c, Model__c,Division__c, Pricebook2Id, Cancel_Reason__c, OpportunityId, Opportunity.StageName, Opportunity.Probability, Recordtype.Developername,(Select Id, Quantity, UnitPrice, Type__c, Total__c, NetAmount__c, Product2Id, PricebookEntryId, QuoteId from QuoteLineItems) from Quote where id=: QuoteId];
            sc =  qt.clone(false, false, false, false);
            sc.Name='NewQuote';
            sc.Status = 'Draft';
            sc.Lot__c = NewLotId;
            sc.Previous_Quote__c = Qt.Id;
            sc.Cancelled__c = false;
            insert sc;
            Quote newQt=[Select Id,QuoteNumber from Quote where Id=:sc.Id];
            sc.Name=newQt.QuoteNumber;
            update sc;
            System.debug('@@@@@NewQoute'+sc);
            
            PricebookEntry Pbe = New PricebookEntry();
            Pbe = [SELECT Id, ProductCode, Name, UnitPrice, product2id FROM PricebookEntry where Product2id =: NewLotid limit 1];
            QuoteLineItem Ql = New QuoteLineItem();
            Ql.QuoteId = sc.Id;
            Ql.PriceBookEntryId = Pbe.Id;
            Ql.unitPrice = Pbe.UnitPrice;
            Ql.Type__c = 'Lot';
            Id ExistingLotId = qt.Lot__c;
            Ql.Quantity =1;
            Insert Ql;
            System.debug('@@'+Ql);
            qt.Status = 'Cancelled';     
            qt.Cancelled__c = True;
            
            update qt;
            System.debug('@@@@@oldQoute'+Qt);
            
            List<QuoteLineItem> NewQtlines = New List<QuoteLineItem>();
            for(QuoteLineItem Line: qt.QuoteLineItems)
            {	QuoteLineItem Qlnew=new QuoteLineItem();  
             if(Line.Type__c<>'Lot'){
                 Qlnew=Line.clone(false, false, false, false);                  
                 Qlnew.QuoteId = sc.Id; 
                 System.debug('@@@@@Qlnew'+Qlnew);  
                 NewQtlines.add(Qlnew);
             }
            }
            if(!NewQtlines.isEmpty())
                Insert NewQtlines;   
            
            if(qt.Status == 'Approved' && (!String.IsBlank(ExistingLotId) || !String.IsBlank(NewLotid)))
            {
                List<Product2> LotProducts = New List<Product2>();
                LotProducts = [Select Id, Status__c from Product2 where Id =: ExistingLotId or Id =: NewLotid];
                for(Product2 Pdt: LotProducts)
                {
                    if(ExistingLotId == Pdt.Id)
                        Pdt.Status__c = 'Open';
                    else if(NewLotid == Pdt.Id)
                        Pdt.Status__c = 'Closed';    
                }
                Update LotProducts;
            }                   
        }
        catch(Exception e){
            message = e.getMessage();
            Database.rollback(sp);
        }
        if(message == null || message == '')
            message = 'updated successfully-'+sc.Id;
        return message;
        
    }
    @AuraEnabled
    Public Static String cancelQuote(Quote QuoteDetails)
    {   system.debug('QuoteDetails:::::::@class'+QuoteDetails);
     System.Savepoint sp = Database.setSavepoint();
     try
     {
         Quote Qt = New Quote();
         if(QuoteDetails.Cancel_Reason__c == 'Convert to Spec' || QuoteDetails.Cancel_Reason__c == 'Revert to Dirt')
         {
             if(QuoteDetails.Cancel_Reason__c == 'Convert to Spec')
             {
                 
                 Opportunity Opp = New Opportunity();
                 Opp = [Select Id from Opportunity where Name ='Spec Connection'];
                 System.Debug('testopp'+Opp);
                 Id RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Spec Quote').getRecordTypeId();
                 Quote qtvals = [Select id, Lot__c, Community__c,Status, Cancelled__c, Model__c,Division__c, Pricebook2Id, Cancel_Reason__c, OpportunityId, Opportunity.StageName, Opportunity.Probability, Recordtype.Developername,(Select id, Type__c, Product2Id from QuoteLineItems where Type__c = 'Lot' limit 1) from Quote where id=: QuoteDetails.Id];
                 
                 Qt =  QuoteDetails.clone(false, false, false, false);
                 
                 Qt.Name='Test';
                 Qt.Cancel_Reason__c = '';
                 Qt.OpportunityId = Opp.Id;
                 Qt.RecordTypeId = RecordTypeId;
                 // Qt.Pricebook2Id=qtvals.Pricebook2Id;
                 // Qt.Status = 'Draft';
                 // After quote is converted to spec the new quote status shouls be new 
                 Qt.Status ='New';
                 Qt.Previous_Quote__c=QuoteDetails.Id;
                 system.debug('Before quote Insert:'+Qt);
                 Insert Qt;
                 system.debug('After quote Insert:'+Qt);
                 Quote specQ=[Select Id,QuoteNumber from Quote where Id=:Qt.Id];
                 QuoteLineItem ModelLine = New QuoteLineItem();
                 QuoteLineItem LotLine = New QuoteLineItem();
                 List<QuoteLineItem> QuoteLines = [Select Id, Product2Id, PriceBookEntryId, ListPrice, Notes__c, Quantity, QuoteId, UnitPrice, Type__c from QuoteLineItem where (Type__c = 'Model' or Type__c = 'Lot') and QuoteId =: QuoteDetails.Id];
                 system.debug('QuoteLines::::::'+QuoteLines);
                 for(QuoteLineItem QtLine: QuoteLines)
                 {     system.debug('QtLine::::::'+QtLine);
                  if(QtLine.Type__c == 'Model')
                  {
                      ModelLine = QtLine.clone();
                      ModelLine.QuoteId = Qt.Id;
                  }
                  else
                  {
                      LotLine = QtLine.clone();
                      LotLine.QuoteId = Qt.Id;
                  }
                 }
                 if(LotLine <> Null)
                 {system.debug('LotLine Insert:'+LotLine); system.debug('LotLine:::::'+LotLine);
                  Insert LotLine;
                  Qt.Lot__c = LotLine.Product2Id;
                  Product2 LotProduct = New Product2();
                  LotProduct.Id = Qt.Lot__c;
                  LotProduct.Status__c = 'Spec';
                  Update LotProduct;
                 }
                 if(ModelLine <> Null)
                 {
                     Insert ModelLine;
                     Qt.Model__c = ModelLine.Product2Id;
                 }  
                 
                 Qt.Name=specQ.QuoteNumber;
                 Update Qt;
                 
                 QuoteLines = [Select Id, Product2Id, PriceBookEntryId, ListPrice, Notes__c, Quantity, QuoteId, UnitPrice, Type__c from QuoteLineItem where Type__c = 'Model Options' and QuoteId =: QuoteDetails.Id];
                 List<QuoteLineItem> SpecQuoteLines = New List<QuoteLineItem>();
                 for(QuoteLineItem Lines: QuoteLines)
                 {
                     QuoteLineItem NewLine = New QuoteLineItem();
                     NewLine = Lines.clone();
                     NewLine.QuoteId = Qt.Id;
                     NewLine.Required_By__c = ModelLine.Id;
                     SpecQuoteLines.add(NewLine);
                 } 
                 if(!SpecQuoteLines.IsEmpty())
                     Insert SpecQuoteLines;  
             }
             else if(QuoteDetails.Lot__c <> Null)
             {   if(QuoteDetails.Cancel_Reason__c == 'Revert to Dirt')
                 system.debug('true');
              system.debug('LOT');
              Product2 ProductDetails = New product2();
              ProductDetails = [Select Id, Status__c from Product2 where Id =: QuoteDetails.Lot__c]; 
              system.debug('ProductDetails :::'+ProductDetails);
              ProductDetails.Status__c = 'Open';
              Update ProductDetails;   system.debug('ProductDetails :::'+ProductDetails);
             }
             
             QuoteDetails.Status = 'Cancelled';
             QuoteDetails.Cancelled__c = True;
             Update QuoteDetails;		system.debug('QuoteDetails:::'+QuoteDetails);
             /*   
Quote Quot=new Quote();
//  Quot.ID=Qt.ID;
Quot.ID=QuoteDetails.ID;
//Quot.Name=Qt.QuoteNumber;
Quot.Name=QuoteDetails.Name;
Update Quot;
system.debug('Quot:::'+Quot); */
             if(QuoteDetails.Cancel_Reason__c == 'Convert to Spec')
                 return 'Success-'+Qt.Id;
             else
                 return 'Success-'+QuoteDetails.Id;	
             //  return 'Success-'+Qt.Id;
         }
         else
             return 'Error';
     }
     catch(Exception e){
         Database.rollback(sp); 
         system.debug('testmess'+e.getMessage());
         return e.getMessage()+ ' : '+ e.getStackTraceString();
     }
    }
    @AuraEnabled
    public static Quote getCommunity(String QuoteId) 
    {
        Quote  qt = [Select id, Lot__c, Community__c,Division__c, Model__r.ProductCode, Cancelled__c from Quote where id=: QuoteId];
        return qt;
    }
    @AuraEnabled
    public static List<picklistWrap> getCancelTo(String QuoteId)
    {
        Id RecordTypeId = RecordTypeUtil.scenarioSpecRecordTypeName();
        Quote pq=new Quote();
        Quote qt=[Select id, Previous_Quote__c from Quote where id=: QuoteId];
        if(qt.Previous_Quote__c!=NULL)
            pq= [Select id, RecordTypeId from Quote where id=: qt.Previous_Quote__c];
        List<picklistWrap> PriorityOptions = new List<picklistWrap>();
        Schema.DescribeFieldResult fieldResult = Quote.Cancel_Reason__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        PriorityOptions.add(new picklistWrap('--None--', ''));
        if(pq==NULL || pq.RecordTypeId!=RecordTypeId){
            for( Schema.PicklistEntry f : ple)
            {
                PriorityOptions.add(new picklistWrap(f.getLabel(), f.getValue()));
            }      
        }
        else if(pq!=NULL && pq.RecordTypeId==RecordTypeId){
            for( Schema.PicklistEntry f : ple)
            {
                if(f.getLabel()=='Convert to Spec')
                    PriorityOptions.add(new picklistWrap(f.getLabel(), f.getValue()));
            }      
        }
        return PriorityOptions;
    }
    public class picklistWrap {
        @AuraEnabled public string labelval;
        @AuraEnabled public string selectedVal;
        public picklistWrap(string labelval, string selectedVal){
            this.labelval = labelval;
            this.selectedVal = selectedVal;
        }
    }
}