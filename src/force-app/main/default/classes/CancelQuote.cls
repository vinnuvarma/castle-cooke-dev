Public Class CancelQuote
{  
    @AuraEnabled
    Public Static Quote selectedQuote(String RecordId)
    {
        Quote Qtdetails = New Quote();
        Qtdetails = [Select Id, Name, Status, Lot__c, Model__c, Community__c,Division__c, Pricebook2Id, Cancel_Reason__c, OpportunityId, Opportunity.StageName, Opportunity.Probability, Recordtype.Developername  from Quote where Id =: RecordId];
        return Qtdetails;
    }
    @AuraEnabled
    Public Static String cancelQuote(Quote QuoteDetails)
    {
       System.Savepoint sp = Database.setSavepoint();
       try
       {
           Quote Qt = New Quote();
           if(QuoteDetails.Cancel_Reason__c == 'Convert to Spec' || QuoteDetails.Cancel_Reason__c == 'Revert to Dirt')
           {
               if(QuoteDetails.Cancel_Reason__c == 'Convert to Spec')
               {
                   
                   Opportunity Opp = New Opportunity();
                   Opp = [Select Id from Opportunity where Name ='Spec Connection'];
                   System.Debug('testopp'+Opp);
                   Id RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Spec Quote').getRecordTypeId();
                   
                   Qt =  QuoteDetails.clone(false, false, false, false);
                  Qt.Cancel_Reason__c = '';
                   Qt.OpportunityId = Opp.Id;
                   Qt.RecordTypeId = RecordTypeId;
                  // Qt.Status = 'Draft';
                  // After quote is converted to spec the new quote status shouls be new 
                   Qt.Status ='New';
                   Qt.Previous_Quote__c=QuoteDetails.Id;
                   Insert Qt;
                   system.debug('quote Insert:'+Qt);
                   
                   QuoteLineItem ModelLine = New QuoteLineItem();
                   QuoteLineItem LotLine = New QuoteLineItem();
                   List<QuoteLineItem> QuoteLines = [Select Id, Product2Id, PriceBookEntryId, ListPrice, Notes__c, Quantity, QuoteId, UnitPrice, Type__c from QuoteLineItem where (Type__c = 'Model' or Type__c = 'Lot') and QuoteId =: QuoteDetails.Id];
                   for(QuoteLineItem QtLine: QuoteLines)
                   {
                       if(QtLine.Type__c == 'Model')
                       {
                           ModelLine = QtLine.clone();
                           ModelLine.QuoteId = Qt.Id;
                       }
                       else
                       {
                           LotLine = QtLine.clone();
                           LotLine.QuoteId = Qt.Id;
                       }
                   }
                   if(LotLine <> Null)
                   {
                       Insert LotLine;
                       Qt.Lot__c = LotLine.Product2Id;
                       Product2 LotProduct = New Product2();
                       LotProduct.Id = Qt.Lot__c;
                       LotProduct.Status__c = 'Spec';
                       Update LotProduct;
                   }
                   if(ModelLine <> Null)
                   {
                       Insert ModelLine;
                       Qt.Model__c = ModelLine.Product2Id;
                   }      
                   Update Qt;
                   
                   QuoteLines = [Select Id, Product2Id, PriceBookEntryId, ListPrice, Notes__c, Quantity, QuoteId, UnitPrice, Type__c from QuoteLineItem where Type__c = 'Model Options' and QuoteId =: QuoteDetails.Id];
                   List<QuoteLineItem> SpecQuoteLines = New List<QuoteLineItem>();
                   for(QuoteLineItem Lines: QuoteLines)
                   {
                       QuoteLineItem NewLine = New QuoteLineItem();
                       NewLine = Lines.clone();
                       NewLine.QuoteId = Qt.Id;
                       NewLine.Required_By__c = ModelLine.Id;
                       SpecQuoteLines.add(NewLine);
                   } 
                   if(!SpecQuoteLines.IsEmpty())
                       Insert SpecQuoteLines;  
              }
               else if(QuoteDetails.Lot__c <> Null)
               {   if(QuoteDetails.Cancel_Reason__c == 'Revert to Dirt')
                   		system.debug('true');
                   system.debug('LOT');
                   Product2 ProductDetails = New product2();
                   ProductDetails = [Select Id, Status__c from Product2 where Id =: QuoteDetails.Lot__c]; 
                   system.debug('ProductDetails :::'+ProductDetails);
                   ProductDetails.Status__c = 'Open';
                   Update ProductDetails;   system.debug('ProductDetails :::'+ProductDetails);
               }
               
               QuoteDetails.Status = 'Cancelled';
               QuoteDetails.Cancelled__c = True;
               Update QuoteDetails;		system.debug('QuoteDetails:::'+QuoteDetails);
            /*   
               Quote Quot=new Quote();
                 //  Quot.ID=Qt.ID;
                   Quot.ID=QuoteDetails.ID;
               //Quot.Name=Qt.QuoteNumber;
               Quot.Name=QuoteDetails.Name;
                   Update Quot;
                   system.debug('Quot:::'+Quot); */
               if(QuoteDetails.Cancel_Reason__c == 'Convert to Spec')
                   return 'Success-'+Qt.Id;
               else
                  	return 'Success-'+QuoteDetails.Id;	
                  //  return 'Success-'+Qt.Id;
           }
           else
               return 'Error';
       }
       catch(Exception e){
            Database.rollback(sp); 
             system.debug('testmess'+e.getMessage());
            return e.getMessage()+ ' : '+ e.getStackTraceString();
        }
    }
    @AuraEnabled
    public static List<picklistWrap> getCancelTo(String QuoteId)
    {
           Id RecordTypeId = RecordTypeUtil.scenarioSpecRecordTypeName();
        Quote  pq=new Quote();
        Quote  qt = [Select id, Previous_Quote__c from Quote where id=: QuoteId];         
        if(qt.Previous_Quote__c!=NULL)
            pq= [Select id, RecordTypeId from Quote where id=: qt.Previous_Quote__c];
        List<picklistWrap> PriorityOptions = new List<picklistWrap>();
        Schema.DescribeFieldResult fieldResult = Quote.Cancel_Reason__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        PriorityOptions.add(new picklistWrap('--None--', ''));
        if(pq==NULL || pq.RecordTypeId!=RecordTypeId){
            for( Schema.PicklistEntry f : ple)
            {
                PriorityOptions.add(new picklistWrap(f.getLabel(), f.getValue()));
            }      
        }
        else if(pq!=NULL && pq.RecordTypeId==RecordTypeId){
            for( Schema.PicklistEntry f : ple)
            {
                if(f.getLabel()=='Convert to Spec')
                    PriorityOptions.add(new picklistWrap(f.getLabel(), f.getValue()));
            }      
        }
        return PriorityOptions;
    }
    public class picklistWrap {
        @AuraEnabled public string labelval;
        @AuraEnabled public string selectedVal;
        public picklistWrap(string labelval, string selectedVal){
          this.labelval = labelval;
          this.selectedVal = selectedVal;
        }
    }
}