@istest
Public class BeBackCreation_TC{
    static testmethod void BeBackCreationTest()
{
    Id accRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Homebuyer').getRecordTypeId();
        Account a = new Account();
        a.FirstName = 'Test';
        a.lastname = 'test1';
        a.PersonEmail = 'testtest@gmail.com'; 
        a.RecordTypeId = accRecordType;
        insert a;
         Id RegRecordType=RecordTypeUtil.trafficRegisteredRecordTypeName();
        List<Division__c> d = TestDataUtil.divisionCreation(1);
        Insert d;
 
        List<Community__c> c = TestDataUtil.communityCreation(1);
        c[0].Division__c = d[0].Id;
        Insert c;
     
        Product2 ModelProduct = TestDataUtil.modelProductCreation();
        ModelProduct.Community__c = c[0].Id;
        Update ModelProduct;
        List<Product2> prod = New List<Product2>();
        prod.add(ModelProduct);
        PricebookEntry ModelEntry = [select Id, name from PricebookEntry where isActive = True and Product2Id =: ModelProduct.Id];
       
         List<Opportunity> op = TestDataUtil.opportunitiesCreation(2);
         op[0].Accountid=a.id;
         op[0].Division__c=d[0].id;
         op[1].Division__c=d[0].id;
         op[1].Accountid = a.id;
         Insert op;
    Traffic__c tra=new Traffic__c();
    tra.community__c = c[0].Id;
    tra.connection__c = op[0].Id; 
    insert tra;
    
    
        
       /*Registration_Card__c regcard = new Registration_Card__c();
        regcard.Community__c = c[0].id;
        regcard.Connection__c = op[0].id;
        regcard.Division__c = d[0].id;
        regcard.Homebuyer__c = a.id;
        insert regcard;*/
        
    
                        
        String s = op[0].id;
        String s1 = op[1].id;
        
         try{
        BeBackCreation.createBeBack(s);
        BeBackCreation.createBeBack(s1);
        BeBackCreation.createBeBack('1234');     
        }
        catch (exception e){
        }
      }  
   
}