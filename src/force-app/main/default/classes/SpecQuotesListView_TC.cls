@IsTest()
Public Class SpecQuotesListView_TC
{
    Public Static TestMethod Void unitTest()
    {
        
         Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Homebuyer').getRecordTypeId();
       
       Account acc = new Account(LastName ='Test Account',FirstName = 't',personEmail = 'testAccount@gmail.com',RecordTypeId = devRecordTypeId );
       insert acc;
       List<Division__c> Divisions = TestDataUtil.divisionCreation(1);
       Insert Divisions;
       
       List<Community__c> Communities = TestDataUtil.communityCreation(1);
        Communities[0].Active__c=true;
       Communities[0].Division__c = Divisions[0].Id;
       Insert Communities;
       
       List<Opportunity> Opportunities = TestDataUtil.opportunitiesCreation(1);
       Opportunities[0].Community__c = Communities[0].Id; Opportunities[0].Division__c = Divisions[0].Id; 
       Opportunities[0].AccountId=acc.Id;
       Insert Opportunities;
       Opportunities[0].Name = 'Spec Connection';
       Update Opportunities;
        
       Quote Qt = New Quote();
       Qt.Name = 'Test';
       Qt.Status = 'Draft';
       
       SpecQuotesListView.newSpecQuote(Qt, Communities[0].Id, Divisions[0].Id);
       SpecQuotesListView.specQuotes(Communities[0].Id, Divisions[0].Id);
      // SpecQuotesListView.getStatus();
       SpecQuotesListView.getDivisions();
       SpecQuotesListView.getCommunities(Divisions[0].Id);
      
    }
    
}