@IsTest()
Public Class inputLookupReusableClass_TC
{
    @testSetup static void prepareSetupData() 
    {
        List<Division__c> Divisions = TestDataUtil.divisionCreation(5);
        Insert Divisions;
        
        List<Community__c> Communities = TestDataUtil.communityCreation(5);
        Insert Communities;         
    }
    Public Static TestMethod Void unitTest()
    {
        List<Division__c> Divisions = [Select Id, Name from Division__c];
        inputLookupReusableClass.getSearchResults('Division__c','Id,Name','Name','Name','Name','','','','','','','Test Division');
    }
    Public Static TestMethod Void unitTest1()
    {
        List<Community__c> communities = [Select Id, Name from Community__c];
        inputLookupReusableClass.getSearchResults('Community__c','Id,Name','Name','Name','Name','','','','','','','Test Community');
    }
    Public Static TestMethod Void unitTest2()
    {
        List<Division__c> Divisions = [Select Id, Name from Division__c];
        inputLookupReusableClass.getSearchResults('Community__c','Id,Name,Division__c','Name','Name','Name','Division__c',Divisions[0].Id,'','','','','Test Community');
    }
    Public Static TestMethod Void unitTest3()
    {
        List<Community__c> communities = [Select Id, Name from Community__c];
        inputLookupReusableClass.getSearchResults('Community__c','Id,Name','','','','Name','','','','','','');
    }
    Public Static TestMethod Void unitTest4()
    {
        List<Community__c> communities = [Select Id, Name from Community__c];
        inputLookupReusableClass.getSearchResults('Community__c','Id,Name','','Name','','','','','','','','Test Community');
    }
    Public Static TestMethod Void unitTest5()
    {
        List<Community__c> communities = [Select Id, Name from Community__c];
        inputLookupReusableClass.getSearchResults('Community__c','Id,Name','','','Name','','','','','','','Test Community');
    }
    Public Static TestMethod Void unitTest6()
    {
        List<Community__c> communities = [Select Id, Name from Community__c];
        inputLookupReusableClass.getSearchResults('Community__c','Id,Name','','','Name','','','','','','','');
    }
     Public Static TestMethod Void unitTest7()
    {
       Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Homebuyer').getRecordTypeId();
       
       Account acc = new Account(LastName ='Test Account',FirstName = 't',personEmail = 'testAccount@gmail.com',RecordTypeId = devRecordTypeId );
       insert acc;  
        inputLookupReusableClass.getAccLeads(acc.Id,NULL);
        
         Lead l1 = new Lead(LastName = 'Test', FirstName = 'lead', Company = 'A' , Email = 'testaccc@testemail.com', Status = 'New',  LeadSource = 'Website', 
        Community_ID__c = '722,993', Division_ID__c = '7,35', State_ID__c = 'az,co',
         Price_Range__c = '$100,000 - $300,000',
         //Date_of_Appointment__c = System.Today(),
         IsRealtor__c = false); 
              
         insert l1;
         inputLookupReusableClass.getAccLeads(NULL,l1.id);
    }
     Public Static TestMethod Void unitTest8()
    {
        List<Community__c> communities = [Select Id, Name from Community__c];
        inputLookupReusableClass.getSearchResults('Product2','Id,Name','','','','Name','test','','','','','');
    }
}