public with sharing class PrintScenarioEmailController {
    Public String Quoteid{set;get;}
    public PrintScenarioEmailController() {
        Quoteid=Apexpages.currentpage().getparameters().get('Id');
    }
    public pageReference sendEmail(){
        pageReference pgref=new pageReference('/apex/SendScenarioPrint?Id='+Quoteid);
        pgref.setRedirect(True);
        return pgref;
    }
}