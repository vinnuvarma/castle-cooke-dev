// Test class for Controller: unregisteredController

@isTest
private class unregisteredControllerTest {
    static testMethod void unReg(){
       
        //Inserting Division Object record
        list<Division__c> divs = TestDataUtil.divisionCreation(1);
        insert divs; 
        
        //Inserting Community Object record
        list<Community__c> comm = TestDataUtil.communityCreation(1);
        insert comm;               
        
        //Calling standard Controller,  queryCommunities(String keyword) and insertTrfic(string cmmId, string dte, string count)     
        unregisteredController unreg1=new unregisteredController(new ApexPages.StandardController(new Traffic__c()));
        unregisteredController.queryCommunities('Test');
       
        try{
        unregisteredController.insertTrfic(comm[0].Id, '02/22/2017', string.valueOf(02/22/2017));
         
        }
        catch(Exception e){}
    }
}