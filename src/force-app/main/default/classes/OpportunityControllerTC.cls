// Test class for Controller : OpportunityController

@isTest
private class OpportunityControllerTC{
    static testMethod void OpportunityControllerTestMethod(){
         
       
       List<Account> perAcc=TestDataUtil.PersonAccountCreation(1);
       insert perAcc[0];
      
       List<Division__c> divcreation=TestDataUtil.divisionCreation(1);
       insert divcreation[0];

       List<Community__c> comm=TestDataUtil.communityCreation(1);
       comm[0].Division__c = divcreation[0].Id;
       comm[0].Active__c = true;
       insert comm[0];
       
       List<Opportunity> oppcrceation=TestDataUtil.opportunitiesCreation(1);
       oppcrceation[0].Name = 'Test001';
       oppcrceation[0].StageName = 'Lead';
       oppcrceation[0].AccountId = perAcc[0].id;
       oppcrceation[0].Community__c = comm[0].id;
       insert oppcrceation[0];
        
        Community_Interest__c cc = new Community_Interest__c();
        cc.Community__C = comm[0].id;
        cc.Homebuyer__c = perAcc[0].Id;
        insert cc;
       
       OpportunityController.UpdateOpportunity(oppcrceation[0].id);
      OpportunityController.getComInterest(perAcc[0].Id);
        OpportunityController.createOpportunity(perAcc[0].Id,cc.Id);
       
     }
   
}