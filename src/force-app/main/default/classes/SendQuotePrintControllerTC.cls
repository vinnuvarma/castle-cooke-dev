@IsTest()
Public Class SendQuotePrintControllerTC{
    
    public static testmethod void SendQuotePrintControllermethod(){
    
      //Id Hbrecordtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Homebuyer').getRecordTypeId();
      List<Account> con=TestDataUtil.PersonAccountCreation(1);
      //con[0].Type__c='Lead';        
      insert con;
    
      Opportunity op = New Opportunity();
      op.Name = 'Test Opportunity';
      op.Status__c = 'Lead';
       op.StageName = 'Lead'; 
        op.CloseDate = System.today();
      op.AccountId=con[0].id;
      //op.CloseDate = System.Today();
      Insert op;
      
      Quote qo = New Quote();
      qo.Name = 'Test Quote';
      qo.OpportunityId = op.Id;
      Insert qo;
      
      ApexPages.currentPage().getParameters().put('Id',qo.id);
      
      SendScenarioPrintController SQPC = new SendScenarioPrintController();
      SQPC.ccAddress='test@gmail.com';
      SQPC.sendEmail();
      SQPC.body='test Body';
      SQPC.cancel();
      
      SQPC.subject = 'test';
      SQPC.sendEmail();
      
      
      SQPC.toAddress='test@gmail.com';  
      SQPC.sendEmail();

    }
    
}