public class Incentivesfiltered {
    public class innerclass { 
        @AuraEnabled public String IncentiveAvailableAt{set;get;}
        @AuraEnabled public String IncentiveType {set;get;}
        @AuraEnabled public String division {set;get;}
        @AuraEnabled public String community {set;get;}
        @AuraEnabled public String lot {set;get;}
         @AuraEnabled public String model {set;get;}
        @AuraEnabled public String IncetiveName {set;get;}
        @AuraEnabled public ID quoteid {set;get;}
        @AuraEnabled public ID incentiveid {set;get;}
        @AuraEnabled public Boolean checklist{set;get;}
        @AuraEnabled public Decimal Amount{set;get;}
        @AuraEnabled public Decimal AmountIncentive{set;get;}
        @AuraEnabled public Decimal Percentage{set;get;}
    }
    
    @AuraEnabled 
    public static List<innerclass> Incentivesfiltered (ID quoteid){  
        system.debug('!!!!!'+quoteid);     
        List<innerclass> innerclas = new List<innerclass>();
        List<Quote_Incentive__c> QuoteIncentives = New List<Quote_Incentive__c>(); 
        Set<Id> ExistingQuoteIncentiveIds = New Set<Id>();
        Quote qu = new Quote();
        qu = [select id, Name, division__c, Lot_Premium__c,Net_Amount__c, Options_Total__c, TotalPrice, Community__c, Community__r.Division__c, Lot__c, Division__r.Name, Community__r.Name,Lot__r.Name,Model__c,Model__r.Name from Quote where id =: quoteid];         
        String incentiveQuery = 'select id, Name, Incentive_Type__c, division__c, community__c, lot__c, Division__r.Name,Community__r.Name,Lot__r.Name,Percent__c,Amount__c,Model__c,Model__r.Name,Effective_Date__c,Expiration_Date__c, (select Incentive_Master__c, Quote__c from Quote_Incentives__r where Quote__c = \''+qu.id+'\') from Incentive_Master__c where ID <> NULL AND Effective_Date__c<=TODAY AND Expiration_Date__c>=TODAY AND Approval_Status__c=\'Approved\'';
        if(qu.Model__c <> null) incentiveQuery = incentiveQuery + ' and (Division__c = \''+qu.Division__c+'\' OR community__c = \''+qu.community__c+'\' OR lot__c = \''+qu.lot__c+'\' OR Model__c = \''+qu.Model__c+'\')';
        else if(qu.Lot__c <> null) incentiveQuery = incentiveQuery + ' and (Division__c = \''+qu.Division__c+'\' OR community__c = \''+qu.community__c+'\' OR lot__c = \''+qu.lot__c+'\')';
        else if(qu.community__c <> null) incentiveQuery = incentiveQuery + ' and (Division__c = \''+qu.Division__c+'\' OR Community__c = \''+qu.community__c+'\')';
        else if(qu.division__c <> null) incentiveQuery = incentiveQuery + ' and division__c = \''+qu.division__c+'\'';         
        system.debug(':::'+incentiveQuery);
        List<Incentive_Master__c> im = database.query(incentiveQuery);
        system.debug('@@@@@@'+im);
        QuoteIncentives  = [select Incentive_Master__c,Quote__c from Quote_Incentive__c where Quote__c =: qu.id];
        for(Quote_Incentive__c Qt: QuoteIncentives)
        {
            if(!String.IsEmpty(Qt.Incentive_Master__c))
                ExistingQuoteIncentiveIds.add(Qt.Incentive_Master__c);  
        }
        system.debug('@@###'+im);
        for(Incentive_Master__c i : im){                             
            if(!ExistingQuoteIncentiveIds.Contains(i.Id)){
                innerclass inn = new innerclass();
                inn.division = qu.Division__r.Name;
                inn.community = qu.Community__r.Name;
                inn.lot = qu.Lot__r.Name;
                inn.model = qu.Model__r.Name;
                 if(i.Model__c <> null) inn.IncentiveAvailableAt = 'Model';
                if(i.Lot__c <> null) inn.IncentiveAvailableAt = 'Lot';
                else if(i.Community__c <> null) inn.IncentiveAvailableAt = 'Community';
                else if(i.Division__c <> null) inn.IncentiveAvailableAt = 'Division';
                inn.IncentiveType = i.Incentive_Type__c;
                inn.checklist = false;
                inn.IncetiveName = i.Name;  
                inn.quoteid = qu.id;
                inn.incentiveid = i.id;
                inn.AmountIncentive = i.Amount__c;
                if(i.Percent__c != null){
                    inn.Percentage = i.Percent__c/100;
                }
                if(i.Amount__c != null){
                    inn.Amount = i.Amount__c;
                }
                if(i.Percent__c != null){
                    if(i.Incentive_Type__c == 'Home Price'){
                        inn.Amount = ((qu.Net_Amount__c)*(i.Percent__c/100)); 
                    }
              /*      if(i.Incentive_Type__c == 'Design Center'){
                        inn.Amount = ((qu.Options_Total__c)*(i.Percent__c/100));
                    }*/
                    /*if(i.Incentive_Type__c == 'Closing Costs'){
                        inn.Amount = ((qu.TotalPrice)*(i.Percent__c/100));
                    }*/
                }                          
                innerclas.add(inn); 
            }
        }  system.debug('@@@@'+innerclas);
        return innerclas;
    }
    
    @AuraEnabled  
    public static String save(String positionRecords){
        System.Debug('POSTIONRECORD@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'+positionRecords);  
        try
        {
            if(!string.isBlank(positionRecords)){
                List<innerclass> innerclas =  (List<innerclass>) System.JSON.deserialize(positionRecords, List<innerclass>.class);
                List<Quote_Incentive__c> quoteincentivedata = new List<Quote_Incentive__c>();
                for(innerclass i : innerclas){  
                    if(i.checklist){
                        if(Schema.sObjectType.Quote_Incentive__c.fields.Incentive_Master__c.isUpdateable() && Schema.sObjectType.Quote_Incentive__c.fields.Quote__c.isAccessible() && Schema.sObjectType.Quote_Incentive__c.fields.Amount__c.isUpdateable())
                            quoteincentivedata.add(new Quote_Incentive__c(Incentive_Master__c = i.incentiveid, Quote__c = i.quoteid, Amount__c = i.Amount)); 
                        
                    }
                }
                if(Schema.sObjectType.Quote_Incentive__c.iscreateable())
                    insert quoteincentivedata;
                
            }
            return 'success';
        }
        Catch(exception e)
        {
            return e.getmessage();
        }       
    } 
}