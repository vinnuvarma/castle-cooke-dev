public class inputLookupReusableClass
{
    @AuraEnabled
    public static List<wrapperClass> getSearchResults(String sObjectName,string sObjectFields,string objectFilter1,string objectFilter2,string objectFilter3, string filter1, string filter1value, string filter2, string filter2value, string filter3, string filter3value, string searchValue)
    {
        String sQuery;
        sQuery = 'select '+sObjectFields+' from '+sObjectName+' where createddate != null';
        boolean b = false;
        
        if(!string.IsBlank(filter1) && !string.IsBlank(filter1value) && sObjectName == 'Community__c')
        {
             if(!filter1value.contains(',')){
               sQuery += ' and Active__c = True and '+filter1+' = \''+filter1value+'\'';
            }
            else if(filter1value.contains(',')){
                Set<String> filterSet = new Set<String>();
                filterSet.addAll(filter1value.split(','));
                sQuery += ' and Active__c = True and '+filter1+' IN: filterSet';   
            }  
        }
        if(!string.IsBlank(filter1) && !string.IsBlank(filter1value) && sObjectName == 'Product2')
        {
            sQuery += ' and '+filter1+' = \''+filter1value+'\'';
            if(!string.IsBlank(filter2) && !string.IsBlank(filter2value))
                sQuery += ' and '+filter2+' = \''+filter2value+'\'';
            if(!string.IsBlank(filter3) && !string.IsBlank(filter3value)) 
                sQuery += ' and '+filter3+' <> \''+filter3value+'\'';   
        }
        if(!string.IsBlank(searchValue) && !string.IsBlank(objectFilter1))
        {
            sQuery += ' and ('+objectFilter1+' LIKE \'%'+ String.escapeSingleQuotes(searchValue.trim())+ '%\'';
            b = true;
        }
        if(!string.IsBlank(searchValue) && !string.IsBlank(objectFilter2))
        {
            if(b)
                sQuery += ' or '+objectFilter2+' LIKE \'%'+ String.escapeSingleQuotes(searchValue.trim())+ '%\'';
            else
            {
                sQuery += ' and ('+objectFilter2+' LIKE \'%'+ String.escapeSingleQuotes(searchValue.trim())+ '%\'';
                b = true;
            }    
        }
        if(!string.IsBlank(searchValue) && !string.IsBlank(objectFilter3))
        {
            if(b)
                sQuery += ' or '+objectFilter3+' LIKE \'%'+ String.escapeSingleQuotes(searchValue.trim())+ '%\'';
            else
                sQuery += ' and ('+objectFilter3+' LIKE \'%'+ String.escapeSingleQuotes(searchValue.trim())+ '%\'';
        }
        if(!string.IsBlank(searchValue) && (!string.IsBlank(objectFilter3) || !string.IsBlank(objectFilter2) || !string.IsBlank(objectFilter1)))
            sQuery += ')  limit 50';
        else if(string.IsBlank(objectFilter3) && string.IsBlank(objectFilter2) && string.IsBlank(objectFilter1)) 
            sQuery += ' limit 5';   
        else
            sQuery += ' limit 5';
        List<sObject> sObjectList = Database.query(sQuery);
        
        List<wrapperClass> wrapList = new List<wrapperClass>();
        for(sObject s : sObjectList)
        {
            wrapperClass w = new wrapperClass();
            if(!string.IsBlank(objectFilter1))
                w.displayField1 = string.valueOf(s.get(objectFilter1));
            if(!string.IsBlank(objectFilter2))
                w.displayField2 = string.valueOf(s.get(objectFilter2));
            if(!string.IsBlank(objectFilter3))
                w.displayField3 = string.valueOf(s.get(objectFilter3)); 
            
            w.recordId = string.valueOf(s.get('id'));
            wrapList.add(w);
        }
        system.debug(wrapList);
        return wrapList;
    }
    @AuraEnabled
    public static sObject getAccLeads(String accid,String leadid){
        sObject sob;
        if(accid!=NULL && accid!=''){
            Account acc=[Select Id,Name from Account where Id=:accid];
            sob=new Account();
            sob=acc;
        }
        else if(leadid!=Null && leadid!=''){
            Lead lea=[Select Id,Name from Lead where Id=:leadid];
            sob=new Lead();
            sob=lea;
        }
        return sob;
    }
    public class wrapperClass
    {
        @AuraEnabled public string displayField1{Get;set;}
        @AuraEnabled public string displayField2{Get;set;}
        @AuraEnabled public string displayField3{Get;set;}
        @AuraEnabled public string recordId{Get;set;}
    }
}