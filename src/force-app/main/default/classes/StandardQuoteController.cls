public class StandardQuoteController {
    
    @AuraEnabled
    public static Id getRecTypeId(String recordTypeLabel){
        Id recid = Schema.SObjectType.Quote.getRecordTypeInfosByName().get(recordTypeLabel).getRecordTypeId();        
        return recid;
    }  
    @AuraEnabled
    public static QuoteClass RegRequired(String conId){
        Id recId = RecordTypeUtil.trafficRegisteredRecordTypeName();
        Opportunity connectionRecord = [select id, Name, Division__c, Division__r.Active__c, Community__c, Community__r.Active__c, (select id from Quotes where Status = 'Approved' and Cancelled__c = False), (Select Id from Traffic__r where RecordTypeId =: recId) from Opportunity where Id =: conId];
        Quote_Validation__mdt isRegRequired = new Quote_Validation__mdt();
        isRegRequired = [select id, MasterLabel, Reg_Card_Required__c from Quote_Validation__mdt where MasterLabel = 'RegCardRequired'];
        QuoteClass sc = new QuoteClass();
        sc.hasError = False;
        sc.message = '';
        // return isRegRequired.Reg_Card_Required__c;
        if(!connectionRecord.Division__r.Active__c || !connectionRecord.Community__r.Active__c){
            sc.hasError = True;
            sc.message = 'Division and Community on Opportunity should be active to create Quote.';
        }
        else if(!connectionRecord.Quotes.isEmpty()){
            sc.hasError = True;
            sc.message = 'One of the Quote for the Opportunity is already Approved. Please use new Opportunity or cancel the approved scenario to continue.';
        }
        else if(isRegRequired.Reg_Card_Required__c == True){
            if(connectionRecord.Traffic__r.isEmpty()){
                sc.hasError = True;
                sc.message = 'Quote cannot be created without RegCard.';
            }
        }
        return sc;
    }
    @AuraEnabled
    public static void UpdateSaleDate(String quotid){
        Quote q=[Select Id,Name,Estimated_Sale_date__c,OpportunityId from Quote where Id=:quotid];
        Opportunity opp=[Select Id,Name,CloseDate from Opportunity where Id=:q.OpportunityId];
        if(opp.CloseDate!=NULL)
        q.Estimated_Sale_Date__c=opp.CloseDate;
        update q;
        
    }  
    public class QuoteClass{
        @AuraEnabled public boolean hasError{get;set;}
        @AuraEnabled public string message{get;set;}
        
    }    
}