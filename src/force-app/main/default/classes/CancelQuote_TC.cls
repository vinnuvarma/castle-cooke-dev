@Istest
Public Class CancelQuote_TC{
    public static testmethod void CancelQuote() 
    {
        Id RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Spec Quote').getRecordTypeId();
        Id accRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Homebuyer').getRecordTypeId();
        
        Account a = new Account();
        a.FirstName = 'Test';
        a.lastname = 'test1';
        a.PersonEmail = 'testtest@gmail.com'; 
        a.RecordTypeId = accRecordType;
        insert a;
        
        Division__c d = new Division__c();
        d.Name = 'divisiontest';
        insert d;
        
        id rt = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Lot').getRecordTypeId();
        id rt1 = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Lot').getRecordTypeId();
        
        Product2 p=new Product2();
        p.Name='Test Product';
        p.recordTypeId=rt;
        p.Price__c=100;
        p.IsActive=true;
        insert p;
        
        Product2 p1=new Product2();
        p1.Name='Test Product';
        p1.recordTypeId=rt1;
        p1.Price__c=100;
        p1.IsActive=true;
        insert p1;
        
        Community__c c=new Community__c();
        c.Active__c=true;
        c.Name='Test Community';
        c.Division__c=d.id;
        insert c;
        
        Pricebook2 pb = new Pricebook2();
        pb.Name = 'Dheeraj';
        insert pb;
        
        opportunity op = new opportunity();
        op.Name = 'Spec Connection';
        op.AccountID= a.id;
        op.Pricebook2Id = pb.id;
        op.Community__c = c.id;
        op.StageName = 'Prospect';
        op.closeDate=System.today()+10;
        insert op;
        System.debug('TestOpp'+op);
        
        quote q = new quote();
        q.Name = 'sandeep';
        q.pricebook2id = pb.Id;
        q.OpportunityID= op.id;
        q.Division__c=d.id;
        q.Community__c=c.id;
        q.RecordTypeId = RecordTypeId;
        q.Lot__c=p.id;
        q.Model__c = p1.Id;
        q.Cancel_Reason__c = 'Convert to Spec';
        q.PriceBook2Id = pb.id;
        insert q;
        
  /*      quote q1 = new quote();
        q1.Name = 'sandeep';
        q1.OpportunityID= op.id;
        q1.Division__c=d.id;
        q1.Community__c=c.id;
        q1.Lot__c=null;
        insert q1;
        
        quote q2 = new quote();
        q2.Name = 'sandeep';
        q2.OpportunityID= op.id;
        q2.Division__c=d.id;
        q2.Community__c=null;
        q2.Lot__c=null;
        insert q2; */
        
        PricebookEntry pbe = new PricebookEntry();
        pbe.Product2Id = p.id;
        pbe.UnitPrice = 100;
        pbe.Pricebook2Id = pb.id;
        pbe.UseStandardPrice=false;
        pbe.isActive = true;
        insert pbe;
        
        QuoteLineItem qli=new QuoteLineItem();
        qli.UnitPrice=20;
        qli.Quantity=10;
        qli.QuoteId=q.id;
        qli.type__c='lot';
        qli.PriceBookEntryID=pbe.id;
        insert qli;         
        
        
       
        
         QuoteLineItem qli1=new QuoteLineItem();
        qli1.UnitPrice=20;
        qli1.Quantity=10;
        qli1.QuoteId=q.id;
        qli1.type__c='Model';
        qli1.PriceBookEntryID=pbe.id;
        insert qli1;  
        
        QuoteLineItem qli2=new QuoteLineItem();
        qli2.UnitPrice=20;
        qli2.Quantity=10;
        qli2.QuoteId=q.id;
        qli2.type__c='Model Options';
        qli2.PriceBookEntryID=pbe.id;
        insert qli2; 
         CancelQuote.selectedQuote(q.Id);
        cancelQuote.cancelQuote(q);
    }
    public static testmethod void CancelQuote01() 
    {
        Id RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Spec Quote').getRecordTypeId();
        Id pricebookId = Test.getStandardPricebookId();
        Id accRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Homebuyer').getRecordTypeId();
        
        Account a = new Account();
        a.FirstName = 'Tests';
        a.lastname = 'test12';
        a.PersonEmail = 'testtest2@gmail.com'; 
        a.RecordTypeId = accRecordType;
        insert a;
        
        
        List<Community__c> com = TestDataUtil.communityCreation(1);
        com[0].name = 'test community';
        Insert com;
        
        List<Division__c> div = TestDataUtil.divisionCreation(1);
        div[0].name = 'test division';
        Insert div;
        
        list<Opportunity> opp = TestDataUtil.opportunitiesCreation(1);
        opp[0].Name = 'Spec Connection';
        opp[0].Accountid=a.id;
        opp[0].Division__c = div[0].Id; 
        opp[0].Community__c = com[0].id;
        opp[0].StageName = 'Prospect';
        opp[0].closeDate=System.today()+10;
        Insert opp;
        
        List<Quote> quot  = TestDataUtil.quotesCreation(1);
        quot[0].OpportunityId = opp[0].id;
        quot[0].Community__c = com[0].Id;
        quot[0].Division__c = div[0].Id;
        quot[0].Cancel_Reason__c = 'Convert to Spec';
        quot[0].RecordTypeId = RecordTypeId;
        quot[0].PriceBook2Id = pricebookId;
        Insert quot;
        
        CancelQuote.selectedQuote(quot[0].Id);
        cancelQuote.cancelQuote(quot[0]);
    }
    public static testmethod void CancelQuote02() 
    {
        Id RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Spec Quote').getRecordTypeId();
        Id pricebookId = Test.getStandardPricebookId();
        Product2 lotProduct = TestDataUtil.lotProductCreation();
        Id accRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Homebuyer').getRecordTypeId();
        
        Account a = new Account();
        a.FirstName = 'Test22';
        a.lastname = 'test11';
        a.PersonEmail = 'testtest3@gmail.com'; 
        a.RecordTypeId = accRecordType;
        insert a;
        
        
        List<Community__c> com = TestDataUtil.communityCreation(1);
        com[0].name = 'test community';
        Insert com;
        
        List<Division__c> div = TestDataUtil.divisionCreation(1);
        div[0].name = 'test division';
        Insert div;
        
        list<Opportunity> opp = TestDataUtil.opportunitiesCreation(1);
        opp[0].Name = 'Spec Connection';
        opp[0].Accountid=a.id;
        opp[0].Division__c = div[0].Id; 
        opp[0].Community__c =com[0].id;
        opp[0].StageName = 'Prospect';
        opp[0].closeDate=System.today()+10;
        Insert opp;
        
        List<Quote> quot  = TestDataUtil.quotesCreation(1);
        quot[0].OpportunityId = opp[0].id;
        quot[0].Community__c = com[0].Id;
        quot[0].Division__c = div[0].Id;
        quot[0].Lot__c = lotProduct.id;
        quot[0].Cancel_Reason__c = 'Revert to Dirt';
        quot[0].RecordTypeId = RecordTypeId;
        quot[0].PriceBook2Id = pricebookId;
        Insert quot;
        
        CancelQuote.selectedQuote(quot[0].Id);
        cancelQuote.cancelQuote(quot[0]);
        cancelQuote.getCancelTo(quot[0].Id);
    }
}