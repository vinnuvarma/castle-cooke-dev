public class RecordTypeUtil{
    public Static Id  AccountRecordTypeName(){
     Id lstRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Homebuyer').getRecordTypeId();
      return lstRecordType ;
    }
    public Static Id  contactAgentRecordTypeName(){
     Id lstRecordType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Agent').getRecordTypeId();
      return lstRecordType ;
    }
    public Static Id  lotRecordTypeName(){
     Id lstRecordType = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Lot').getRecordTypeId();
      return lstRecordType ;
    }
    public Static Id  modelRecordTypeName(){
     Id lstRecordType = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Model').getRecordTypeId();
      return lstRecordType ;
    }
    public Static Id  optionRecordTypeName(){
     Id lstRecordType = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Options').getRecordTypeId();
      return lstRecordType ;
    }
    public Static Id  modelOptionRecordTypeName(){
     Id lstRecordType = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Model Options').getRecordTypeId();
      return lstRecordType ;
    }
     public Static Id  incentiveRecordTypeName(){
     Id lstRecordType = Schema.SObjectType.Incentive_Master__c.getRecordTypeInfosByName().get('Community Incentive').getRecordTypeId();
      return lstRecordType ;
    }
    public Static Id  incentivePercentRecordTypeName(){
     Id lstRecordType = Schema.SObjectType.Incentive_Master__c.getRecordTypeInfosByName().get('Division Incentive').getRecordTypeId();
      return lstRecordType ;
    }
    public Static Id  scenarioRegularRecordTypeName(){
     Id lstRecordType = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Regular').getRecordTypeId();
      return lstRecordType ;
    }
     public Static Id  scenarioSpecRecordTypeName(){
     Id lstRecordType = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Spec Quote').getRecordTypeId();
      return lstRecordType ;
    }
     public Static Id  trafficRecordTypeName(){
     Id lstRecordType = Schema.SObjectType.Traffic__c.getRecordTypeInfosByName().get('BeBack').getRecordTypeId();
      return lstRecordType ;
    }
      public Static Id  trafficRegisteredRecordTypeName(){
     Id lstRecordType = Schema.SObjectType.Traffic__c.getRecordTypeInfosByName().get('Registered').getRecordTypeId();
      return lstRecordType ;
    }
    public Static Id  trafficUnregisteredRecordTypeName(){
     Id lstRecordType = Schema.SObjectType.Traffic__c.getRecordTypeInfosByName().get('Unregistered').getRecordTypeId();
      return lstRecordType ; 
    }
}