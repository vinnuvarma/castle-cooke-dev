@istest
public class CommunityInterestClass_TC{
     
     @istest
        public static void UnitTest1(){
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r; 
        profile p = new profile();
        p = [select id,Name from profile where Name = 'System Administrator' limit 1];
        User u = TestDataUtil.createTestUser(r.id,p.id,'test','test');
        insert u;
        system.runas(u){
        List<Division__c> div = TestDataUtil.divisionCreation(1);
        insert div;
        
        Division_Sales_Team__c divsale = new Division_Sales_Team__c();
        divsale.Division__c = div[0].id;
        divsale.User__c = u.id;
        insert divsale;
        
        Lead lead=new Lead(LastName='Doe',FirstName='John',Company='Test',Status='Inquiry');
        insert lead;
                
        List<Community__c> com =  TestDataUtil.communityCreation(1);
        insert com;
        
        List<Account> acc = TestDataUtil.PersonAccountCreation(1);
        insert acc; system.debug('ac:'+acc);
        
        CommunityInterestClass.getDivisions();
        CommunityInterestClass.coi_insert(Null,com[0].id,acc[0].id);
        CommunityInterestClass.coi_insert(lead.id,com[0].id,Null);       
        }
    } 
     @istest
        public static void UnitTest2(){
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r; 
        profile p = new profile();
        p = [select id,Name from profile where Name = 'System Administrator' limit 1];
        User u = TestDataUtil.createTestUser(r.id,p.id,'test','test');
        insert u;
        system.runas(u){
            List<Division__c> div = TestDataUtil.divisionCreation(1);
            insert div;
            
            Division_Sales_Team__c divsale = new Division_Sales_Team__c();
            divsale.Division__c = div[0].id;
            divsale.User__c = u.id;
           // insert divsale;
            
            Lead lead=new Lead(LastName='Doe',FirstName='John',Company='Test',Status='Inquiry');
            insert lead;
                    
            List<Community__c> com =  TestDataUtil.communityCreation(1);
            insert com;
            
            List<Account> acc = TestDataUtil.PersonAccountCreation(1);
            insert acc; system.debug('ac:'+acc);
           
            try{
            CommunityInterestClass.getDivisions();
            CommunityInterestClass.coi_insert(Null,com[0].id,acc[0].id);
                CommunityInterestClass.coi_insert(lead.id,com[0].id,Null);             
            CommunityInterestClass.coi_insert(Null,com[0].id,acc[0].id);
            } 
            catch(Exception e) {
              //  Block of code to handle errors
            }      
        }
    }
    @istest
    public Static void unittest3(){
        List<Community__c> com =  TestDataUtil.communityCreation(1);
        insert com;
        
        List<Account> acc = TestDataUtil.PersonAccountCreation(1);
        insert acc; system.debug('ac:'+acc);
            Community_Interest__c coi=new Community_Interest__c();
            coi.Homebuyer__c=acc[0].Id;
            coi.Community__c=com[0].id;
            insert coi;
        try{
               CommunityInterestClass.coi_insert(Null,com[0].id,acc[0].id);
        }
        catch(exception e){
            
        }
                      
    }
}