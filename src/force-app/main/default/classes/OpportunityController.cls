public class OpportunityController {
    @AuraEnabled
    public static void UpdateOpportunity(Id recId){ 
        Opportunity con=[Select Id,Name,Community__c,Accountid,Division__c from Opportunity where Id=:recId];
        system.debug('con:::'+con);
        Community__c com=[Select Id,Name,Division__c from Community__c where Id=:con.Community__c];
        system.debug('com:::'+com);
        Account acc=[Select Id,Name from Account where Id=:con.Accountid];
        system.debug('acc:::'+acc);                                          
        con.Name=com.Name+'-'+System.Today().format()+'-'+acc.Name;
        // con.Division__c=com.Division__c;
        update con;
        System.debug('@@con'+con);
    }
    @AuraEnabled
    public static List<Community_Interest__c> getComInterest(Id recId){ 
        List<Community_Interest__c> coilist=[Select Id,Name,Active__c,Community__c,Community__r.Name,Homebuyer__c,Lead__c from Community_Interest__c where Active__c=True and Homebuyer__c=:recId];
        return coilist;
    }
    @AuraEnabled
    public static String createOpportunity(string accountId,string selectedInterest){
         Community_Interest__c comInfo= [select id, Community__c, Community__r.Name, Community__r.division__c, homebuyer__r.Name from Community_Interest__c where id =: selectedInterest];
        
         Opportunity opp_info = new Opportunity();
            opp_info.accountId = accountId;
            if(comInfo.Community__c <> null) {
               opp_info.Name = comInfo.Community__r.Name+'-'+System.Today().format()+'-'+comInfo.homebuyer__r.Name;
               opp_info.Community__c = comInfo.Community__c;
               opp_info.division__c = comInfo.Community__r.division__c;
            }   
            if(comInfo.Homebuyer__c <> null) 
                opp_info.accountid = comInfo.Homebuyer__c;
            opp_info.closedate = system.today();
            opp_info.stageName = 'Prospect';
            insert opp_info;
             if(opp_info.id != null){
                delete comInfo;
            } system.debug('comInfo:'+comInfo);
        return opp_info.ID;
    }
}