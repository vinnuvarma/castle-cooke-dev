// Trigger : RegCardTriggerHandler
public class RegCardHelper{
    @AuraEnabled
    public static string doRegCard(string opp){
        String Message;
        String RecordTypeId = RecordTypeUtil.TrafficRegisteredRecordTypeName();
        DescribeSObjectResult connectionDescribeResult = Opportunity.getSObjectType().getDescribe();
        List<String> connectionFieldNames = new List<String>( connectionDescribeResult.fields.getMap().keySet() );
        DescribeSObjectResult TrafficDescribeResult = Traffic__c.getSObjectType().getDescribe();
        List<String> trafficFieldNames = new List<String>( TrafficDescribeResult.fields.getMap().keySet() );
        String connectionQuery = 'SELECT '+String.join( connectionFieldNames, ',' )+',(SELECT '+String.join( trafficFieldNames, ',' )+ ' FROM traffic__r WHERE RecordTypeId =\''+String.escapeSingleQuotes(RecordTypeId)+'\')' +' FROM '+ connectionDescribeResult.getName()+' WHERE id =\''+String.escapeSingleQuotes(opp)+'\'';
        //Connection__c op=[select id,I_plan_to_purchase_a_home__c,How_did_you_hear_about_us__c,When_do_you_plan_to_move__c,Current_Residence__c,Price_Range_Min__c,Price_Range_Max__c,How_many_bedrooms_you_want_in_New_Home__c,Community__c,Date__c,(Select id,Connection__c,Community__c,RecordTypeId,I_plan_to_purchase_a_home__c,How_did_you_hear_about_us__c,When_do_you_plan_to_move__c,Current_Residence__c,Price_Range_Min__c,Price_Range_Max__c,How_many_bedrooms_you_want_in_New_Home__c from Traffic__r Where RecordTypeId =:RecordTypeId AND  Connection__c=: opp) from Connection__c where id=:opp];
        Opportunity op = Database.query(connectionQuery);
        Traffic__c trafficrec= new Traffic__c();
        trafficrec.Connection__c=op.id;
        if(Schema.sObjectType.Traffic__c.fields.RecordTypeId.isUpdateable())
            trafficrec.RecordTypeId=RecordTypeId;
        //List<Traffic__c> rlist=[Select id,Connection__c,Community__c,RecordTypeId,Date__c from Traffic__c Where RecordTypeId =:RecordTypeId AND  Connection__c=: opp];
        //String trafficfields2 = ' SELECT ' + String.join( trafficFieldNames, ',' ) + ' FROM ' + TrafficDescribeResult.getName() +' WHERE Connection__c =\''+ opp+'\' And RecordTypeId = \''+RecordTypeId+'\'';
        //Traffic__c rlist = Database.query(trafficfields2);
        if(op.Traffic__r.size()>0)
        {
            trafficrec.id=op.Traffic__r[0].id;
            Message='Reg card has been successfully updated!';
        }
        else
            Message='Reg card has been successfully created!';
        if(Schema.sObjectType.Traffic__c.fields.I_plan_to_purchase_a_home__c.isUpdateable())
            trafficrec.I_plan_to_purchase_a_home__c=op.I_plan_to_purchase_a_home__c;
        if(Schema.sObjectType.Traffic__c.fields.How_did_you_hear_about_us__c.isUpdateable())
            trafficrec.How_did_you_hear_about_us__c=op.How_did_you_hear_about_us__c;
        if(Schema.sObjectType.Traffic__c.fields.When_do_you_plan_to_move__c.isUpdateable())
            trafficrec.When_do_you_plan_to_move__c=op.When_do_you_plan_to_move__c;
        if(Schema.sObjectType.Traffic__c.fields.Current_Residence__c.isUpdateable())
            trafficrec.Current_Residence__c=op.Current_Residence__c;
   /*     if(Schema.sObjectType.Traffic__c.fields.Price_Range_Min__c.isUpdateable())
            trafficrec.Price_Range_Min__c=op.Price_Range_Min__c;
        if(Schema.sObjectType.Traffic__c.fields.Price_Range_Max__c.isUpdateable())
            trafficrec.Price_Range_Max__c=op.Price_Range_Max__c;  */
        if(Schema.sObjectType.Traffic__c.fields.How_many_bedrooms_you_want_in_New_Home__c.isUpdateable())
            trafficrec.How_many_bedrooms_you_want_in_New_Home__c=op.How_many_bedrooms_you_want_in_New_Home__c;
        if(Schema.sObjectType.Traffic__c.fields.Community__c.isUpdateable())
            trafficrec.Community__c = op.Community__c;
        if(Schema.sObjectType.Traffic__c.fields.Date__c.isUpdateable())
            trafficrec.Date__c=op.Date__c;
        try
        {
            if(Schema.sObjectType.Traffic__c.isUpdateable() || Schema.sObjectType.Traffic__c.isCreateable())
                upsert trafficrec;
            return Message;
        }
        catch(Exception e){
            return e.getMessage();
        }
    }
   /* public static void cardValidation(List<Registration_Card__c> regvalues){
        Set<Id> oppIds = new Set<Id>();
        for(Registration_Card__c r: regvalues){
            oppIds.add(r.Connection__c);  
        }  
        List<Registration_Card__c> rlist=[Select id,Connection__c from Registration_Card__c Where Connection__c != null AND  Connection__c IN: oppIds];
        Map<Id, Registration_Card__c> ConnectionsRegCards = New Map<Id, Registration_Card__c>();
        for(Registration_Card__c rexist : rlist){ 
            ConnectionsRegCards.Put(rexist.Connection__c, rexist);       
        }    
        for(Registration_Card__c r : regvalues){
            if(!String.IsEmpty(r.Connection__c) && ConnectionsRegCards.ContainsKey(r.Connection__c))
                r.addError('Can\'t have duplicate Cards on Connection');                   
        }
    }
    public static void trafficCreation(List<Registration_Card__c> regvalues){
        List<Traffic__c> registredTraffic = New List<Traffic__c>();
        String RecordTypeId = Schema.SObjectType.Traffic__c.getRecordTypeInfosByDeveloperName().get('Registered').getRecordTypeId();
        for(Registration_Card__c card: regvalues)
        {
            registredTraffic.add(New Traffic__c(Connection__c = card.Connection__c, Community__c  = card.Community__c, Count__c = 1,RecordTypeId = RecordTypeId));
        }
        if(!registredTraffic.IsEmpty())
            Insert registredTraffic;
   }  }*/
     @AuraEnabled
    public static Opportunity getOppRec(String oppId){
     Opportunity OppRec=[Select Id,Name,Status__c,stageName,Date__c from Opportunity where Id=:oppId];
        System.debug('@@@'+OppRec);
        return OppRec;
    }
    
}