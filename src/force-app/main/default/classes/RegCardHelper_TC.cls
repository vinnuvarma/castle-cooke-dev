//Test Class for Trigger : RegCardValidation

@isTest
Private class RegCardHelper_TC{

    public static testMethod void reg(){
         Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Homebuyer').getRecordTypeId();
       
       Account hom = new Account(LastName ='Test Account',FirstName = 't',personEmail = 'testAccount@gmail.com',RecordTypeId = devRecordTypeId );
       insert hom;
        
        //Inserting Account of type 'Agencies'
        List<Account> acc = TestDataUtil.createAccounts(1,Schema.SObjectType.Account.getRecordTypeInfosByName().get('Agency').getRecordTypeId());
        insert acc; 
        String acctId = acc.size()>0 && acc[0].id!=null ? acc[0].id : '';
        
        //Inserting Agent  
        List<Contact> con = TestDataUtil.contactsCreation(1);
        insert con; 
        String contId = con.size()>0 && con[0].id!= null ? con[0].id : '';
        
         List<Division__c> div = TestDataUtil.divisionCreation(1);
        div[0].name = 'test division';
        Insert div;

        
         List<Community__c> com = TestDataUtil.communityCreation(1);
        com[0].name = 'test community';
        com[0].Division__c=div[0].Id;
        Insert com;
        
        //Inserting Connection
        List<opportunity> opp = TestDataUtil.opportunitiesCreation(1);
         opp[0].Community__c=com[0].Id;
         opp[0].AccountId=hom.Id;
        insert opp;
        
        opp[0].StageName = 'Lead';
        update opp;
        String opptId = opp.size()>0 && opp[0].id!=null ? opp[0].id : '';
        //Inserting Registration Card
     //   List<Registration_Card__c> reg1 = TestDataUtil.createReg(3,acctId, opptId,'');
     //   insert reg1;
        
        RegCardHelper.getOppRec(opp[0].Id);
        RegCardHelper.doRegCard(opp[0].Id);
        
        //Updating Registration Card
      //  try{update reg1;}
      //  catch(Exception e){}
    }
}