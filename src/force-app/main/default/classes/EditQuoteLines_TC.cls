@IsTest
public class EditQuoteLines_TC
{
    public static testmethod void UnitTest()
    {
        Id RecordTypeIdContact = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Homebuyer').getRecordTypeId();
        
        //Creating PersonAccount records 
        Account newPersonAccount = new Account();        
        newPersonAccount.FirstName = 'Fred';
        newPersonAccount.LastName = 'Smith';
        newPersonAccount.RecordTypeid = RecordTypeIdContact;  
        newPersonAccount.PersonEmail =  'testaccount@testemail.com';  
        //new changes for package installation 
        //commented because asking pardot to install
        /*newPersonAccount.Community_ID__pc = '722,977'; 
newPersonAccount.Division_ID__pc = '711';
newPersonAccount.State_ID__pc = 'az,co';   
newPersonAccount.Date_of_Appointment__pc = System.Today();
newPersonAccount.Time_of_Visit__pc= '2am'; 
newPersonAccount.Price_Range__pc = '$100,000 - $300,000';   */
        newPersonAccount.PersonLeadSource = 'Website';
        insert newPersonAccount;
        
        List<Division__c> div = TestDataUtil.divisionCreation(1);
        Insert div;
        
        List<Community__c> com = TestDataUtil.communityCreation(1);
        com[0].Division__c = div[0].Id;
        Insert com;
        system.debug('com:'+com);    system.debug('com::'+com[0].Id);
        
        Product2 ModelProduct = TestDataUtil.modelProductCreation();
        ModelProduct.Community__c = com[0].Id;
        Update ModelProduct;
        PricebookEntry ModelEntry = [select Id, name from PricebookEntry where isActive = True and Product2Id =: ModelProduct.Id];
        
        
        Product2 lotProduct = TestDataUtil.lotProductCreation();
        lotProduct.Community__c = com[0].Id;
        Update lotProduct;
        PricebookEntry LotEntry = [select Id, name from PricebookEntry where isActive = True and Product2Id =: lotProduct.Id];
        
        List<Product2> OptionProducts = TestDataUtil.optionProductCreation(1);
        
        List<Product2> modelOptionsProduct = TestDataUtil.modelOptionCreation(1);
        
        Set<Id> ProductIds = New Set<Id>();
        Map<Id,Id> PriceEntries = New Map<Id,Id>(); 
        List<Product2> ModelOptions = New List<Product2>();
        Id RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Model Options').getRecordTypeId();
        for(Product2 Prod: OptionProducts)
        {
            Product2 Pdt = New Product2();
            Pdt = Prod.Clone();
            Pdt.RecordTypeId = RecordTypeId;
            Pdt.Model__c = ModelProduct.Id;
            Pdt.Option__c = Prod.Id;
            Pdt.IsActive = True;
            Pdt.Category__c = 'Fireplace';
            ModelOptions.add(Pdt);
        }
        Insert ModelOptions;
        
        Id PackageRecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Package').getRecordTypeId();
        Product2 packagePdt = New Product2();
        packagePdt.name='packagepro';
        packagePdt.RecordTypeId=PackageRecordTypeId;
        packagePdt.Community__c=com[0].Id;
        packagePdt.IsActive=true;
        insert packagePdt;
        
        Model_Package__c mp= new Model_Package__c();
        mp.Model__c=ModelProduct.Id;
        mp.Package__c=packagePdt.id;
        insert mp;system.debug('mp:'+mp);
        
        PricebookEntry ModelEntries = [select Id, name from PricebookEntry where isActive = True and Product2Id =: ModelOptions[0].Id];
        PriceBook2 pb2=new PriceBook2(Id = Test.getStandardPricebookId(), ISActive = True);
        update pb2;
        
        List<Opportunity> opp = TestDataUtil.opportunitiesCreation(1);
        opp[0].Community__c = com[0].Id;
        opp[0].Division__c = div[0].Id; 
        opp[0].AccountId = newPersonAccount.Id;
        opp[0].Created_from_Form__c = true;
        Insert opp;
        
        
        List<Quote> quot  = TestDataUtil.quotesCreation(1);
        quot[0].OpportunityId = opp[0].id;quot[0].Community__c = com[0].Id;quot[0].Division__c = div[0].Id; quot[0].PriceBook2Id = Test.getStandardPricebookId();
        Insert quot;
        
        QuoteLineItem LotLine = New QuoteLineItem();
        LotLine.QuoteId = quot[0].Id;
        LotLine.PricebookEntryId = LotEntry.Id;
        LotLine.Quantity = 1;
        LotLine.UnitPrice = 2;
        LotLine.Product2Id = lotProduct.Id;
        Insert LotLine;
        
        QuoteLineItem ModelLine = New QuoteLineItem();
        ModelLine.QuoteId = quot[0].Id;
        ModelLine.PricebookEntryId = LotEntry.Id;
        ModelLine.Quantity = 1;
        ModelLine.UnitPrice = 2;
        ModelLine.Product2Id = ModelProduct.Id;
        ModelLine.Type__c='Model';
        Insert ModelLine;
        
        quot[0].Lot__c = lotProduct.Id;
        //quot[0].Model__c = ModelProduct.Id;
        Update quot[0];
        
        QuoteLineItem modelOptionsLine = New QuoteLineItem();
        modelOptionsLine.QuoteId = quot[0].Id;
        modelOptionsLine.PricebookEntryId = LotEntry.Id;
        modelOptionsLine.Quantity = 1;
        modelOptionsLine.UnitPrice = 2;
        modelOptionsLine.Product2Id = modelOptionsProduct[0].Id;
        modelOptionsLine.Required_By__c = ModelLine.Id;
        Insert modelOptionsLine;
        
        List<QuoteLineItem> quotline = New List<QuoteLineItem>();
        quotline.add(modelOptionsLine);
        
        QuoteLineItem modelOptionsLine01 = New QuoteLineItem();
        modelOptionsLine01.QuoteId = quot[0].Id;
        modelOptionsLine01.PricebookEntryId = LotEntry.Id;
        modelOptionsLine01.Quantity = 1;
        modelOptionsLine01.UnitPrice = 2;
        modelOptionsLine01.Product2Id = modelOptionsProduct[0].Id;
        modelOptionsLine01.Required_By__c = ModelLine.Id;
        modelOptionsLine01.Cancelled__c = true;
        Insert modelOptionsLine01;
        
        QuoteLineItem modelOptionsLine02 = New QuoteLineItem();
        modelOptionsLine02.QuoteId = quot[0].Id;
        modelOptionsLine02.PricebookEntryId = LotEntry.Id;
        modelOptionsLine02.Quantity = 1;
        modelOptionsLine02.UnitPrice = 2;
        modelOptionsLine02.Product2Id = modelOptionsProduct[0].Id;
        modelOptionsLine02.Required_By__c = ModelLine.Id;
        modelOptionsLine02.Cancelled__c = true;
        Insert modelOptionsLine02;
        
        
        list<EditQuoteLines.Wapperclass> wcc= new list<EditQuoteLines.Wapperclass>();
        
        EditQuoteLines.Wapperclass wc= new EditQuoteLines.Wapperclass();
        wc.Quantity=1;
        wc.ProductDetails=modelOptionsProduct[0];
        wcc.add(wc);
        
        List<QuoteLineItem> selectedOptions=  [Select Id, Product2Id, Product2.Name,  Notes__c, Cancelled__c, Product2.ProductCode, ListPrice, UnitPrice, Quantity, Subtotal, Discount, TotalPrice, Group__c, Group__r.Name from QuoteLineItem where QuoteId =: quot[0].ID and Required_By__c =: ModelLine.Id and Product2.recordtype.developername='Model_Options' Order by Group__r.createddate Asc Nulls Last];
        system.debug('selectedOptions:'+selectedOptions);
        
        quot[0].Model__c = ModelProduct.Id;
        Update quot[0];
        
        List<QuoteLineItem> ModelLines = New List<QuoteLineItem>();
        for(Product2 pdt: ModelOptions)
        {
            QuoteLineItem OptionLine = New QuoteLineItem();
            OptionLine.QuoteId = quot[0].Id;
            OptionLine.PricebookEntryId = ModelEntries.Id;
            OptionLine.Quantity = 3;
            OptionLine.UnitPrice = pdt.Price__c;
            OptionLine.Product2Id = Pdt.Id;
            OptionLine.Required_By__c = ModelLine.Id;
            ModelLines.add(OptionLine);
            
            QuoteLineItem OptionLine1 = New QuoteLineItem();
            OptionLine1.QuoteId = quot[0].Id;
            OptionLine1.PricebookEntryId = ModelEntries.Id;
            OptionLine1.Quantity = 3;
            OptionLine1.UnitPrice = pdt.Price__c;
            OptionLine1.Product2Id = Pdt.Id;
            OptionLine1.Required_By__c = ModelLine.Id;
            ModelLines.add(OptionLine1);
        }
        Insert ModelLines;
        
        Quote_Group__c GroupLine = New Quote_Group__c();
        GroupLine.Name = 'Group1';
        Insert GroupLine;
        
        ModelLines[0].Group__c = GroupLine.Id;
        Update ModelLines[0];
        
        QuoteLineItem Qtline = ModelLines[0].Clone();
        Qtline.Group__c = Null;
        Insert Qtline;
        
        
        
        //   String Lot = '[{"Checkbox": true, "Discount": 0, "ProductDetails": {"Community__c": "'+quot[0].Community__c+'", "Id": "'+lotProduct.Id+'", "Name": "'+lotProduct.Name+'", "Price__c": '+lotProduct.Price__c+', "ProductCode": "'+lotProduct.ProductCode+'", "RecordType": {"DeveloperName": "Lot", "Id": "'+lotProduct.recordtypeId+'"}, "RecordTypeId": "'+lotProduct.recordtypeId+'", "Status__c": "Open"}, "Quantity": 1, "UnitPrice": '+lotProduct.Price__c+'}]';   
        String Lot = '[{"Checkbox": true, "Discount": 0, "ProductDetails": {"Community__c": "'+quot[0].Community__c+'", "Id": "'+lotProduct.Id+'", "Name": "'+lotProduct.Name+'", "Price__c": '+lotProduct.Price__c+', "ProductCode": "'+lotProduct.ProductCode+'", "RecordType": {"DeveloperName": "Lot", "Id": "'+lotProduct.recordtypeId+'"}, "RecordTypeId": "'+lotProduct.recordtypeId+'", "Status__c": "Open"}, "Quantity": 1, "UnitPrice": '+lotProduct.Price__c+'}]';
        String Model = '[{"Checkbox": true, "Discount": 0, "ProductDetails": {"Community__c": "'+quot[0].Community__c+'", "Id": "'+ModelProduct.Id+'", "Name": "'+ModelProduct.Name+'", "Price__c": '+ModelProduct.Price__c+', "ProductCode": "'+ModelProduct.ProductCode+'", "RecordType": {"DeveloperName": "Model", "Id": "'+ModelProduct.recordtypeId+'"}, "RecordTypeId": "'+ModelProduct.recordtypeId+'", "Status__c": "Open"}, "Quantity": 1, "UnitPrice": '+ModelProduct.Price__c+'}]';
        String Option = '[{"Checkbox": true, "Discount": 0, "ProductDetails": {"Category__c": "STR", "Community__c": "'+quot[0].Community__c+'", "Id": "'+ModelOptions[0].Id+'", "Name": "'+ModelOptions[0].Name+'", "Price__c": '+ModelOptions[0].Price__c+', "ProductCode": "'+ModelOptions[0].ProductCode+'", "RecordType": {"DeveloperName": "Model_Options", "Id": "'+ModelOptions[0].recordtypeId+'"}, "RecordTypeId": "'+ModelOptions[0].recordtypeId+'"}, "Quantity": 1, "UnitPrice": '+ModelOptions[0].Price__c+'}]';
        // string lot1='[{"Checkbox": true, "Discount": 0, "ProductDetails": {"Community__c": "'+quot[0].Community__c+'", "Id": "'+lotProduct.Id+'", "Name": "'+lotProduct.Name+'", "Price__c": '+lotProduct.Price__c+', "ProductCode": "'+lotProduct.ProductCode+'", "RecordType": {"DeveloperName": "Lot", "Id": "'+lotProduct.recordtypeId+'"}, "RecordTypeId": "'+lotProduct.recordtypeId+'", "Status__c": "Open"}, "Quantity": 1, "UnitPrice": '+lotProduct.Price__c+'}{"Checkbox": true, "Discount": 0, "ProductDetails": {"Community__c": "'+quot[0].Community__c+'", "Id": "'+ModelProduct.Id+'", "Name": "'+ModelProduct.Name+'", "Price__c": '+ModelProduct.Price__c+', "ProductCode": "'+ModelProduct.ProductCode+'", "RecordType": {"DeveloperName": "Model", "Id": "'+ModelProduct.recordtypeId+'"}, "RecordTypeId": "'+ModelProduct.recordtypeId+'", "Status__c": "Open"}, "Quantity": 1, "UnitPrice": '+ModelProduct.Price__c+'}{"Checkbox": true, "Discount": 0, "ProductDetails": {"Category__c": "STR", "Community__c": "'+quot[0].Community__c+'", "Id": "'+ModelOptions[0].Id+'", "Name": "'+ModelOptions[0].Name+'", "Price__c": '+ModelOptions[0].Price__c+', "ProductCode": "'+ModelOptions[0].ProductCode+'", "RecordType": {"DeveloperName": "Model_Options", "Id": "'+ModelOptions[0].recordtypeId+'"}, "RecordTypeId": "'+ModelOptions[0].recordtypeId+'"}, "Quantity": 1, "UnitPrice": '+ModelOptions[0].Price__c+'}]';
        String Lot1 = '[{"Checkbox": true, "Discount": 0, "ProductDetails": {"Community__c": "'+quot[0].Community__c+'", "Id": "'+lotProduct.Id+'", "Name": "'+lotProduct.Name+'", "Price__c": '+lotProduct.Price__c+', "ProductCode": "'+lotProduct.ProductCode+'", "RecordType": {"DeveloperName": "Lot", "Id": "'+lotProduct.recordtypeId+'"}, "RecordTypeId": "'+lotProduct.recordtypeId+'", "Status__c": "Spec"}, "Quantity": 1, "UnitPrice": '+lotProduct.Price__c+'}]';
        system.debug('lottt:'+Lot);
        EditQuoteLines EQL = New EditQuoteLines();
        EditQuoteLines.selectedQuote(quot[0].Id);
        EditQuoteLines.availableLots(quot[0].Id, quot[0].Community__c);
        EditQuoteLines.availableModels(quot[0].Id, quot[0].Community__c);
        //EditQuoteLines.selectedPackage(quot[0].Id,lotProduct.Id);
        //EditQuoteLines.availablePackages(packagePdt.Id ,ModelProduct.Id,com[0].Id);
        EditQuoteLines.insertLotModelPackage(lot ,quot[0].Id);
        EditQuoteLines.insertLotModelPackage(Model ,quot[0].Id);
        EditQuoteLines.insertLotModelPackage(Option ,quot[0].Id);
        //        EditQuoteLines.insertLotModelPackage(Lot1 ,quot[0].Id);
        // EditQuoteLines.selectedOptions(quot[0].Id,wcc);
        // EditQuoteLines.insertLotAndModel(Model,quot[0].Id);
        //  List<EditQuotelines.WapperClass> WapperLines = (List<EditQuotelines.WapperClass>)JSON.deserialize(Option, List<EditQuoteLines.WapperClass>.class);
        // String GroupString = '[{"Checkbox": true, "Discount": 0, "LineDetails": {"Id": "'+QtLine.Id+'", "Cancelled":false, "Discount": 0, "ListPrice": "'+QtLine.ListPrice+'","Product2": {"Id": "'+QtLine.Product2Id+'", "Name": "Test Product", "ProductCode": "'+ModelOptions[0].ProductCode+'", "RecordTypeId": "'+ModelOptions[0].recordtypeId+'"}, "Quantity": 1, "UnitPrice": '+ModelOptions[0].Price__c+'}}]';
        String GroupString = '[{"Checkbox": true, "Discount": 0,"LineDetails": {"Id": "'+QtLine.Id+'", "Cancelled__c":false, "Discount": 0, "UnitPrice":199, "Subtotal":199, "TotalPrice":199, "ListPrice": 34,"Product2": {"Id": "'+QtLine.Product2Id+'", "Name": "Test Product", "ProductCode": "'+ModelOptions[0].ProductCode+'", "RecordTypeId": "'+ModelOptions[0].recordtypeId+'"},"Quantity": 1,"UnitPrice": '+ModelOptions[0].Price__c+'}}]';
        // string linestring  = '[{"GroupName":"--","InnerWapperList":[{"Checkbox":false,"Discount":0,"LineDetails":{"Id":"'+QtLine.Id+'","Cancelled__c":false,"ListPrice":199,"UnitPrice":199,"Quantity":1,"Subtotal":199,"Discount":0,"TotalPrice":199,"Product2":{"Name":"Pro Kit Lite A w/Range-36\" Dual Fuel Range","ProductCode":"'+ModelOptions[0].ProductCode+'","RecordTypeId": "'+ModelOptions[0].recordtypeId+'"}}]';
        String linestring = '[{"GroupName":"--","InnerWapperList":[{"Checkbox": true, "Discount": 0,"LineDetails": {"Id": "'+QtLine.Id+'", "Cancelled__c":false, "Discount": 0, "UnitPrice":199, "Subtotal":199, "TotalPrice":199, "ListPrice": 34,"Product2": {"Id": "'+QtLine.Product2Id+'", "Name": "Test Product", "ProductCode": "'+ModelOptions[0].ProductCode+'", "RecordTypeId": "'+ModelOptions[0].recordtypeId+'"},"Quantity": 1,"UnitPrice": '+ModelOptions[0].Price__c+'}}]';
        string GroupString1='[{"GroupName":"--","InnerWapperList":'+Lot+'}]';
        EditQuoteLines.UpdateLineOptions(GroupString1,QtLine.Id);
        EditQuoteLines.availableOptions(quot[0].Id, quot[0].Community__c, ModelProduct.Id, 'Fireplace','');
        EditQuoteLines.insertOptions(Option, quot[0].Id, ModelProduct.Id);
        EditQuoteLines.ungroupedQuoteLines(quot[0].Id, ModelProduct.Id);
        EditQuoteLines.tieToGroups(quot[0].Id, GroupLine.Id, 'TestSrikanth', GroupString);
        EditQuoteLines.selectedlines(quot[0].Id, ModelLine.Id);
        EditQuoteLines.selectedLot(quot[0].Id, lotProduct.Id);
        EditQuoteLines.selectedModel(quot[0].Id, ModelProduct.Id);
        EditQuoteLines.selectedOptions(quot[0].Id, quotline[0].id);
        //EditQuoteLines.selectedOptions(quot[0].Id, WapperLines);
        EditQuoteLines.updateLine(ModelLines[0]);
        EditQuoteLines.categorypicklist(quot[0].Id, ModelProduct.Id);
        EditQuoteLines.groupPicklist(quot[0].Id);  
        EditQuoteLines.removeFromGroup(ModelLines[0].Id);
        EditQuoteLines.updateOptionsLines(ModelLines, ModelLines[0].Id);
        EditQuoteLines.deleteOptionLine(ModelLines, ModelLines[0].Id);
        EditQuoteLines.deleteModelLine(quot[0].Id, ModelLine);
        EditQuoteLines.deletelotLine(LotLine);
    }
    public static testmethod void UnitTest01()
    {
        
        Id RecordTypeIdContact = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Homebuyer').getRecordTypeId();
        
        //Creating PersonAccount records 
        Account newPersonAccount = new Account();        
        newPersonAccount.FirstName = 'Fred';
        newPersonAccount.LastName = 'Smith';
        newPersonAccount.RecordTypeid = RecordTypeIdContact;  
        newPersonAccount.PersonEmail =  'testaccount@testemail.com';  
        //new changes for package installation 
        //commented because asking pardot to install
        /*
newPersonAccount.Community_ID__pc = '722,977'; 
newPersonAccount.Division_ID__pc = '711';
newPersonAccount.State_ID__pc = 'az,co';   
newPersonAccount.Date_of_Appointment__pc = System.Today();
newPersonAccount.Time_of_Visit__pc= '2am'; 
newPersonAccount.Price_Range__pc = '$100,000 - $300,000';  
*/
        newPersonAccount.PersonLeadSource = 'Website';
        insert newPersonAccount;
        
        List<Division__c> div = TestDataUtil.divisionCreation(1);
        Insert div;
        
        List<Community__c> com = TestDataUtil.communityCreation(1);
        com[0].Division__c = div[0].Id;
        Insert com;
        
        Product2 ModelProduct = TestDataUtil.modelProductCreation();
        ModelProduct.Community__c = com[0].Id;
        Update ModelProduct;
        PricebookEntry ModelEntry = [select Id, name from PricebookEntry where isActive = True and Product2Id =: ModelProduct.Id];
        
        PriceBook2 pb2=new PriceBook2(Id = Test.getStandardPricebookId(), ISActive = True);
        update pb2;
        
        Product2 lotProduct = TestDataUtil.lotProductCreation();
        lotProduct.Community__c = com[0].Id;
        Update lotProduct;
        PricebookEntry LotEntry = [select Id, name from PricebookEntry where isActive = True and Product2Id =: lotProduct.Id];
        
        List<Product2> OptionProducts = TestDataUtil.optionProductCreation(1);
        
        List<Product2> modelOptionsProduct = TestDataUtil.modelOptionCreation(1);
        
        List<Opportunity> opp = TestDataUtil.opportunitiesCreation(1);
        opp[0].Community__c = com[0].Id;
        opp[0].Division__c = div[0].Id; 
        opp[0].AccountId = newPersonAccount.Id;
        opp[0].Created_from_Form__c = true;
        Insert opp;
        
        List<Quote> quot  = TestDataUtil.quotesCreation(1);
        quot[0].OpportunityId = opp[0].id;quot[0].Community__c = com[0].Id;quot[0].Division__c = div[0].Id; quot[0].PriceBook2Id = Test.getStandardPricebookId();
        Insert quot;
        quot[0].Lot__c = lotProduct.Id;
        Update quot[0];
        
        String Lot = '[{"Checkbox": true, "Discount": 0, "ProductDetails": {"Community__c": "'+quot[0].Community__c+'", "Id": "'+lotProduct.Id+'", "Name": "'+lotProduct.Name+'", "Price__c": '+lotProduct.Price__c+', "ProductCode": "'+lotProduct.ProductCode+'", "RecordType": {"DeveloperName": "Lot", "Id": "'+lotProduct.recordtypeId+'"}, "RecordTypeId": "'+lotProduct.recordtypeId+'", "Status__c": "Spec"}, "Quantity": 1, "UnitPrice": '+lotProduct.Price__c+'}]';
        Id quoteRecordType = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Spec Quote').getRecordTypeId();
        
        List<Quote> quot01  = TestDataUtil.quotesCreation(1);
        quot01[0].OpportunityId = opp[0].id;quot01[0].Community__c  = com[0].Id;quot01[0].Division__c   = div[0].Id;quot01[0].PriceBook2Id  = Test.getStandardPricebookId();quot01[0].RecordTypeId = quoteRecordType;quot01[0].Lot__c = lotProduct.Id;quot01[0].Status = 'Draft';
        Insert quot01;
        
        QuoteLineItem LotLine = New QuoteLineItem();
        LotLine.QuoteId = quot01[0].Id;LotLine.PricebookEntryId = LotEntry.Id;LotLine.Quantity = 1;LotLine.UnitPrice = 2;LotLine.Product2Id = lotProduct.Id;LotLine.Type__c = 'Model Options';LotLine.UnitPrice = 20;
        Insert LotLine;
        
        QuoteLineItem LotLine01 = New QuoteLineItem();
        LotLine01.QuoteId = quot01[0].Id;LotLine01.PricebookEntryId = LotEntry.Id;LotLine01.Quantity = 1;LotLine01.UnitPrice = 2;LotLine01.Product2Id = lotProduct.Id;LotLine01.Type__c = 'Model';LotLine01.UnitPrice = 20;
        Insert LotLine01;
        
        QuoteLineItem LotLine02 = New QuoteLineItem();
        LotLine02.QuoteId = quot01[0].Id;LotLine02.PricebookEntryId = LotEntry.Id;LotLine02.Quantity = 1;LotLine02.UnitPrice = 2;LotLine02.Product2Id = lotProduct.Id;LotLine02.Type__c = 'Lot';LotLine02.UnitPrice = 20;
        Insert LotLine02; 
        
        // EditQuoteLines EQL = New EditQuoteLines();
        EditQuoteLines.insertLotModelPackage(Lot ,quot[0].Id);
        //   String Option = '[{"Checkbox": true, "Discount": 0, "ProductDetails": {"Category__c": "STR", "Community__c": "'+quot[0].Community__c+'", "Id": "'+ModelOptions[0].Id+'", "Name": "'+ModelOptions[0].Name+'", "Price__c": '+ModelOptions[0].Price__c+', "ProductCode": "'+ModelOptions[0].ProductCode+'", "RecordType": {"DeveloperName": "Model_Options", "Id": "'+ModelOptions[0].recordtypeId+'"}, "RecordTypeId": "'+ModelOptions[0].recordtypeId+'"}, "Quantity": 1, "UnitPrice": '+ModelOptions[0].Price__c+'}]';
        
        EditQuoteLines EQL = New EditQuoteLines();
        EditQuoteLines.selectedQuote(quot[0].Id);
        EditQuoteLines.availableLots(quot[0].Id, quot[0].Community__c);
        EditQuoteLines.availableModels(quot[0].Id, quot[0].Community__c);
        //    EditQuoteLines.selectedOptions(quot[0].Id, Json.deserialize(Option, List<EditQuoteLines.Wapperclass>))
        
    }
}