public class WetSignature{
    @AuraEnabled
    Public Static String WetSignaturevalidations(String QuoteId, String SelectedType)
    {
        string message = '';
        String QuoteLineError = '';
        Boolean QuoteLineValidation = False;
        try{
            if(QuoteId <> Null && QuoteId <> '')
            {
                Quote  quote = new Quote();
                DescribeSObjectResult quoteDescribeResult = Quote.getSObjectType().getDescribe();
                List<String> QuotefieldNames = new List<String>( quoteDescribeResult.fields.getMap().keySet());
                String quoteQuery = ' SELECT id, Name, Status, OpportunityId, AccountId, Lot__c, Model__c, Cancelled__c, Home_Buyer_Signed_Date__c, Purchase_Agreement_Date__c, Division__c, Community__c from Quote where Id = \''+QuoteId+'\'';
                                   
                quote = Database.query(quoteQuery);
                if(quote.Cancelled__c)
                    message = '<li>Quote is cancelled.</li>';
                if(quote.Lot__c == Null)
                    message +=  '<li>Please select Lot on Quote.</li>';
                if(quote.Model__c == null)
                    message += '<li>Please select Model on Quote.</li>'; 
                if(Quote.AccountId == Null)
                    message += '<li>Please fill the Homebuyer on Quote.</li>';
                if(Quote.OpportunityId == Null) 
                    message += '<li>Please fill the Connection on Quote.</li>';
                
                if(message == Null || message == '')
                    return 'Success';
                else
                {
                    message = 'You can\'t upload the Attachment until you fill the field listed below for the associated Quote:<br/><ul>'+message+'</ul>';
                    return message;
                }        
            }    
            else
            {
                message = 'Sorry, Something went wrong. Please come back from Quote Wet Signature';
                return message;
            }
        }
        catch(Exception e){
             return e.getMessage();
        }
    }
    //Updating the quote after uploading the file
    @AuraEnabled
    Public Static String updateQuote(String QuoteId, String SelectedType)
    {
        try{
            Quote  quote = new Quote();
            if(QuoteId <> Null && QuoteId <> '')
            {
                if(quote.Home_Buyer_Signed_Date__c == null) quote.Home_Buyer_Signed_Date__c = System.Now();
                if(quote.Purchase_Agreement_Date__c == null) quote.Purchase_Agreement_Date__c = System.Today();
                quote.status = 'Approved';
                quote.Id = QuoteId;
                update quote;
                 return 'Success';
            }
            else
                return 'Sorry, something went wrong';   
         }
         catch(Exception e){
             return e.getMessage();
         }
    } 
    //To get signature type picklist values
    @AuraEnabled
    public static List<picklistWrap> SignatureType()
    {
       List<picklistWrap> Type = new List<picklistWrap>();
       Type.add(new picklistWrap('--None--', ''));

       Signature__c sign = Signature__c.getInstance();
       if(sign.Signature_Value1__c <> '' && sign.Signature_Value1__c <> null && sign.Signature_Label1__c <> '' && sign.Signature_Label1__c <> null)
           Type.add(new picklistWrap(sign.Signature_Label1__c, sign.Signature_Value1__c));
       if(sign.Signature_Value2__c <> '' && sign.Signature_Value2__c <> null && sign.Signature_Label2__c <> '' && sign.Signature_Label2__c <> null)
           Type.add(new picklistWrap(sign.Signature_Label2__c,sign.Signature_Value2__c));
       return Type;
    }
    public class picklistWrap {
        @AuraEnabled public string labelval;
        @AuraEnabled public string selectedVal;
        public picklistWrap(string labelval, string selectedVal)
        {
            this.labelval = labelval;
            this.selectedVal = selectedVal;
        }
    }   
     @AuraEnabled
    public static boolean AllowWetsignature(String QuoteId){
        system.debug('QuoteId :::'+QuoteId);
        Quote qte=[select id,name,Lot__c,Model__c from Quote where id=:QuoteId];
        if(qte.Lot__c!=null && qte.Model__c!=null)
            return true;
         else
            return false;
    }
}