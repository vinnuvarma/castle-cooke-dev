// Test class for Controller : ConnectionTriggerHandler

@isTest
private class ConnectionTriggerHandlerTC{
    static testMethod void ConnectionTriggerHandlerMethod(){
       
       Recursive.stopOppUpdateStageTrigger = true;
       
       //Adding Custom Setting for Connection_Prospect_Ranking_Conditions__c
       Connection_Prospect_Ranking_Conditions__c prospectCondition1 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for A Rank - Task1',Subject__c='Thank you Email',Number_Of_Days__c=0,Rank__c='A');
       Connection_Prospect_Ranking_Conditions__c prospectCondition2 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for A Rank - Task2',Subject__c='Call',Number_Of_Days__c=7,Rank__c='A');
       Connection_Prospect_Ranking_Conditions__c prospectCondition3 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for A Rank - Task3',Subject__c='Call/Email',Number_Of_Days__c=14,Rank__c='A');
       Connection_Prospect_Ranking_Conditions__c prospectCondition4 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for A Rank - Task4',Subject__c='Email',Number_Of_Days__c=21,Rank__c='A');
       Connection_Prospect_Ranking_Conditions__c prospectCondition5 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for A Rank - Task5',Subject__c='Call/Email',Number_Of_Days__c=30,Rank__c='A');
       Connection_Prospect_Ranking_Conditions__c prospectCondition6 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for A Rank - Task6',Subject__c='Email/ Evaluate to B',Number_Of_Days__c=45,Rank__c='A');
       
       Connection_Prospect_Ranking_Conditions__c prospectCondition7 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for B Rank - Task1',Subject__c='Auto Email',Number_Of_Days__c=0,Rank__c='B');
       Connection_Prospect_Ranking_Conditions__c prospectCondition8 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for B Rank - Task2',Subject__c='Call',Number_Of_Days__c=14,Rank__c='B');
       Connection_Prospect_Ranking_Conditions__c prospectCondition9 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for B Rank - Task3',Subject__c='Email',Number_Of_Days__c=21,Rank__c='B');
       Connection_Prospect_Ranking_Conditions__c prospectCondition10 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for B Rank - Task4',Subject__c='Call ',Number_Of_Days__c=30,Rank__c='B');
       Connection_Prospect_Ranking_Conditions__c prospectCondition11 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for B Rank - Task5',Subject__c='Email',Number_Of_Days__c=60,Rank__c='B');
       Connection_Prospect_Ranking_Conditions__c prospectCondition12 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for B Rank - Task6',Subject__c='Email',Number_Of_Days__c=90,Rank__c='B');
       Connection_Prospect_Ranking_Conditions__c prospectCondition13 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for B Rank - Task7',Subject__c='Call/Email ',Number_Of_Days__c=120,Rank__c='B');
       Connection_Prospect_Ranking_Conditions__c prospectCondition14 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for B Rank - Task8',Subject__c='Email',Number_Of_Days__c=150,Rank__c='B');
       Connection_Prospect_Ranking_Conditions__c prospectCondition15 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for B Rank - Task9',Subject__c='Email/Evaluate to C',Number_Of_Days__c=180,Rank__c='B');
       
       Connection_Prospect_Ranking_Conditions__c prospectCondition16 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for C Rank - Task1',Subject__c='Email',Number_Of_Days__c=0,Rank__c='C');
       Connection_Prospect_Ranking_Conditions__c prospectCondition17 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for C Rank - Task2',Subject__c='Email',Number_Of_Days__c=14,Rank__c='C');
       Connection_Prospect_Ranking_Conditions__c prospectCondition18 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for C Rank - Task3',Subject__c='Call',Number_Of_Days__c=30,Rank__c='C');
       Connection_Prospect_Ranking_Conditions__c prospectCondition19 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for C Rank - Task4',Subject__c='Email',Number_Of_Days__c=60,Rank__c='C');
       Connection_Prospect_Ranking_Conditions__c prospectCondition20 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for C Rank - Task5',Subject__c='Email',Number_Of_Days__c=90,Rank__c='C');
       Connection_Prospect_Ranking_Conditions__c prospectCondition21 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for C Rank - Task6',Subject__c='Call',Number_Of_Days__c=120,Rank__c='C');
       Connection_Prospect_Ranking_Conditions__c prospectCondition22 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for C Rank - Task7',Subject__c='Email',Number_Of_Days__c=180,Rank__c='C');
       Connection_Prospect_Ranking_Conditions__c prospectCondition23 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for C Rank - Task8',Subject__c='Email',Number_Of_Days__c=270,Rank__c='C');
       Connection_Prospect_Ranking_Conditions__c prospectCondition24 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for C Rank - Task9',Subject__c='Email',Number_Of_Days__c=360,Rank__c='C');
       
       PriceBook2 pb2=new PriceBook2(Id = Test.getStandardPricebookId(), ISActive = True);
       update pb2;
       
       Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Homebuyer').getRecordTypeId();
       
       Account acc = new Account(LastName ='Test Account',FirstName = 't',personEmail = 'testAccount@gmail.com',RecordTypeId = devRecordTypeId );
       insert acc;     
       
       Contact con = new Contact(LastName = 'Test Contact');
       insert con;
       
       Community__c com = New Community__c();
       com.Community_ID__c = '1';
       com.Active__c = true;
       insert com;
   
       Opportunity opp = new Opportunity();
       opp.Name = 'Test001';
       Opp.StageName = 'Lead';
       opp.AccountId = acc.id;
       opp.closeDate = system.today() + 10;
       insert opp;
       
       Task tas = New Task();
       tas.WhatId = opp.id;
       tas.ActivityDate = System.Today();
       tas.Status = 'Open';
       tas.Opportunity_Ranking_Type__c = 'Prospect';
       insert tas;
       
       ConnectionTriggerHandler.isRecursive = true;
       opp.StageName = 'Lead';
       update opp;
       
       Datetime today = System.Today();
       Test.setCreatedDate(opp.Id, today);
       
       
       Quote quote = New Quote(); 
       quote.Name = 'Q-0007';
       quote.OpportunityId = opp.id;  
       insert quote;
       update quote; 
      
    }
    
    static testMethod void ConnectionTriggerHandlerMethod01()
    {
   
      Recursive.stopOppUpdateStageTrigger = true;
      
       Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Homebuyer').getRecordTypeId();
       Account acc = new Account(LastName ='Test Account',FirstName = 't',personEmail = 'testAccount@gmail.com',RecordTypeId = devRecordTypeId );
       insert acc; 
        
      //  Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Homebuyer').getRecordTypeId();
       Account acc1 = new Account(LastName ='Test Account1',FirstName = 'th',personEmail = 'testAccoun1t@gmail.com',RecordTypeId = devRecordTypeId );
       insert acc1; 
       
       Contact con = new Contact(LastName = 'Test Contact');
       insert con;
        
       // To satisfy lead ranking 1 condition
       Opportunity opp = new Opportunity();opp.Name = 'Test';opp.AccountId = acc.id;opp.closeDate = system.today() + 10;opp.StageName = 'Lead';opp.Quote_Created__c = true;opp.Pre_Qual__c = true;opp.Prospect_Rating__c = 'A';opp.stageName = 'Pre-Sale';opp.Prospect_Rating__c = 'B';
       insert opp;
       
       Task tas = New Task();
       tas.WhatId = opp.id;
       tas.ActivityDate = System.Today();
       tas.Status = 'Open';
       insert tas;
       
       opp.Prospect_Rating__c = 'B';
       opp.AccountId = acc1.id;
       Quote quote = New Quote(); 
       quote.Name = 'Q-0008';
       quote.OpportunityId = opp.id;         
       insert quote;
       
      
       update opp;
    }
    
    static testMethod void ConnectionTriggerHandlerMethod02()
    {
  
       Recursive.stopOppUpdateStageTrigger = true;
      
       Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Homebuyer').getRecordTypeId();
       Account acc = new Account(LastName ='Test Account',FirstName = 't',personEmail = 'testAccount@gmail.com',RecordTypeId = devRecordTypeId );
       insert acc;     
       
       Contact con = new Contact(LastName = 'Test Contact');
       insert con;
       
        
       // To satisfy lead ranking 1 condition
       Opportunity opp = new Opportunity();opp.Name = 'Test';opp.AccountId = acc.id;opp.closeDate = system.today() + 10;opp.StageName = 'Lead';opp.Quote_Created__c = true;opp.Pre_Qual__c = true;opp.Probability = 40;opp.Initial_Deposit_Received__c = true;
       insert opp;
       
       Quote quote = New Quote();
       quote.Name = 'Q-0001'; 
       quote.OpportunityId = opp.id;
       quote.Home_Buyer_Signed_Date__c = datetime.newInstance(2014, 9, 15, 12, 30, 0);         
       insert quote;
       
       update opp;
    }
   
    static testMethod void ConnectionTriggerHandlerMethod03()
    {

       Recursive.stopOppUpdateStageTrigger = true;
      
       Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Homebuyer').getRecordTypeId();
       Account acc = new Account(LastName ='Test Account',FirstName = 't',personEmail = 'testAccount@gmail.com',RecordTypeId = devRecordTypeId );
       insert acc;     
       
       Contact con = new Contact(LastName = 'Test Contact');
       insert con;
       
       Contact con1 = new Contact(LastName = 'Test Contact1');
       insert con1;
      
       Opportunity opp = new Opportunity();opp.Name = 'Test';opp.AccountId = acc.id;opp.closeDate = system.today() + 10;opp.StageName = 'Lead';opp.Quote_Created__c = true;opp.Pre_Qual__c = true;opp.Probability = 40;opp.Initial_Deposit_Received__c = true;
       insert opp;       
       
       Quote quote = New Quote(); 
       quote.Name = 'Q-0002'; 
       quote.OpportunityId = opp.id;
       quote.Home_Buyer_Signed_Date__c = datetime.newInstance(2014, 9, 15, 12, 30, 0);         
       insert quote;
       

 /*      Registration_Card__c regcard = New Registration_Card__c();
       regcard.Connection__c = opp.id;
       insert regcard;   */       
    }
      
  /*  static testMethod void ConnectionTriggerHandlerMethodOne(){
    

       Recursive.stopOppUpdateStageTrigger = true;
       
       //Adding Custom Setting for Connection_Prospect_Ranking_Conditions__c
       Connection_Prospect_Ranking_Conditions__c prospectCondition1 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for A Rank - Task1',Subject__c='Thank you Email',Number_Of_Days__c=0,Rank__c='A');
       Connection_Prospect_Ranking_Conditions__c prospectCondition2 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for A Rank - Task2',Subject__c='Call',Number_Of_Days__c=7,Rank__c='A');
       Connection_Prospect_Ranking_Conditions__c prospectCondition3 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for A Rank - Task3',Subject__c='Call/Email',Number_Of_Days__c=14,Rank__c='A');
       Connection_Prospect_Ranking_Conditions__c prospectCondition4 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for A Rank - Task4',Subject__c='Email',Number_Of_Days__c=21,Rank__c='A');
       Connection_Prospect_Ranking_Conditions__c prospectCondition5 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for A Rank - Task5',Subject__c='Call/Email',Number_Of_Days__c=30,Rank__c='A');
       Connection_Prospect_Ranking_Conditions__c prospectCondition6 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for A Rank - Task6',Subject__c='Email/ Evaluate to B',Number_Of_Days__c=45,Rank__c='A');
       
       Connection_Prospect_Ranking_Conditions__c prospectCondition7 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for B Rank - Task1',Subject__c='Auto Email',Number_Of_Days__c=0,Rank__c='B');
       Connection_Prospect_Ranking_Conditions__c prospectCondition8 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for B Rank - Task2',Subject__c='Call',Number_Of_Days__c=14,Rank__c='B');
       Connection_Prospect_Ranking_Conditions__c prospectCondition9 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for B Rank - Task3',Subject__c='Email',Number_Of_Days__c=21,Rank__c='B');
       Connection_Prospect_Ranking_Conditions__c prospectCondition10 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for B Rank - Task4',Subject__c='Call ',Number_Of_Days__c=30,Rank__c='B');
       Connection_Prospect_Ranking_Conditions__c prospectCondition11 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for B Rank - Task5',Subject__c='Email',Number_Of_Days__c=60,Rank__c='B');
       Connection_Prospect_Ranking_Conditions__c prospectCondition12 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for B Rank - Task6',Subject__c='Email',Number_Of_Days__c=90,Rank__c='B');
       Connection_Prospect_Ranking_Conditions__c prospectCondition13 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for B Rank - Task7',Subject__c='Call/Email ',Number_Of_Days__c=120,Rank__c='B');
       Connection_Prospect_Ranking_Conditions__c prospectCondition14 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for B Rank - Task8',Subject__c='Email',Number_Of_Days__c=150,Rank__c='B');
       Connection_Prospect_Ranking_Conditions__c prospectCondition15 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for B Rank - Task9',Subject__c='Email/Evaluate to C',Number_Of_Days__c=180,Rank__c='B');
       
       Connection_Prospect_Ranking_Conditions__c prospectCondition16 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for C Rank - Task1',Subject__c='Email',Number_Of_Days__c=0,Rank__c='C');
       Connection_Prospect_Ranking_Conditions__c prospectCondition17 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for C Rank - Task2',Subject__c='Email',Number_Of_Days__c=14,Rank__c='C');
       Connection_Prospect_Ranking_Conditions__c prospectCondition18 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for C Rank - Task3',Subject__c='Call',Number_Of_Days__c=30,Rank__c='C');
       Connection_Prospect_Ranking_Conditions__c prospectCondition19 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for C Rank - Task4',Subject__c='Email',Number_Of_Days__c=60,Rank__c='C');
       Connection_Prospect_Ranking_Conditions__c prospectCondition20 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for C Rank - Task5',Subject__c='Email',Number_Of_Days__c=90,Rank__c='C');
       Connection_Prospect_Ranking_Conditions__c prospectCondition21 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for C Rank - Task6',Subject__c='Call',Number_Of_Days__c=120,Rank__c='C');
       Connection_Prospect_Ranking_Conditions__c prospectCondition22 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for C Rank - Task7',Subject__c='Email',Number_Of_Days__c=180,Rank__c='C');
       Connection_Prospect_Ranking_Conditions__c prospectCondition23 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for C Rank - Task8',Subject__c='Email',Number_Of_Days__c=270,Rank__c='C');
       Connection_Prospect_Ranking_Conditions__c prospectCondition24 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for C Rank - Task9',Subject__c='Email',Number_Of_Days__c=360,Rank__c='C');
       
       
       Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Homebuyer').getRecordTypeId();
       Account acc = new Account(LastName ='Test Account',FirstName = 't',personEmail = 'testAccount@gmail.com',RecordTypeId = devRecordTypeId );
       insert acc;     
     
       Opportunity opp10= New Opportunity();
       opp10.name = 'test1230';
       opp10.AccountId = acc.id;
       opp10.closeDate = system.today();
       opp10.StageName = 'Lead';
       opp10.lost_opportunity__c = 'no';
       opp10.Why__c = 'Plan Related';
       insert opp10;
       opp10.lost_opportunity__c = 'yes';
       update opp10;
       
       Opportunity oppref = New Opportunity();
       oppref.name = 'test123';
       oppref.AccountId = acc.id;
       oppref.closeDate = system.today();
       oppref.StageName = 'Prospect';
       oppref.lost_opportunity__c = 'yes';
       oppref.Why__c = 'Plan Related';
       insert oppref;
       oppref.lost_opportunity__c = 'no';
       oppref.RegCard_Created__c = true;
       update oppref;
       
       Opportunity oppref1 = New Opportunity();
       oppref1.name = 'test123';
       oppref1.AccountId = acc.id;
       oppref1.closeDate = system.today();
       oppref1.StageName = 'Prospect';
       oppref1.prospect_Rating__c = 'B';
       oppref1.lost_opportunity__c = 'yes';
       oppref1.Why__c = 'Plan Related';
       insert oppref1;
       oppref1.lost_opportunity__c = 'no';
       oppref1.RegCard_Created__c = false;
       update oppref1;
       
       Opportunity opp11= New Opportunity();
       opp11.name = 'test3451';
       opp11.AccountId = acc.id;
       opp11.closeDate = system.today();
       opp11.StageName = 'Prospect';
       opp11.Quote_Created__c = true;
       opp11.Pre_Qual__c = true;
       opp11.Initial_Deposit_Received__c = false;
       insert opp11;
             
       Integer duedays = 4;
      Test.startTest();  
       Opportunity orr = new Opportunity();
       orr.name = 'test345';
       orr.AccountId = acc.id;
       orr.closeDate = system.today();
       orr.StageName = 'Lead';  
       orr.Prospect_Rating__c  = 'A';
       insert orr;
    Test.stopTest();   
       orr.Prospect_Rating__c  = 'B';
       update orr;
    
    }
      static testMethod void ConnectionTriggerHandlerMethod04()
    {

       Recursive.stopOppUpdateStageTrigger = true;
      
       Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Homebuyer').getRecordTypeId();
       Account acc = new Account(LastName ='Test Account',FirstName = 't',personEmail = 'testAccount@gmail.com',RecordTypeId = devRecordTypeId );
       insert acc;     
       
       Contact con = new Contact(LastName = 'Test Contact');
       insert con;
       
       Opportunity orr = new Opportunity();
       orr.name = 'test345';
       orr.AccountId = acc.id;
       orr.closeDate = system.today();
       orr.StageName = 'Lead';  
       orr.Prospect_Rating__c  = 'A';
       insert orr;
       Quote quote = New Quote();
       quote.Name = 'Q-0009';
       quote.OpportunityId = orr.id;
       quote.Home_Buyer_Signed_Date__c = datetime.newInstance(2014, 9, 15, 12, 30, 0);         
       insert quote;
    }*/
    
    static testMethod void ConnectionTriggerHandlerMethod05(){
       

     //  user u = [Select id from User where IsActive = true limit 1];
     // user u1 = [Select id from User where IsActive = true limit 1 offset 2];
       
       Recursive.stopOppUpdateStageTrigger = true;
       
       //Adding Custom Setting for Connection_Prospect_Ranking_Conditions__c
       Connection_Prospect_Ranking_Conditions__c prospectCondition1 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for A Rank - Task1',Subject__c='Thank you Email',Number_Of_Days__c=0,Rank__c='A');
       Connection_Prospect_Ranking_Conditions__c prospectCondition2 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for A Rank - Task2',Subject__c='Call',Number_Of_Days__c=7,Rank__c='A');
       Connection_Prospect_Ranking_Conditions__c prospectCondition3 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for A Rank - Task3',Subject__c='Call/Email',Number_Of_Days__c=14,Rank__c='A');
       Connection_Prospect_Ranking_Conditions__c prospectCondition4 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for A Rank - Task4',Subject__c='Email',Number_Of_Days__c=21,Rank__c='A');
       Connection_Prospect_Ranking_Conditions__c prospectCondition5 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for A Rank - Task5',Subject__c='Call/Email',Number_Of_Days__c=30,Rank__c='A');
       Connection_Prospect_Ranking_Conditions__c prospectCondition6 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for A Rank - Task6',Subject__c='Email/ Evaluate to B',Number_Of_Days__c=45,Rank__c='A');
       
       Connection_Prospect_Ranking_Conditions__c prospectCondition7 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for B Rank - Task1',Subject__c='Auto Email',Number_Of_Days__c=0,Rank__c='B');
       Connection_Prospect_Ranking_Conditions__c prospectCondition8 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for B Rank - Task2',Subject__c='Call',Number_Of_Days__c=14,Rank__c='B');
       Connection_Prospect_Ranking_Conditions__c prospectCondition9 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for B Rank - Task3',Subject__c='Email',Number_Of_Days__c=21,Rank__c='B');
       Connection_Prospect_Ranking_Conditions__c prospectCondition10 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for B Rank - Task4',Subject__c='Call ',Number_Of_Days__c=30,Rank__c='B');
       Connection_Prospect_Ranking_Conditions__c prospectCondition11 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for B Rank - Task5',Subject__c='Email',Number_Of_Days__c=60,Rank__c='B');
       Connection_Prospect_Ranking_Conditions__c prospectCondition12 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for B Rank - Task6',Subject__c='Email',Number_Of_Days__c=90,Rank__c='B');
       Connection_Prospect_Ranking_Conditions__c prospectCondition13 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for B Rank - Task7',Subject__c='Call/Email ',Number_Of_Days__c=120,Rank__c='B');
       Connection_Prospect_Ranking_Conditions__c prospectCondition14 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for B Rank - Task8',Subject__c='Email',Number_Of_Days__c=150,Rank__c='B');
       Connection_Prospect_Ranking_Conditions__c prospectCondition15 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for B Rank - Task9',Subject__c='Email/Evaluate to C',Number_Of_Days__c=180,Rank__c='B');
       
       Connection_Prospect_Ranking_Conditions__c prospectCondition16 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for C Rank - Task1',Subject__c='Email',Number_Of_Days__c=0,Rank__c='C');
       Connection_Prospect_Ranking_Conditions__c prospectCondition17 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for C Rank - Task2',Subject__c='Email',Number_Of_Days__c=14,Rank__c='C');
       Connection_Prospect_Ranking_Conditions__c prospectCondition18 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for C Rank - Task3',Subject__c='Call',Number_Of_Days__c=30,Rank__c='C');
       Connection_Prospect_Ranking_Conditions__c prospectCondition19 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for C Rank - Task4',Subject__c='Email',Number_Of_Days__c=60,Rank__c='C');
       Connection_Prospect_Ranking_Conditions__c prospectCondition20 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for C Rank - Task5',Subject__c='Email',Number_Of_Days__c=90,Rank__c='C');
       Connection_Prospect_Ranking_Conditions__c prospectCondition21 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for C Rank - Task6',Subject__c='Call',Number_Of_Days__c=120,Rank__c='C');
       Connection_Prospect_Ranking_Conditions__c prospectCondition22 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for C Rank - Task7',Subject__c='Email',Number_Of_Days__c=180,Rank__c='C');
       Connection_Prospect_Ranking_Conditions__c prospectCondition23 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for C Rank - Task8',Subject__c='Email',Number_Of_Days__c=270,Rank__c='C');
       Connection_Prospect_Ranking_Conditions__c prospectCondition24 = new Connection_Prospect_Ranking_Conditions__c(Name = 'Follow up schedule for C Rank - Task9',Subject__c='Email',Number_Of_Days__c=360,Rank__c='C');
       
       
       Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Homebuyer').getRecordTypeId();
       Account acc = new Account(LastName ='Test Account',FirstName = 't',personEmail = 'testAccount@gmail.com',RecordTypeId = devRecordTypeId );
       insert acc;     
       
       Contact con = new Contact(LastName = 'Test Contact');
       insert con;       
      
       Community__c com = New Community__c();
       com.Community_ID__c = '1';
       com.Active__c = true;
       insert com;
       
       
       Opportunity opp = new Opportunity();
       opp.Name = 'Test001';
       Opp.StageName = 'Lead';
       opp.AccountId = acc.id;
       opp.closeDate = system.today() + 10;
       opp.Prospect_Rating__c = 'A';
       insert opp;
       
       ConnectionTriggerHandler.isRecursive = true;
       opp.Prospect_Rating__c = 'B';
       opp.StageName = 'Lead';
       update opp;
       
       Datetime today = System.Today();
       Test.setCreatedDate(opp.Id, today);
     
       Quote quote = New Quote();
       quote.Name  = 'Q-0004'; 
       quote.OpportunityId = opp.id;         
       insert quote;
    
       update quote; 
     
    }
    static testMethod void ConnectionTriggerHandlerMethod06(){

        Division__c div=new Division__c();
        div.Name='Phoenix';
        insert div;
        
        Division__c div1=new Division__c();
        div1.Name='Dallas';
        insert div1;
        
        Community__c com = New Community__c();
        com.Community_ID__c = '1';
        com.Division__c = div.Id;
        com.Active__c = true;
        insert com;
        
        Community__c com1 = New Community__c();
        com1.Community_ID__c = '1';
        com.Division__c = div.Id;
        com1.Active__c = true;
        insert com1;        
        
        Id userId = [Select id from User where IsActive = true and Profile.Name <> 'System Administrator' and UserType = 'Standard' Limit 1].Id;
        
/*        Community_Sales_Rep__c csrt=new Community_Sales_Rep__c();
        csrt.Community__c=com.Id;
        csrt.Sales_Rep__c=userId;
        
        Community_Sales_Rep__c csrt1=new Community_Sales_Rep__c();
        csrt1.Community__c=com1.Id;
        csrt1.Sales_Rep__c=userId;
        
        List<Community_Sales_Rep__c> csl=new List<Community_Sales_Rep__c>();
        csl.add(csrt);
        csl.add(csrt1);
        insert csl;
        
        for(Community_Sales_Rep__c cs1:csl){
            cs1.Community__c = com.id;
        }
        update csl;   */     
        
       Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Homebuyer').getRecordTypeId();
       Account a = new Account(LastName ='Test Account',personEmail = 'testAccount@gmail.com',RecordTypeId = devRecordTypeId );
       insert a; 
        
        
        
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'Test002';
        Opp1.StageName = 'Lead';
        opp1.AccountId = a.id;
        opp1.closeDate = system.today() + 10;
        opp1.Prospect_Rating__c = 'A';
        opp1.Division__c = div.Id;
        opp1.Community__c=com.id;
        insert opp1;
        
        opp1.Community__c=com1.id;
     //   Update opp1;
        
    }    
   
}