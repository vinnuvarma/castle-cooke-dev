/*
Class name: ConnectionTriggerHandler
Test Class : ConnectionTriggerHandlerTC

*/
public class ConnectionTriggerHandler{   
    
    public static void conDivisionUpdate(List<Opportunity> newopprecords){
        List<ID> commlist=new List<ID>();
        for(Opportunity op:newopprecords){
            if(op.Community__c <> null) commlist.add(op.Community__c);
        }
        /*  List<Community__c> com=[Select Id,Name,Division__c from Community__c where Id in:commlist];
Map<ID,ID> idmap=new Map<ID,ID>(); 
for(Community__c cm:com){
idmap.put(cm.id,cm.Division__c);
}
*/
        Map<ID,Community__c> comdivmap=new Map<ID,Community__c>([Select Id,Name,Division__c from Community__c where Id in:commlist]);
        
        for(Opportunity op : newopprecords){
            if(op.Community__c <> null && comdivmap.containsKey(op.Community__c))op.Division__c = comdivmap.get(op.Community__c).Division__c;
        }
    }
    
    public static void beforeTrigger(List<Opportunity> newOppRecords,  Map<Id, Opportunity> oldOppRecords){
        Set<Id> contactIds = new Set<Id>();
        
        for(Opportunity oppt : newOppRecords){
            if(oppt.Agent__c <> null) contactIds.add(oppt.Id);          
        }
        Map<Id, Contact> contactMap = new Map<Id, Contact>([select id, AccountId, Name from Contact where Id In: contactIds]);
        List<Pricebook2> pricebookList =  [select id from Pricebook2 where IsStandard = TRUE];
        for(Opportunity optRec : newOppRecords){  
            if(!pricebookList.isEmpty()){
                optRec.PriceBook2Id = pricebookList[0].id;
            }
            
            if(optRec.Agent__c <> null && contactMap.get(optRec.Agent__c) != null && contactMap.get(optRec.Agent__c).AccountId<>NULL){
                optRec.Agency__c = contactMap.get(optRec.Agent__c).AccountId;
            }  
            if(optRec.Agent__c == null) optRec.Agency__c = null;       
        }
    }
    
    public static void OpportunityLeadRanking(Map<Id, Opportunity> newOpportunityRecords, Map<Id, Opportunity> oldOpportunityRecords){
        
        Map<Id, Opportunity> opportunityRecords = new Map<Id, Opportunity>([select id, Community__c, Community__r.Community_ID__c, Created_from_Form__c, Name, StageName, Prospect_Rating__c, OwnerId, Prospect_Rating_Modified_Date__c from Opportunity where Id In: newOpportunityRecords.Keyset()]);       
        Map<Id,Task> oppRecordActivity =  new Map<Id,Task>([Select Id, WhatId ,Subject, ActivityDate, Opportunity_Ranking_Type__c,IsClosed  from Task where ActivityDate >= Today and Opportunity_Ranking_Type__c != null and WhatId In:opportunityRecords.Keyset() and Isclosed = false]);
        Map<Id,Task> oppRankingRecordActivity =  new Map<Id,Task>([Select Id, WhatId ,Subject, ActivityDate, Opportunity_Ranking_Type__c,IsClosed  from Task where Opportunity_Ranking_Type__c != null and WhatId In: opportunityRecords.KeySet() and Isclosed = false]);
        
        Map<string, Connection_Prospect_Ranking_Conditions__c> prospectRankingCondtionRecords = Connection_Prospect_Ranking_Conditions__c.getAll();
        
        List<Task> tasksToInsert = new List<Task>();
        List<Task> tasksToDelete = new List<Task>();
        List<Opportunity> opportunityToUpdate = new List<Opportunity>(); 
        
        for(Opportunity op : opportunityRecords.Values()){
            Opportunity oppty = new Opportunity();
            if(oldOpportunityRecords.isEmpty()){
                oppty.Id = op.Id;
                
                if(op.Prospect_Rating__c <> null && op.Prospect_Rating__c <> '' ){
                    tasksToInsert.addAll(createTasks(op.Prospect_Rating__c, op.OwnerId, op.Id, 0, prospectRankingCondtionRecords)); 
                    oppty.Prospect_Rating_Modified_Date__c = System.Today();
                }
                opportunityToUpdate.add(oppty);                  
            }            
            else{
                Opportunity oldOppty = oldOpportunityRecords.get(op.Id);
                oppty.Id = op.Id; 
                
                if(op.Prospect_Rating__c <> null && op.Prospect_Rating__c <> '' && op.Prospect_Rating__c <> oldOppty.Prospect_Rating__c){
                    List<Task> deleteTasks = new List<Task>();
                    
                    for(Task taskRecords : oppRecordActivity.values()){
                        if(op.Id == taskRecords.WhatId){ //Card 4819 - Code ends
                            if(taskRecords.Opportunity_Ranking_Type__c == 'Prospect') deleteTasks.add(new Task(Id = taskRecords.Id));
                        }
                    }
                    tasksToDelete.addAll(deleteTasks);
                    oppty.Prospect_Rating_Modified_Date__c = System.Today();
                    Integer noOfdays = 0;
                    if(oldOppty.Prospect_Rating_Modified_Date__c <> null)
                        noOfdays = oldOppty.Prospect_Rating_Modified_Date__c.daysBetween(system.Today());                  
                    tasksToInsert.addAll(createTasks(op.Prospect_Rating__c, op.OwnerId, op.Id, noOfdays, prospectRankingCondtionRecords));
                    
                }  
                //If Prospect Rating is null, delete all open pending tasks related to Prospect 
                if(op.Prospect_Rating__c == '' || op.Prospect_Rating__c == null) {                    
                    List<Task> deleteTasks = new List<Task>();
                    for(Task taskRecords : oppRankingRecordActivity.values()){
                        if(op.Id == taskRecords.WhatId ){                
                            if(taskRecords.Opportunity_Ranking_Type__c == 'Prospect'){                                                     
                                deleteTasks.add(new Task(Id = taskRecords.Id));                            
                            }
                        }
                    }
                    tasksToDelete.addAll(deleteTasks);            
                }                    
                opportunityToUpdate.add(oppty); 
                isRecursive = false;
            }
        }
        
        update opportunityToUpdate;   
        delete tasksToDelete;
        insert tasksToInsert;
        
    }
    
    public static List<Task> createTasks(String prospectRating, string OwnerId, string opptyId, Integer noOfDays, Map<string, Connection_Prospect_Ranking_Conditions__c> prospectRankingCondtionRecords){
        List<Task> taskRecords = new List<Task>();
        if(prospectRating <> null || prospectRating <> ''){
            for(Connection_Prospect_Ranking_Conditions__c rank : prospectRankingCondtionRecords.values()){
                Integer duedays = 0;
                if(rank.Number_Of_Days__c <> null) duedays = Integer.valueOf(rank.Number_Of_Days__c);
                system.debug(rank.Rank__c+'*****'+prospectRating);
                if(prospectRating == rank.Rank__c && duedays >= noOfDays){
                    taskRecords.add(new Task(Subject = rank.Subject__c, ActivityDate = System.Today().addDays(duedays), Opportunity_Ranking_Type__c = 'Prospect', OwnerId = OwnerId, WhatId = opptyId));    
                }
            }
        }    
        return taskRecords;
    }
    
    public static boolean isRecursive = true;
    public static boolean homebuyerRole=true;
    public static void createPrimaryContact(Map<Id, Opportunity> opptyRecords,Map<Id, Opportunity> OldMapConnections){
        if(Trigger.isInsert){
            List<OpportunityContactRole> opptyContactRoles = new List<OpportunityContactRole>();
            for(Opportunity op : opptyRecords.values()){
                if(op.ContactId__c <> null && op.ContactId__c <> '')    opptyContactRoles.add(new OpportunityContactRole(OpportunityId = op.Id, ContactId = op.ContactId__c, IsPrimary = true, Role = 'Buyer'));             
            }
            insert opptyContactRoles; 
        }
        if(Trigger.isUpdate && homebuyerRole){
            Set<Id> ConRoleIds=new Set<Id>();
            List<OpportunityContactRole> newContactRoles=new List<OpportunityContactRole>();
            for(Opportunity con:opptyRecords.values()){
                OpportunityContactRole cb=new OpportunityContactRole();
                System.debug('@@@con.Accountid'+con.Accountid);
                 System.debug('@@@@OldMapConnections.get(con.Id).AccountId'+OldMapConnections.get(con.Id).AccountId);
                if(con.Accountid<> NULL && con.AccountId<>OldMapConnections.get(con.Id).AccountId){
                    cb.Contactid=con.ContactId__c;
                    cb.OpportunityId=con.Id; 
                    cb.Role='Buyer';
                    cb.IsPrimary = true;
                    newContactRoles.add(cb);
                    homebuyerRole = false;
                    if(OldMapConnections.get(con.Id).AccountId<>NULL){
                        ConRoleIds.add(OldMapConnections.get(con.Id).ContactId__c);
                    }
                }else if(OldMapConnections.get(con.Id).AccountId<>NULL && con.AccountId== NULL)
                    ConRoleIds.add(OldMapConnections.get(con.Id).ContactId__c);
            }
            List<OpportunityContactRole> deleteCb=new List<OpportunityContactRole>();
            List<OpportunityContactRole> cblist=[Select Id,ContactId,OpportunityId from OpportunityContactRole where ContactId In:ConRoleIds AND Role='Buyer'];
            for(Opportunity con:opptyRecords.values()){
                for(OpportunityContactRole cob:cblist){
                    if(OldMapConnections.get(con.Id).AccountId<>NULL && con.AccountId<> OldMapConnections.get(con.Id).AccountId && (cob.OpportunityId==con.Id && cob.ContactId==OldMapConnections.get(con.Id).ContactId__c)){
                        deleteCb.add(cob);  
                    }                 
                }
            }
           
            delete deleteCb;
            System.debug('@@@@@@'+newContactRoles);
            insert newContactRoles; 
            //homebuyerRole = false;
        }
    }
    
    //Purpose: When connection probability is greater than 50 then update the below
    //To update the Lot Status to Closed
    //To update spec quote Status as Converted 
    Public Static Void productAndSpecQuoteUpdate(List<Opportunity> Opportunities, Map<Id, Opportunity> OldOpportunitiesMap)
    {
        /*  Set<Id> Lots = New set<Id>();
Set<Id> SyncQuotes = New Set<Id>();
for(Opportunity Opp: Opportunities)
{
System.Debug('Surya Opportunities'+Opp.Probability);
if(OldOpportunitiesMap.ContainsKey(Opp.Id) && Opp.Probability >= 50 && OldOpportunitiesMap.get(Opp.Id).Probability < 50 && Opp.StageName <> 'Lost' && Opp.SyncedQuoteId <> Null)
SyncQuotes.add(Opp.SyncedQuoteId);                
}
if(!SyncQuotes.IsEmpty())
{
List<Quote> ApprovedQuotes = New List<Quote>();
ApprovedQuotes = [Select Id, Lot__c from Quote where Id IN: SyncQuotes];
for(Quote Qt: ApprovedQuotes)
{
if(Qt.Lot__c <> Null)
Lots.add(Qt.Lot__c);    
}
if(!Lots.IsEmpty())
{
List<Product2> LotProducts = new List<Product2>();
LotProducts = [Select Id, Status__c from product2 where Id IN: Lots];
for(Product2 Prod: LotProducts)
{
Prod.Status__c = 'Closed';
}
if(!LotProducts.IsEmpty())
Update LotProducts;
//Spec Quotes
ApprovedQuotes = [Select Id, Lot__c from Quote where Lot__c IN: Lots and RecordType.DeveloperName = 'Spec_Quote'];
for(Quote Qt: ApprovedQuotes)
{
System.debug('@@@@@@@@@@@@@@@@@@'+Qt);
Qt.Status = 'Converted';
}
if(!ApprovedQuotes.IsEmpty())
Update ApprovedQuotes;
}
}
*/
        
    }
    //Purpose: To control the access on opportunity from community reps   
 /*   Public static void OpportunitySharingForReps(List<Opportunity> OpportunityList,Map<Id, Opportunity> OldMapOpportunities)
    {
        Map<Id,Id> newCommunityIds = New Map<Id,Id>(); 
        Map<Id,Id> oldCommunityIds = New Map<Id,Id>(); 
        Map<Id, Set<Id>> CommunityWiseReps = New Map<Id,Set<Id>>();
        Set<Id> OpportunityIds = New Set<Id>();  
        for(Opportunity opp: OpportunityList)
        {
            if(Trigger.IsInsert && !String.isEmpty(opp.Community__c))
                newCommunityIds.Put(opp.Id, opp.Community__c);
            else if(Trigger.IsUpdate && OldMapOpportunities.ContainsKey(opp.Id) && opp.Community__c <> OldMapOpportunities.get(opp.Id).Community__c)
            {
                if(!String.isEmpty(opp.Community__c))
                    newCommunityIds.Put(opp.Id, opp.Community__c);
                if(!String.isEmpty(OldMapOpportunities.get(opp.Id).Community__c)) 
                    oldCommunityIds.Put(opp.Id, opp.Community__c); 
            }       
        }
        System.debug('Old Communities'+ oldCommunityIds);
        List<Community_Sales_Rep__c> CommunityReps = new List<Community_Sales_Rep__c>();
        CommunityReps = [Select id,Sales_Rep__c,Community__c from Community_Sales_Rep__c Where Community__c in: newCommunityIds.Values()];
        if(!CommunityReps.IsEmpty())
        {
            for(Community_Sales_Rep__c Reps: CommunityReps)
            {
                if(CommunityWiseReps.ContainsKey(Reps.Community__c))
                {
                    Set<Id> RepIds = New Set<Id>();
                    RepIds.addAll(CommunityWiseReps.get(Reps.Community__c));
                    RepIds.add(Reps.Sales_Rep__c);
                    CommunityWiseReps.Put(Reps.Community__c, RepIds);
                }
                else
                {
                    Set<Id> RepIds = New Set<Id>();
                    RepIds.add(Reps.Sales_Rep__c);
                    CommunityWiseReps.Put(Reps.Community__c, RepIds); 
                }     
            }
        }
        List<OpportunityShare> Oppshare = new List<OpportunityShare>();
        List<OpportunityShare> deleteOppshare = new List<OpportunityShare>();
        if((Trigger.IsInsert || Trigger.IsUpdate) && !CommunityWiseReps.IsEmpty())
        {
            for(Opportunity opp: OpportunityList)
            {
                if(!String.isEmpty(opp.Community__c) && CommunityWiseReps.ContainsKey(opp.Community__c))
                {
                    for(Id RepId: CommunityWiseReps.get(opp.Community__c))
                    {
                        Oppshare.add(New OpportunityShare(Opportunityid = opp.Id, RowCause = 'Manual', OpportunityAccessLevel = 'EDIT', UserOrGroupId = RepId));    
                    }
                }
            }
        }   
        if(!oldCommunityIds.isEmpty())
            deleteOppshare = [Select Id, OpportunityId, OpportunityAccessLevel, UserOrGroupId from OpportunityShare where OpportunityId IN: oldCommunityIds.KeySet() and RowCause = 'Manual'];  
        if(!deleteOppshare.isEmpty())
            Delete deleteOppshare;
        if(!Oppshare.isEmpty())
        {
            System.Debug('Insert Opp Shares**'+Oppshare);
            Insert Oppshare; 
        }           
    }*/
}