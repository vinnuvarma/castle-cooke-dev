global class SyncQuotWithOpportunity implements Queueable{
    public Map<Id,Id> syncMap;        
    public SyncQuotWithOpportunity(Map<Id, Id> syncMap) {
        this.syncMap = syncMap;
    } 
    public void execute(QueueableContext context) {
          SyncQuote(syncMap);
    }   
    @future
    public static void SyncQuote(Map<Id,Id> SyncMap)
    {
        List<Opportunity> Opportunities = New List<Opportunity>();
        Opportunities = [Select Id, SyncedQuoteId from Opportunity where Id IN: SyncMap.KeySet()];
        for(Opportunity Opp: Opportunities)
        {
          if(SyncMap.ContainsKey(Opp.Id))
              Opp.SyncedQuoteId = SyncMap.get(Opp.Id);
        }
        if(!Opportunities.IsEmpty())
          Update Opportunities;
    } 
}