public class SelectionReportController{ 
    public SelectionReportController(){    
         
        quoteId = (Id)ApexPages.currentPage().getParameters().get('qid');
    
        System.Debug('testquot1'+quoteId);
        
        String quoteQuery = ' SELECT Id, Name,Total_Incentive_Amount__c, QuoteNumber, Garage_Swing__c,Lot_Address__c, Options_Total__c, Lot_Premium__c, Base_Price__c, Discount_Amount__c, Net_Amount__c, Division__c, Account.FirstName, Account.LastName, Lot__r.Name, Lot__r.Street__c, Lot__r.City__c, Lot__r.State__c, Lot__r.Postal_Code__c, Model__r.Name, Division__r.Name, Community__r.Name, '+
                                       + ' (SELECT Id, Quantity, ListPrice, TotalPrice, Cancelled__c, Product_Code__c, Product2.Name, Group__r.Name From QuoteLineItems where Type__c = \'Model Options\' Order By Cancelled__c, Group__r.Name, Product2.Name ASC) '
                                       + ' FROM Quote WHERE  Id = \''+quoteId+'\'';
        System.debug('*** QUERY ***'+quoteQuery);
        System.Debug('testquot'+quoteId);
        quote = database.query(quoteQuery);
        
        innerList = new List<InnerClass>();
        ammendaRecordMap = new Map<String, List<QuoteLineItem>>();
        ammendaRecordMap.put('--', new List<QuoteLineItem>());
        List<QuoteLineItem> cancelledItems = new List<QuoteLineItem>();
        List<QuoteLineItem> itemsWithOutAddenda = new List<QuoteLineItem>();
        for(QuoteLineItem ql : quote.QuoteLineItems){
        system.debug('//'+ql.Group__c +ammendaRecordMap+'888'+ammendaRecordMap.containsKey(ql.Group__r.Name));
            List<QuoteLineItem> quoteline = new List<QuoteLineItem>();
            if(ql.Cancelled__c){
                cancelledItems.add(ql); 
            }
            else if(ql.Group__c == null){
                itemsWithOutAddenda.add(ql);
            }            
            else if(ql.Group__c <> null && ammendaRecordMap.containsKey(ql.Group__r.Name)){
                List<QuoteLineItem> quotelines = ammendaRecordMap.get(ql.Group__r.Name);
                quotelines.add(ql);
                ammendaRecordMap.put(ql.Group__r.Name, quotelines);
            }
            else{
                quoteline.add(ql);
                ammendaRecordMap.put(ql.Group__r.Name, quoteline);
            }
        }
        if(!cancelledItems.isEmpty()) ammendaRecordMap.put('Cancelled', cancelledItems);   
        if(itemsWithOutAddenda.isEmpty()) 
           ammendaRecordMap.remove('--');
        else   
           ammendaRecordMap.get('--').addAll(itemsWithOutAddenda);   
        innerList = new List<InnerClass>();
        system.debug('**** *****'+ammendaRecordMap);
        
        for(String s : ammendaRecordMap.KeySet()){             
                 InnerClass ic = new InnerClass();
                 ic.AmmendaSeqNumber = s;
                 ic.quoteLines = ammendaRecordMap.get(s);
                 
                 innerList.add(ic);
        }
        
    }
    public Map<String, List<QuoteLineItem>> ammendaRecordMap{get;set;}
    public Map<Id, QuoteLineItem> quoteLineAttributeMap{get;set;}
    public List<InnerClass> innerList{get;set;}
    public Quote quote{get;set;}
    public string quoteId{get;set;}
    public class InnerClass{
        public string AmmendaSeqNumber{get;set;}
        public List<QuoteLineItem> quoteLines{get;set;}
        public Map<String, List<QuoteLineItem>> categoryLines{get;set;}
        
    }   
}