@istest
public class Incentivesfiltered_TC{
    public static testmethod void test_incentives(){
        
       
  Id accRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Homebuyer').getRecordTypeId();
       
        Account a = new Account();
        a.FirstName = 'Test';
        a.lastname = 'test1';
        a.PersonEmail = 'testtest@gmail.com'; 
        a.RecordTypeId = accRecordType;
        insert a;
        
        Division__c d = new Division__c();
        d.Name = 'divisiontest';
        insert d;
        
        ID rt = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Lot').getRecordTypeId();
        
        Product2 p=new Product2();
        p.Name='Test Product';
        p.recordTypeId=rt;
        p.Price__c=100;
        p.IsActive=true;
        insert p;
        
        Community__c c=new Community__c();
        c.Name='Test Community';
        c.Division__c=d.id;
        insert c;
        
        Pricebook2 pb = new Pricebook2();
        pb.Name = 'Dheeraj';
        insert pb;
        
        opportunity op = new opportunity();
        op.Name = 'test';
        op.AccountID= a.id;
        op.Pricebook2Id = pb.id;
        op.Division__c = d.id;
        op.StageName = 'Prospect';
        op.closeDate=System.today()+10;
        insert op;
                        
        quote q = new quote();
        q.Name = 'sandeep';
        q.pricebook2id = pb.Id;
        q.OpportunityID= op.id;
        q.Division__c=d.id;
        q.Community__c=c.id;
        q.Lot__c=p.id;
        insert q;
        
        quote q1 = new quote();
        q1.Name = 'sandeep';
        q1.OpportunityID= op.id;
        q1.Division__c=d.id;
        q1.Community__c=c.id;
        q1.Lot__c=null;
        insert q1;
        
        quote q2 = new quote();
        q2.Name = 'sandeep';
        q2.OpportunityID= op.id;
        q2.Division__c=d.id;
        q2.Community__c=null;
        q2.Lot__c=null;
        insert q2;
        
        PricebookEntry pbe = new PricebookEntry();
        pbe.Product2Id = p.id;
        pbe.UnitPrice = 100;
        pbe.Pricebook2Id = pb.id;
        pbe.UseStandardPrice=false;
        pbe.isActive = true;
        insert pbe;
                
        QuoteLineItem qli=new QuoteLineItem();
        qli.UnitPrice=20;
        qli.Quantity=10;
        qli.QuoteId=q.id;
        qli.PriceBookEntryID=pbe.id;
        insert qli;         
         
        id rt1 =Schema.SObjectType.Incentive_Master__c.getRecordTypeInfosByName().get('Community Incentive').getRecordTypeId(); 
    
        id rt2 =Schema.SObjectType.Incentive_Master__c.getRecordTypeInfosByName().get('Division Incentive').getRecordTypeId();
             
        Incentive_Master__c im=new Incentive_Master__c();
        im.Name='TestIM';
        im.Division__c=d.id;
        im.RecordTypeId=rt1;
        im.Amount__c=100;
        im.Effective_Date__c=System.Today();
        im.Expiration_date__c=System.Today()+1;
        insert im;
        
        Incentive_Master__c im1=new Incentive_Master__c();
        im1.Name='TestIM1';
        im1.Lot__c=p.id;
        im1.RecordTypeId=rt2;
        im1.Percent__c=30;
         im1.Effective_Date__c=System.Today();
        im1.Expiration_date__c=System.Today()+1;
        insert im1;     
        
        Quote_Incentive__c qi=new Quote_Incentive__c();
        qi.Quote__c=q.id;
        qi.Incentive_Master__c=im.id;
        insert qi;
        
        Quote_Incentive__c qi1=new Quote_Incentive__c();
        qi1.Quote__c=q.id;
        qi1.Incentive_Master__c=im1.id;
        insert qi1;
        
        
        Incentivesfiltered.Incentivesfiltered(q.id);
         Incentivesfiltered.Incentivesfiltered(q1.id);
         Incentivesfiltered.Incentivesfiltered(q2.id);
        String s='[{"checklist":true,"community":"Belmonte Estates","division":"Dallas","incentiveid":"'+im.id+'","lot":"50","quoteid":"'+q.id+'"}]';        
        Incentivesfiltered.save(s);
        String s1='[{"checklist":true,"community":"Belmonte Estates","division":"Dallas","incentiveid":"'+im1.id+'","lot":"50","quoteid":"'+q.id+'"}]';                
        Incentivesfiltered.save(s1);
        string a5='abcd';
        Incentivesfiltered.save(a5);
        
       
    }
}