// Test Class for Controller : WetSignature

@isTest
private class TransferLot_TC{

    public static testmethod void unitTest1() {
        RecordType personAccountRecordType =  [SELECT Id FROM RecordType WHERE Name = 'Homebuyer' and SObjectType = 'Account'];
Account acc = new Account();


 acc.FirstName = 'Fred';
 acc.LastName = 'Smith';
 acc.RecordType = personAccountRecordType;
 insert acc;
       Id pricebookId = Test.getStandardPricebookId();
       Product2 LotProduct = TestDataUtil.lotProductCreation();
       Product2 ModelProduct = TestDataUtil.modelProductCreation();
       
       PriceBookEntry pbe = [select id from PriceBookEntry where Product2Id =: modelProduct.Id limit 1];
        
       List<Community__c> com = TestDataUtil.communityCreation(1);
       com[0].name = 'test community';
       Insert com;
       
       List<Division__c> div = TestDataUtil.divisionCreation(1);
       div[0].name = 'test division';
       Insert div;
       
       list<Opportunity> opp = TestDataUtil.opportunitiesCreation(1);
       opp[0].Name = 'Spec Connection';
       opp[0].Division__c = div[0].Id; 
        opp[0].AccountId = acc.Id;
       Insert opp;
       system.debug('testname'+opp[0]);
        
       Product2 pro1=TestDataUtil.lotProductCreation();
             
       List<Quote> quot  = TestDataUtil.quotesCreation(1);
       quot[0].Lot__c= pro1.id; 
       quot[0].Model__c= ModelProduct.id; 
       quot[0].OpportunityId = opp[0].id;
       quot[0].Community__c = com[0].Id;
       quot[0].Division__c = div[0].Id;
       quot[0].Status='Approved';
       quot[0].Cancel_Reason__c = 'Convert to Spec';
       quot[0].RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Regular').getRecordTypeId();
       quot[0].PriceBook2Id = pricebookId;
       Insert quot;
       quot[0].Status=''; 
        update Quot;
       
       List<QuoteLineItem> quotlineitem = TestDataUtil.quoteslineCreation(1);
       quotlineitem[0].QuoteId=quot[0].id;
       quotlineitem[0].Type__c = 'Model';
       quotlineitem[0].PricebookEntryId =pbe.Id;
       quotlineitem[0].Quantity= 10;
       quotlineitem[0].UnitPrice=23;
       quotlineitem[0].Product2Id = ModelProduct.Id;
       Insert quotlineitem;
       
       List<QuoteLineItem> quotlineitem1 = TestDataUtil.quoteslineCreation(1);
       quotlineitem1[0].QuoteId=quot[0].id;
       quotlineitem1[0].Type__c = 'Lot';
       quotlineitem1[0].PricebookEntryId =pbe.Id;
       quotlineitem1[0].Quantity= 10;
       quotlineitem1[0].UnitPrice=23;
       quotlineitem1[0].Product2Id = LotProduct.Id;
       Insert quotlineitem1;
       
       List<QuoteLineItem> quotlineitem2 = TestDataUtil.quoteslineCreation(1);
       quotlineitem2[0].QuoteId=quot[0].id;
       quotlineitem2[0].Type__c = 'Model Options';
       quotlineitem2[0].PricebookEntryId =pbe.Id;
       quotlineitem2[0].Quantity= 10;
       quotlineitem2[0].UnitPrice=23;
       quotlineitem2[0].Product2Id = ModelProduct.Id;
       Insert quotlineitem2;
       
       TransferLot Tl = New TransferLot();
       TransferLot.lotTransfer(quot[0].Id,LotProduct.Id);
       TransferLot.getCommunity(quot[0].Id);
       
       
       
       Quote quo = [Select id, Lot__c, Community__c,Status, Cancelled__c, Model__c,Division__c, Pricebook2Id, Cancel_Reason__c, OpportunityId, Opportunity.StageName, Opportunity.Probability, Recordtype.Developername,(Select id, Type__c, Product2Id from QuoteLineItems where Type__c = 'Lot' limit 1) from Quote limit 1];
       TransferLot.cancelQuote(quo);
       TransferLot.getCancelTo(quot[0].Id);
       quo.Cancel_Reason__c = 'Revert to Dirt';
       update quo;
       TransferLot.cancelQuote(quo);
       
     }
      public static testmethod void unitTest2() {
        RecordType personAccountRecordType =  [SELECT Id FROM RecordType WHERE Name = 'Homebuyer' and SObjectType = 'Account'];
Account acc = new Account();


 acc.FirstName = 'Fred';
 acc.LastName = 'Smith';
 acc.RecordType = personAccountRecordType;
 insert acc;
       Id pricebookId = Test.getStandardPricebookId();
       Product2 LotProduct = TestDataUtil.lotProductCreation();
       Product2 ModelProduct = TestDataUtil.modelProductCreation();
       
       PriceBookEntry pbe = [select id from PriceBookEntry where Product2Id =: modelProduct.Id limit 1];
        
       List<Community__c> com = TestDataUtil.communityCreation(1);
       com[0].name = 'test community';
       Insert com;
       
       List<Division__c> div = TestDataUtil.divisionCreation(1);
       div[0].name = 'test division';
       Insert div;
       
       list<Opportunity> opp = TestDataUtil.opportunitiesCreation(1);
       opp[0].Name = 'Spec Connection';
       opp[0].Division__c = div[0].Id; 
        opp[0].AccountId = acc.Id;
       Insert opp;
       system.debug('testname'+opp[0]);
        
       Product2 pro1=TestDataUtil.lotProductCreation();
             
       List<Quote> quot  = TestDataUtil.quotesCreation(1);
       quot[0].Lot__c= pro1.id; 
       quot[0].Model__c= ModelProduct.id; 
       quot[0].OpportunityId = opp[0].id;
       quot[0].Community__c = com[0].Id;
       quot[0].Division__c = div[0].Id;
       quot[0].Status='Approved';
       quot[0].Cancel_Reason__c = 'Revert to Dirt';
       quot[0].RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Regular').getRecordTypeId();
       quot[0].PriceBook2Id = pricebookId;
       Insert quot;
       
       
       List<QuoteLineItem> quotlineitem = TestDataUtil.quoteslineCreation(1);
       quotlineitem[0].QuoteId=quot[0].id;
       quotlineitem[0].Type__c = 'Model';
       quotlineitem[0].PricebookEntryId =pbe.Id;
       quotlineitem[0].Quantity= 10;
       quotlineitem[0].UnitPrice=23;
       quotlineitem[0].Product2Id = ModelProduct.Id;
       Insert quotlineitem;
       
       List<QuoteLineItem> quotlineitem1 = TestDataUtil.quoteslineCreation(1);
       quotlineitem1[0].QuoteId=quot[0].id;
       quotlineitem1[0].Type__c = 'Lot';
       quotlineitem1[0].PricebookEntryId =pbe.Id;
       quotlineitem1[0].Quantity= 10;
       quotlineitem1[0].UnitPrice=23;
       quotlineitem1[0].Product2Id = LotProduct.Id;
       Insert quotlineitem1;
       
       List<QuoteLineItem> quotlineitem2 = TestDataUtil.quoteslineCreation(1);
       quotlineitem2[0].QuoteId=quot[0].id;
       quotlineitem2[0].Type__c = 'Model Options';
       quotlineitem2[0].PricebookEntryId =pbe.Id;
       quotlineitem2[0].Quantity= 10;
       quotlineitem2[0].UnitPrice=23;
       quotlineitem2[0].Product2Id = ModelProduct.Id;
       Insert quotlineitem2;
       
       TransferLot Tl = New TransferLot();
       TransferLot.lotTransfer(quot[0].Id,LotProduct.Id);
       TransferLot.getCommunity(quot[0].Id);
       
       
       
       Quote quo = [Select id, Lot__c, Community__c,Status, Cancelled__c, Model__c,Division__c, Pricebook2Id, Cancel_Reason__c, OpportunityId, Opportunity.StageName, Opportunity.Probability, Recordtype.Developername,(Select id, Type__c, Product2Id from QuoteLineItems where Type__c = 'Lot' limit 1) from Quote limit 1];
       TransferLot.cancelQuote(quo);
       TransferLot.getCancelTo(quot[0].Id);
       
     }
}