@istest
public class StandardQuoteController_TC{
    public static testMethod void StandardQuoteControllerTest(){
    
    Id accRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Homebuyer').getRecordTypeId();
       
        Account a = new Account();
        a.FirstName = 'Test';
        a.lastname = 'test1';
        a.PersonEmail = 'testtest@gmail.com'; 
        a.RecordTypeId = accRecordType;
        insert a;
        
        Division__c d = new Division__c();
        d.Name = 'divisiontest';
        d.Active__c=true;
        insert d;
        
      Id rt = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Lot').getRecordTypeId();
                
        Product2 p=new Product2();
        p.Name='Test Product';
        p.recordTypeId=rt;
        p.Price__c=100;
        p.IsActive=true;
        insert p;
        
        Community__c c=new Community__c();
        c.Name='Test Community';
        c.Division__c=d.id;
        c.Active__c=true;
        insert c;
        
        Pricebook2 pb = new Pricebook2();
        pb.Name = 'TestPB';
        insert pb;
        
        opportunity op = new opportunity();
        op.Name = 'test';
         op.Community__c=c.Id;
        op.AccountID= a.id;
        op.Pricebook2Id = pb.id;
        op.Division__c = d.id;
        op.StageName = 'Prospect';
        op.closeDate=System.today()+10;
        insert op;
                        
        quote q = new quote();
        q.Name = 'sandeep';
        q.pricebook2id = pb.Id;
        q.OpportunityID= op.id;
        q.Division__c=d.id;
        q.Community__c=c.id;
        q.Lot__c=p.id;
        insert q;
        
        String quoteID = q.id;
        
        StandardQuoteController.getRecTypeId('Spec Quote');
        StandardQuoteController.RegRequired(op.Id);
        StandardQuoteController.UpdateSaleDate(q.Id);
    }
}