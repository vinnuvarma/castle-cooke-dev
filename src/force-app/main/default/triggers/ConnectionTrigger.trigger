/*Created By: Apostle Tech
Puspose: Populate PricebookId in Opportunty from standard pricebook  (Before Insert)
Purpose: Create a followup task based on Prospect Ranking in Connection.
*********************************************************************************************/

trigger ConnectionTrigger on Opportunity (before insert,before update, after insert,after update, before delete) {
    Trigger_Handler__mdt TriggerHandler = Utilityclass.checkmetadataValues('ConnectionTrigger');
    Trigger_Handler__mdt TriggerHandlerCSR = Utilityclass.checkmetadataValues('CommunitiesRepAccessFromOpp');
    if(TriggerHandler!=null && TriggerHandler.IsActive__c){
        if(Trigger.isBefore && TriggerHandler.isBefore__c && !Trigger.isDelete){
            ConnectionTriggerHandler.beforeTrigger(Trigger.New, Trigger.OldMap); 
            if(trigger.isInsert  && TriggerHandler.isinsert__c){
                ConnectionTriggerHandler.conDivisionUpdate(Trigger.new);
            }
        }
        else if(Trigger.isAfter && TriggerHandler.isAfter__c){
            if(trigger.isInsert  && TriggerHandler.isinsert__c){
                ConnectionTriggerHandler.OpportunityLeadRanking(Trigger.NewMap, new Map<Id, Opportunity>());
                ConnectionTriggerHandler.createPrimaryContact(Trigger.NewMap,Trigger.OldMap);                
            }    
            else if(trigger.isUpdate && TriggerHandler.isUpdate__c && ConnectionTriggerHandler.isRecursive){
                ConnectionTriggerHandler.OpportunityLeadRanking(Trigger.NewMap, Trigger.OldMap);
            }
        }
    }
    if(Trigger.IsAfter && Trigger.IsUpdate){ 
        CreateOpportunityContactRoles.CobuyerCreation(Trigger.new,Trigger.OldMap);
        CreateOpportunityContactRoles.AgentCreation(Trigger.new,Trigger.OldMap);
        ConnectionTriggerHandler.productAndSpecQuoteUpdate(Trigger.New, Trigger.OldMap);
        ConnectionTriggerHandler.createPrimaryContact(Trigger.NewMap,Trigger.OldMap);    
        
    }
    if(Trigger.IsAfter && Trigger.IsUpdate){
        // CreateCobuyer.CobuyerCreation(Trigger.new,Trigger.OldMap);
    }   
    //To control the access on opportunity from community reps    
  //  if(Trigger.IsAfter && TriggerHandlerCSR<> null && TriggerHandlerCSR.IsActive__c && ((Trigger.IsUpdate && TriggerHandlerCSR.IsUpdate__c) || (Trigger.IsInsert  && TriggerHandlerCSR.IsInsert__c)))
   //     ConnectionTriggerHandler.OpportunitySharingForReps(Trigger.New, Trigger.OldMap);    
    
}