trigger QuoteTriggerHandler on Quote (Before Insert, After Update) {
    Trigger_Handler__mdt triggerhandler = Utilityclass.checkmetadataValues('QuoteTriggerHandler'); 
    if(Trigger.isBefore && Trigger.isInsert && triggerhandler <> null && triggerhandler.IsActive__c  && triggerhandler.IsBefore__c && triggerhandler.IsInsert__c)
        QuoteHelperClass.quotePriceBookUpdate(Trigger.New);
    if(Trigger.isAfter && Trigger.isUpdate && triggerhandler <> null && triggerhandler.IsActive__c  && triggerhandler.IsAfter__c && triggerhandler.IsUpdate__c){
        QuoteHelperClass.quoteSyncUpdate(Trigger.New, Trigger.OldMap);   
        QuoteHelperClass.CancelOldQuotes(Trigger.newMap,Trigger.OldMap);
    }
}