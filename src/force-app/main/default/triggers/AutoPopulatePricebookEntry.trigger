trigger AutoPopulatePricebookEntry on Product2 (after insert) {
    List<PricebookEntry> Pblist = New List<PricebookEntry>();
    Pricebook2 sPB = New Pricebook2();
    if(!Test.isRunningTest())
        sPB =  [select id,name from Pricebook2 where isStandard = true limit 1];
    else
        sPB.id =  Test.getStandardPricebookId();
    for (Product2 newProduct: Trigger.new) 
    {
        if(newProduct.Price__c == Null)
            newProduct.Price__c = 0;
        Pblist.add(new PricebookEntry(Pricebook2Id=sPB.id,Product2Id=newProduct.ID, UnitPrice = newProduct.Price__c, IsActive=TRUE, UseStandardPrice=FALSE));
    }
    Insert Pblist;
   /*  if(Trigger.isDelete && Trigger.isBefore){
        ProductTriggerHandler.deleteProduct(Trigger.old,Trigger.oldMap);
    } */
}