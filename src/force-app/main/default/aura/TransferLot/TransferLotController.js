({
    doinit: function (component, event, helper) {
        helper.CancelTypes(component, event);
    },   
    upsertQuote: function (component, event, helper) {
        component.set("v.Spinner", true);
        var q = component.get("v.ProductID");
        if(!$A.util.isEmpty(q)){
        helper.CancelOldQuote(component, event);
        helper.SaveQuote(component, event);      
        }else{
             var resultsToast = $A.get("e.force:showToast");
                        resultsToast.setParams({
                            "title": "Error",
                            "message": "Please select lot and try again.",
                            "type":"Error"
                        });
                        resultsToast.fire();
          			    window.setTimeout(
                        $A.getCallback(function() {
                        component.set("v.Spinner", false);
                        }), 1000
                    );
            
        }
          
    },
    lookupEvent: function(component, event)
    {
        var LotId = event.getParam("lookuprecordId");
        component.set("v.ProductID",LotId);
    },
    openModel: function(component, event, helper) {
        helper.loadAllComponents(component, event, helper);
        component.set("v.isOpen", true);
    },
    closeModel: function(component, event, helper) {
        component.set("v.isOpen", false);
    },
    
    hideerror: function(component, event, helper) {
        document.getElementById("errordiv").style.display = "none"; 
    }
})