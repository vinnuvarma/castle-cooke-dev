({
	loadAllComponents :function(component,event,helper) {
		var id = component.get("v.recordId");
        var actiongetCommunityid  = component.get("c.getCommunity");
        actiongetCommunityid.setParams({
            "QuoteId": id
        }); 
        actiongetCommunityid.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS") { 
                component.set("v.quote", a.getReturnValue());
                console.log('** Quote ID **'+component.get("v.quote.NSE_Quote_ID__c"));
                if(component.get("v.quote.Cancelled__c") == true){
                    component.set("v.Custommessage","Lot transfer is not supported for Cancelled Quote.");
                    document.getElementById("errordiv").style.display = "block";
                }
            } 
        });        
        $A.enqueueAction(actiongetCommunityid);
    },
     CancelTypes: function (component, event) {
        var id = component.get("v.recordId");
        var useraction = component.get("c.getCancelTo");
        useraction.setParams({
            "QuoteId": id
        });
        useraction.setCallback(this, function (response) {
            if (response.getState() === "SUCCESS" && component.isValid()) {
                component.set("v.Cancel", response.getReturnValue());
            }
        });
        $A.enqueueAction(useraction);
    },
    SaveQuote: function(component, event, helper) {
        var quoteid = component.get("v.recordId");
        var productId = component.get("v.ProductID");
        $A.util.removeClass(component.find("spinnerId"), 'slds-hide');
        if(productId != null && productId != '')
        {
            var actionsetlot = component.get("c.lotTransfer");
            actionsetlot.setParams({
                "QuoteId": quoteid, 
                "NewLotId": productId
            }); 
            actionsetlot.setCallback(this, function(a) {
                $A.util.addClass(component.find("spinnerId"), 'slds-hide');
                var state = a.getState();
                if (state === "SUCCESS") { 
                    component.set("v.Custommessage",a.getReturnValue());
                    if(!a.getReturnValue().includes("updated successfully"))
                    {
                        document.getElementById("errordiv").style.display = "block";
                    }
                    else
                    {
                        console.log('Inside else');
                        var dismissActionPanel = $A.get("e.force:closeQuickAction");
                        dismissActionPanel.fire();
                        var resultsToast = $A.get("e.force:showToast");
                        resultsToast.setParams({
                            "title": "Success",
                            "message": "Succesfully transferred Lot",
                            "type":"Success"
                        });
                        resultsToast.fire();
                        var resultVal = a.getReturnValue();
                        console.log('resultVal:::::TransferLot'+resultVal);
                        var resultId= resultVal.includes("updated successfully")? resultVal.split('-')[1] : '';
                        console.log('resultId:::'+resultId);
                        var navEvt = $A.get("e.force:navigateToSObject");
                        navEvt.setParams({
                            "recordId": resultId
                        });
                        navEvt.fire();
                        $A.get('e.force:refreshView').fire();
                        component.set("v.isOpen", false);
                    }
                } 
            });        
            $A.enqueueAction(actionsetlot);
        }
        else
        {
            component.set("v.Custommessage","Please select a Product");
            document.getElementById("errordiv").style.display = "block";        }    
        
    },
        CancelOldQuote : function(component, event) {
        var useraction = component.get("c.cancelQuote"); 
        console.log('QuoteDetails::'+JSON.stringify(component.get("v.quote")));  
            var q = component.get("v.quote");
        useraction.setParams({"QuoteDetails" : component.get("v.quote")});
        useraction.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){ 
                var resultVal = response.getReturnValue();
                 console.log('resultVal::'+resultVal);
                component.set("v.isOpen", false);
                var resultId= resultVal.includes("Success")? resultVal.split('-')[1] : '';
                 console.log('resultId ::'+(resultId));
   			if(resultVal.includes("Success"))
                { 
                   // alert('hi'+resultVal);
                    //this.showSuccessToast(component,event,"Successfully Cancelled the Quote");
                    $A.get('e.force:refreshView').fire();
                    if(resultId!=''){
                      /*  var navEvt = $A.get("e.force:navigateToSObject");
                        navEvt.setParams({
                            "recordId": resultId
                        });
                        navEvt.fire(); */
                        console.log('Cancelled Successfully'+resultId);
                    }
                    
                }
                else
                {
                    this.showErrorToast(component,event,"Something went wrong");
                }    
                
   			}
        });
        $A.enqueueAction(useraction);    
	},
    // this function is to show the error message
    showErrorToast : function(component, event, Message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error!",
            "type": "error",
            "message": Message
        });
        toastEvent.fire();
    },
    // this function is to show the success message
    showSuccessToast : function(component, event, Message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "type": "success",
            "message": Message
        });
        toastEvent.fire();
    }
})