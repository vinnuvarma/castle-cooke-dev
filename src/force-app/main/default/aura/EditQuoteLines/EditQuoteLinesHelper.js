({
	quoteDetail : function(component, event) {
        var useraction = component.get("c.selectedQuote");        
        useraction.setParams({"RecordId" : component.get("v.recordId")});
        useraction.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){ 
                var resultVal = response.getReturnValue();
                component.set("v.QuoteDetails",resultVal);
                if(resultVal.Status != 'Cancelled'){
                    component.set("v.showFOrm",true);
                }
            }
        });
        $A.enqueueAction(useraction);    
	}
})