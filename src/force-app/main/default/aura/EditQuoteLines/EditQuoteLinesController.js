({
   doinit : function(component, event, helper) { 
        helper.quoteDetail(component,event);  
   },
   selectionReport : function(component, event, helper) { 
        var quoteId = component.get("v.recordId");
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/apex/PrintScenarioEmail?Id="+quoteId
        });
        urlEvent.fire();  
   }, 
   sendWithDocusign : function(component, event, helper) { 
        var quoteId = component.get("v.recordId");
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/apex/DocuSign?Id="+quoteId
        });
        urlEvent.fire();  
   }, 
})