({
    doInit : function (component, event, helper) {
         var action = component.get("c.RegRequired");
        action.setParams({
            "conId" : component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){ 
                var resultVal = response.getReturnValue();
                //component.set("v.hasError",resultVal.RegRequired);
                if(resultVal.hasError){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : 'Error',
                        message: resultVal.message,
                        duration:'500',
                        type: 'error',
                    });
                    toastEvent.fire();  
                    $A.get("e.force:closeQuickAction").fire();
                }
            }
        });
        $A.enqueueAction(action);
        var action = component.get("c.getRecTypeId");
        action.setParams({
            "recordTypeLabel" : "Regular"
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){ 
                var resultVal = response.getReturnValue();
                component.set("v.QuoteRecordType",resultVal);
            }
        });
        $A.enqueueAction(action);

    },

    onSubmit  : function(component, event, helper) {

        var eventFields = event.getParam("fields");
        console.log(JSON.stringify(eventFields));

    },
    handleSuccess : function(component, event, helper) {
        var params = event.getParams();        
         var action = component.get("c.UpdateSaleDate");
        action.setParams({
            "quotid" : params.response.id
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){ 
              console.log('Close Date Updated Successfully');
            }
        });
        $A.enqueueAction(action);
        
        
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Success Message',
            message: 'Regular quote has been created successfully!',
            duration:'500',
            type: 'success',
        });
        toastEvent.fire();      
        console.log('response*****',params.response.id);
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": params.response.id
        });
        navEvt.fire();
    },
    handleCancel : function(component, event, helper) {
        event.preventDefault();
        $A.get("e.force:closeQuickAction").fire();
    },
    errorInformation : function(component, event, helper) {
        var eventName = event.getName();
        var eventDetails = event.getParams().error.message;
        console.log('Error Event received' + eventName);


    }
})