({
	doInit : function(component, event, helper) {
        var ObjectName=component.get("v.sObjectName");
        var Errormsg;
        if(ObjectName=="Opportunity")
            Errormsg='Please create Connection from Homebuyer record page';
        else if(ObjectName=="Quotelineitem")
             Errormsg='You cannot manually delete Quotelineitem Records';
        else{
            if(ObjectName=="Quote")
              Errormsg='Please create Quote from Connection record page';   
        }
        var ToastErrmsg=$A.get("e.force:showToast");
        ToastErrmsg.setParams({
            title:'Error',
            message:Errormsg,
            duration:' 5000',
            type:'error'
        });
        ToastErrmsg.fire();
        // After displaying the message navigate to the sobject home page
        var homeEvent=$A.get("e.force:navigateToObjectHome");
        homeEvent.setParams({
          "scope": ObjectName  
        });
        homeEvent.fire();
	}
})