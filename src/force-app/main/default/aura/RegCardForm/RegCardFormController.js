({
    doInit : function(component,event,helper){
        
        var action=component.get('c.getOppRec');
        action.setParams({
            'oppId':component.get('v.recordId')
        });
        action.setCallback(this,function(response){
            var state=response.getState();         
            if(state=='SUCCESS'){             
                component.set("v.OppRecord",response.getReturnValue());               
                var objectInfo = response.getReturnValue();  console.log('objectInfo:'+response.getReturnValue().Date__c);
                if(objectInfo.StageName == 'Closed Won'){                   
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": 'error',
                        "message": "Reg card will create only under Lead Status."
                    });
                    toastEvent.fire(); 
                    $A.get("e.force:closeQuickAction").fire();
                }
                else{
                    if(!objectInfo.Date__c)
                    {
                        var datefieldd = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");                      
                        component.set("v.today",datefieldd);                       
                    }
                    else{
                        component.set("v.today",objectInfo.Date__c);   
                    }
                    var cmpTarget = component.find('showForm');
                    $A.util.removeClass(cmpTarget, 'slds-hide');
                    component.set("v.showSpinner",false);
                }
            }
        });
        $A.enqueueAction(action); 
        
    },
    onsubmitaction : function(component,event,helper){
        var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD")
        event.preventDefault();
        var eventFields = event.getParam("fields");
        if(today >= eventFields["Date__c"] && (!!eventFields["Date__c"])){
            component.find('recordEditForm').submit(eventFields);
        } else {
            if(!eventFields["Date__c"]){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "type": 'error',
                    "message": "Please fill the Date."
                });
                toastEvent.fire(); 
            }
            if(today < eventFields["Date__c"]){
                console.log('in');
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "type": 'error',
                    "message": "Date cannot be greater than today."
                });
                toastEvent.fire();     
            }
        }        
    },
    handleSuccess:function(component){
        var action=component.get('c.doRegCard');
        action.setParams({
            'opp':component.get('v.recordId')
        });
        action.setCallback(this,function(response){
            var state=response.getState();
            if(state==='SUCCESS'){
                component.find('notifLib').showToast({
                    "title": "Success!",
                    "variant":"success",
                    "message": response.getReturnValue()
                });
                $A.get('e.force:refreshView').fire();
                $A.get("e.force:closeQuickAction").fire();
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": component.get("v.recordId")
                });
                navEvt.fire();  
            }
             $A.get('e.force:refreshView').fire();
        });
        $A.enqueueAction(action);
    },
    cancel :function(component){
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get("v.recordId")
        });
        navEvt.fire();
        
    }
    
})