({
    userYearChange : function(component, event, userId, divisionId, year){
        var action = component.get("c.turboList");
        var usId = component.get("v.userId");
        var divId = component.get("v.divisionId");
        var selectedYear = component.get("v.yearChange");
        action.setParams({userId : usId, divisionId : divId, year : year});
        action.setCallback(this, function(response){
            var state = response.getState();
            var err = response.getError();
            if(state==='SUCCESS'){
                component.set("v.turboGoalList" , response.getReturnValue());  
            } 
            if(state==='ERROR'){
                console.log("Error: " + err);
            }
            component.set("v.spinner", false);
        });
        $A.enqueueAction(action);
    },
    saveMethod : function(component, event, turbList){
        var action = component.get("c.turboListSave");
        action.setParams({ turboList : turbList});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state==='SUCCESS'){
                component.set("v.turboGoalList" , response.getReturnValue());
            }
            component.set("v.spinner", false);
        });
        $A.enqueueAction(action);
    }
})