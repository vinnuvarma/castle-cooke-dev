({
    conUpdate : function(component, event, helper) {
        var payload = event.getParams();
        var action=component.get('c.UpdateOpportunity');           
        action.setParams({"recId": payload.response.id});
        action.setCallback(this,function(resp){
            var state=resp.getState();
            if(state==='SUCCESS'){
                console.log("Successfully created Opportunity");
                
            }
        }); 
        $A.enqueueAction(action); 	
    },
    getCOI : function(component, event, helper) {
        var useraction = component.get("c.getComInterest");        
        useraction.setParams({"recId" : component.get("v.recordId")});
        useraction.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){ 
                var resultVal = response.getReturnValue();
                if(resultVal.length!=0)                   
                    component.set("v.isCoi",true);
                component.set("v.Coi",resultVal);
                component.set("v.isSpinner",false);
            }
        });
        $A.enqueueAction(useraction);  
    },
    createOpp:function(component, event, helper) {
        var selectval=component.get("v.selectedValue");
        if(!selectval){
         component.set("v.isSpinner",false);   
        }
        else{
        var useraction = component.get("c.createOpportunity");        
        useraction.setParams({
            "accountId" : component.get("v.recordId"),
            "selectedInterest" : component.get("v.selectedValue")
        });
        useraction.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){ 
                var resultVal = response.getReturnValue();
                if(component.get("v.SaveReg")===true){
                    component.set("v.OpportunityId",resultVal);          
                    component.set("v.SaveReg",false);
                    component.set("v.isReg",true); 
                    component.set("v.isSpinner",false);
                }
                else{
                    var toastmesg = $A.get("e.force:showToast");
                    toastmesg.setParams({
                        "title": "Success!",
                        "type": 'Success',
                        "message": "Opportunity has been created successfully."
                    });
                    toastmesg.fire();
                    component.set("v.isSpinner",false);
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": resultVal
                    });
                    navEvt.fire();   
                }
            }
        });
        $A.enqueueAction(useraction);
        }
    }
})