({
    ChangePop:function(component, event, helper) {
        component.set("v.SaveReg",true);
    },
    Cancel :function(component){
        $A.get("e.force:closeQuickAction").fire();   
    },  
    OnSaveReg : function(component,event,helper) {
        component.set("v.isSpinner",true);
        component.set("v.SaveReg",true);
        if(component.get("v.isCoi")){
            helper.createOpp(component,event,helper);   
        }
    }, 
    SubmitCoi : function(component,event,helper){
        component.set("v.isSpinner",true);
       helper.createOpp(component,event,helper);     
    },
    handleSubmit : function(component,event,helper) {    
        event.preventDefault();      
        var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD"); 
        var eventFields = event.getParam("fields"); 
        if(component.get("v.isReg")==false){   
            var community=eventFields["Community__c"];
            var saleDate=component.get("v.EstDate");
            if(community==null ||community==''){ 
                component.set("v.isSpinner",false);
                var toastEventmesg = $A.get("e.force:showToast");
                toastEventmesg.setParams({
                    "title": "Error!",
                    "type": 'Error',
                    "message": "Please fill the Community!"
                });
                toastEventmesg.fire();
                if(component.get("v.SaveReg")==true)
                    component.set("v.SaveReg",false);   
            }
            else if(saleDate==null ||saleDate==''){ 
                component.set("v.isSpinner",false);
                var toastEventmesg = $A.get("e.force:showToast");
                toastEventmesg.setParams({
                    "title": "Error!",
                    "type": 'Error',
                    "message": "Please fill the Estimated Sale Date!"
                });
                toastEventmesg.fire();
                if(component.get("v.SaveReg")==true)
                    component.set("v.SaveReg",false);   
            }
                else if(saleDate<=today){ 
                    component.set("v.isSpinner",false);
                    var toastEventmesg = $A.get("e.force:showToast");
                    toastEventmesg.setParams({
                        "title": "Error!",
                        "type": 'Error',
                        "message": "Estimated Sale Date Should be greater than today's Date!"
                    });
                    toastEventmesg.fire();
                    if(component.get("v.SaveReg")==true)
                        component.set("v.SaveReg",false);   
                }
                    else{
                        eventFields["Name"] =community+'-'+today+'-'+component.get("v.recordId");  
                        eventFields["CloseDate"] = component.get("v.EstDate");  
                        eventFields["StageName"]='Lead';                       
                        component.find('recordEditForm').submit(eventFields); 
                    }       
        }
        
    },
    handleSuccess:function(component,event, helper){ 
        helper.conUpdate(component, event, helper) ;
        var x=component.get("v.SaveReg");
        var payload = event.getParams();        
        if(component.get("v.SaveReg")===true){
            component.set("v.OpportunityId",payload.response.id);          
            component.set("v.SaveReg",false);
            component.set("v.isReg",true);
            component.set("v.isSpinner",false);
        }
        else{              
            var toastmesg = $A.get("e.force:showToast");
            toastmesg.setParams({
                "title": "Success!",
                "type": 'Success',
                "message": "Opportunity has been created successfully."
            });
            toastmesg.fire();
            component.set("v.isSpinner",false);
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": payload.response.id
            });
            navEvt.fire();  
            /*  window.setTimeout(
                $A.getCallback(function() {
                    $A.get('e.force:refreshView').fire();  
                }), 2000
            ); */
            
        }          
    },  
    doinit:function(component,event, helper){
        helper.getCOI(component, event, helper);
        
    }
})