({
    doInit:function(component){ console.log('hiii');
                               var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
                               component.set('v.today', today);
                               // component.find('Traffic_Date__c').set('v.value', today);
                               var action=component.get("c.dogetRecordtypeid");
                               action.setCallback(this,function(response){
                                   var state=response.getState();
                                   if(state==='SUCCESS'){
                                       component.set('v.recordtypeid',response.getReturnValue());   
                                   }
                               });
                               $A.enqueueAction(action);
                              },
    onsubmitaction : function(component,event,helper){
        var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD")
        event.preventDefault();
        var eventFields = event.getParam("fields");
        if(today >= eventFields["Date__c"] && (!!eventFields["Date__c"])){
            component.find('recordEditForm').submit(eventFields);
        } else {
            if(!eventFields["Date__c"]){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "type": 'error',
                    "message": "Please fill the Date."
                });
                toastEvent.fire(); 
            }
            if(today < eventFields["Date__c"]){
                console.log('in');
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "type": 'error',
                    "message": "Date cannot be greater than today."
                });
                toastEvent.fire();    
            }        
        }
    },
    handleSuccess : function(component, event) {
        var payload = event.getParams().response;
        var navService = component.find("navService");
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": payload.id,
            "slideDevName": "detail"
        });
        navEvt.fire();
    },
    cancel :function(component, event){    
        var homeEvent = $A.get("e.force:navigateToObjectHome");
        homeEvent.setParams({
            "scope": "Traffic__c"
        });
        homeEvent.fire();
    }    
})