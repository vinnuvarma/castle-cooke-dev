({
    quoteDetail : function(component, event) { console.log('In quoteDetail:::Doinit:::Function');
        var useraction = component.get("c.selectedQuote");        
        useraction.setParams({"RecordId" : component.get("v.recordId")});
        useraction.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){ 
                var resultVal = response.getReturnValue();
                console.log('>>>resultVal:::'+JSON.stringify(resultVal)+'>>>');
                component.set("v.QuoteDetails",resultVal);
                this.LotProduct(component,event);
                this.ModelProduct(component,event);
              //  this.PackageProduct(component,event);
                this.OptionProduct(component,event);
            }
        });
        $A.enqueueAction(useraction);    
    },
    SelectLot : function(component, event) { //alert('Hello in select lot 1st');
        component.set("v.showModalSpinner",true);
        var useraction = component.get("c.availableLots");        
        useraction.setParams({"RecordId" : component.get("v.recordId"),
                              "CommunityId" : component.get("v.QuoteDetails.Community__c")
                             });
        useraction.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){ //alert('Hello in select lot');
                var result = response.getReturnValue();
                console.log(result[0]);console.log('result::::'+JSON.stringify(result));
                component.set("v.AvailableProduct",result);
                component.set("v.showModalSpinner",false); 
            }
        });
        $A.enqueueAction(useraction);	
    },
    SelectModel : function(component, event) {
        component.set("v.showModalSpinner",true); 
        var useraction = component.get("c.availableModels");        
        useraction.setParams({"RecordId" : component.get("v.recordId"),
                              "CommunityId" : component.get("v.QuoteDetails.Community__c")
                             });
        useraction.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){ 
                var result = response.getReturnValue();
                component.set("v.AvailableProduct",result);
                component.set("v.showModalSpinner",false); 
                console.log(result);
            }
        });
        $A.enqueueAction(useraction);	
    },
  /*  SelectPackage : function(component, event) {
        var useraction = component.get("c.availablePackages");        
        useraction.setParams({"RecordId" : component.get("v.recordId"),
                              "ModelId" : component.get("v.Model.Product2Id"),
                              "CommunityId" : component.get("v.QuoteDetails.Community__c")
                             });
        useraction.setCallback(this, function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                var result = response.getReturnValue();
                component.set("v.AvailableProduct",result);
                console.log(result);
            }
        });
        $A.enqueueAction(useraction);
    },  */ 
    SelectOptions : function(component, event) {
        component.set("v.showModalSpinner",true); 
        var useraction = component.get("c.availableOptions");        
        useraction.setParams({"RecordId" : component.get("v.recordId"),
                              "CommunityId" : component.get("v.QuoteDetails.Community__c"),
                              "ModelId" : component.get("v.Model.Product2Id"),
                              "Category" : component.get("v.SelectedCategory"),
                              "ProductName" : component.get("v.ProductName")
                             });
        useraction.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){ 
                var result = response.getReturnValue();
                console.log(result);
                component.set("v.AvailableOPtions",result);

                component.set("v.showModalSpinner",false);
            }
        });
        $A.enqueueAction(useraction);	
    },
    LotProduct : function(component, event) {
       // var lotid=component.get("v.QuoteDetails.Lot__c");console.log('lotid::::'+lotid+'>>>');
       // console.log('QuoteDetails.Lot__c:::'+component.get("v.QuoteDetails.Lot__c")+'>>>');
        var useraction = component.get("c.selectedLot");        
        useraction.setParams({"RecordId" : component.get("v.recordId"),
                              "LotId" : component.get("v.QuoteDetails.Lot__c")
                             });
        useraction.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                component.set("v.Lot",result);
                
            }else{console.log('ERROR')}
        });
        $A.enqueueAction(useraction);	
    },
    ModelProduct : function(component, event) {
        var useraction = component.get("c.selectedModel");        
        useraction.setParams({"RecordId" : component.get("v.recordId"),
                              "ModelId" : component.get("v.QuoteDetails.Model__c")
                             });
        useraction.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){ 
                var result = response.getReturnValue();
                component.set("v.Model",result);
            }
        });
        $A.enqueueAction(useraction);	
    },
 /*   PackageProduct : function(component, event){
        
        var useraction = component.get("c.selectedPackage");        
        useraction.setParams({"RecordId" : component.get("v.recordId"),
                              "PackageId" : component.get("v.QuoteDetails.Package__c")
                             });
        useraction.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                component.set("v.Package", result);
            }
        }) 
        $A.enqueueAction(useraction);
    },*/
    OptionProduct : function(component, event) {
        var useraction = component.get("c.selectedOptions");        
        useraction.setParams({"RecordId" : component.get("v.recordId"),
                              "ModelId" : component.get("v.Model.Id")
                             });
        useraction.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){ 
                var result = response.getReturnValue();
                console.log(result);
                component.set("v.Options",result);
                console.log('Options'+JSON.stringify(result));
                this.OptionGroup(component,event);
            }
        });
        $A.enqueueAction(useraction);	
    },
    OptionGroup : function(component, event) {
        var useraction = component.get("c.selectedlines");        
        useraction.setParams({"RecordId" : component.get("v.recordId"),
                              "ModelId" : component.get("v.Model.Id")
                             });
        useraction.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){ 
                var result = response.getReturnValue();
                component.set("v.GroupOPtions",result);
                console.log(result);
            }
        });
        $A.enqueueAction(useraction);	
    },
    UpdateLot : function(component, event) {//alert('Inside update lot');console.log('Inside update lot');
        var useraction = component.get("c.updateLine");        
        useraction.setParams({"Line" : component.get("v.Lot")});
        useraction.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){ 
                var result = response.getReturnValue();
                this.LotProduct(component,event);
            }
        });
        $A.enqueueAction(useraction);	
    },
    UpdateModel : function(component, event) {
        var useraction = component.get("c.updateLine");        
        useraction.setParams({"Line" : component.get("v.Model")});
        useraction.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){ 
                var result = response.getReturnValue();
                this.ModelProduct(component,event);
            }
        });
        $A.enqueueAction(useraction);	
    },
    UpdateOption : function(component, event) {
        var selectedopt = event.getSource().get("v.name");
        var useraction = component.get("c.updateOptionsLines");        
        useraction.setParams({"Lines" : component.get("v.Options"),
                              "LineId" : selectedopt
                             });
        useraction.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){ 
                var result = response.getReturnValue();
                this.OptionProduct(component,event);
            }
        });
        $A.enqueueAction(useraction);	
    },
    GroupOption : function(component, event) {//alert('IN GroupOption');
        var baselines = component.get("v.GroupOPtions");console.log('baselines::::::'+JSON.stringify(baselines));
         var x,y,z,q;
        var bool=false;
        for(x of baselines){
            for(y of x.InnerWapperList){
                z=y.LineDetails.Quantity;
                if(z!=null){
                 q=z.toString();
                if(q.startsWith("-") || q.includes(".") || q=="0"){
                    bool=true;
                    break;
                }  
                }              
            }
            if(bool==true){
                break;
            }
        }
         if(bool==false){
        var selectedopt = event.currentTarget.dataset.datavalue;
        console.log(selectedopt);
        var useraction = component.get("c.UpdateLineOptions");        
        useraction.setParams({"LineItems" : JSON.stringify(baselines),
                              "LineId" : selectedopt
                             });
        useraction.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){ 
                var result = response.getReturnValue();
                if(result == 'Success')
                {	console.log('Hello:::'+component.get("v.OptionsEdit"));
                    this.OptionGroup(component,event);
                    component.set("v.OptionsEdit", false);
                }
                
            }
        });
             $A.enqueueAction(useraction);
         }	
        else{ 
            component.set("v.OptionsEdit", true); 
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "message": "Enter a valid Quantity!",
                "type":"Error"
            });
            toastEvent.fire();
        }
    },
    DeletePopup : function(component, event) {
        component.set("v.isDeletePopup", true);
    },
    DeleteProductNo: function(component, event, helper) {
        component.set("v.isDeletePopup", false);
        if(component.get("v.isOpenedLot")==true)
            component.set("v.isOpenedLot", false);
        if(component.get("v.isOpenedModel")==true)
            component.set("v.isOpenedModel", false);
    },
    DeleteProductYes: function(component, event, helper) {//alert('In DeleteProductYes');
        if(component.get("v.isOpenedLot")==true){ //alert('v.isOpenedLot===true');
            component.set("v.isDeletePopup", false);
            component.set("v.isOpenedLot", false);
            var useraction = component.get("c.deletelotLine");        
            useraction.setParams({"Line" : component.get("v.Lot")});
            useraction.setCallback(this, function(response){
                var state = response.getState(); console.log('state:::::::'+state);
                if(state === "SUCCESS"){ 
                    var result = response.getReturnValue(); console.log('result:::DeleteProductYes'+result);
                    if(result == 'Success')
                    {	 //alert('success');
                        component.set("v.Lot", ""); 
                        this.quoteDetail(component,event);
                    }
                }
                
            });
            $A.enqueueAction(useraction);
        }
        if(component.get("v.isOpenedModel")==true){
            component.set("v.isDeletePopup", false);
            component.set("v.isOpenedModel", false); //console.log('v.Model:::'+JSON.stringify(component.get("v.Model")));
            var useraction = component.get("c.deleteModelLine");        
            useraction.setParams({	"RecordId" : component.get("v.recordId"),
                                  "Line" : component.get("v.Model")
                                 });
            useraction.setCallback(this, function(response){
                var state = response.getState();
                if(state === "SUCCESS"){ 
                    var result = response.getReturnValue();
                    if(result == 'Success')
                    {
                        component.set("v.Model","");
                        this.quoteDetail(component,event);
                    }
                }
            });
            $A.enqueueAction(useraction);
        }
    },
    DeleteLot : function(component, event) {
        component.set("v.isOpenedLot", true);
    },
    DeleteLotNo: function(component, event, helper) {
        component.set("v.isOpenedLot", false);    
    },
    DeleteLotYes: function(component, event, helper) {
        //alert("Hello in delete lot yes");
        component.set("v.isOpenedLot", false);
        var demo=component.get("v.Lot");
        console.log("v.lot:::"+JSON.stringify(demo));
        var useraction = component.get("c.deletelotLine");
        useraction.setParams({"Line" : component.get("v.Lot")});
        useraction.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){ 
                var result = response.getReturnValue();
                if(result == 'Success')
                {
                    component.set("v.Lot", ""); 
                    this.quoteDetail(component,event);
                }
            }
            
        });
        $A.enqueueAction(useraction);
    },
    
    
    
    DeleteModel : function(component, event) {
        component.set("v.isOpenedModel", true);
    },
    DeleteModelNo: function(component, event, helper) {
        component.set("v.isOpenedModel", false);    
    },
    DeleteModelYes: function(component, event, helper) {
       // alert('In DeleteModelYes');
        component.set("v.isOpenedModel", false);
        var useraction = component.get("c.deleteModelLine");     
        console.log("v.recordid:::to class:::"+(component.get("v.recordId")));
       // console.log("v.Model ::::"+JSON.stringify(component.get("v.Model")));
        useraction.setParams({	"RecordId" : component.get("v.recordId"),
                              "Line" : component.get("v.Model")
                             });
        useraction.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){ 
                var result = response.getReturnValue();
                if(result == 'Success')
                {
                    component.set("v.Model","");
                    this.quoteDetail(component,event);
                }
            }
        });
        $A.enqueueAction(useraction);	
    },
 /*   DeletePackage : function(component, event) {
        component.set("v.isOpenedPackage", true);
    }, */
    
    DeleteOption : function(component, event) {
        var selectedopt = event.currentTarget.dataset.datavalue;
        var useraction = component.get("c.deleteOptionLine");        
        useraction.setParams({"LineId" : selectedopt});
        useraction.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){ 
                var result = response.getReturnValue();
                this.OptionGroup(component,event);
            }
        });
        $A.enqueueAction(useraction);	
    },
    InsertLotModelPackage : function(component, event) {
        component.set("v.showModalSpinner",true);
        var baselines = component.get("v.AvailableProduct");
        var useraction = component.get("c.insertLotModelPackage");   console.log('JSON.stringify(baselines):::'+JSON.stringify(baselines));     
        useraction.setParams({"LineItem" : JSON.stringify(baselines),
                              "RecordId" : component.get("v.recordId")
                             });
        useraction.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){ 
                var result = response.getReturnValue(); console.log('result:::InsertLotModelPackage:::'+JSON.stringify(response.getReturnValue()));
                if(result == 'Success')
                {
                    this.quoteDetail(component,event);
                    component.set("v.AvailablePopUp", true);
                    component.set("v.AddModelPopUp", false); 
                    component.set("v.AvailableProduct",''); 
                    component.set("v.showModalSpinner",false);
                } 
               /* else{
                    component.set("v.showModalSpinner",false);
                   // alert('false'+result);
                }   */
            }
        });
        $A.enqueueAction(useraction);	
    },
    CategoryOptions : function(component, event) {
        component.set("v.showModalSpinner",true); 
        var useraction = component.get("c.categorypicklist");        
        useraction.setParams({ "RecordId" : component.get("v.recordId"),
                              "ModelId" : component.get("v.Model.Product2Id")
                             });
        useraction.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){ 
                var result = response.getReturnValue();
                console.log(result);
                component.set("v.Categories",result);
                component.set("v.showModalSpinner",false); 
            }
        });
        $A.enqueueAction(useraction);	
    },
    InsertOptionProducts : function(component, event) {
        component.set("v.showModalSpinner",true);
        var baselines = component.get("v.AvailableOPtions");
        var useraction = component.get("c.insertOptions");        
        useraction.setParams({"LineItem" : JSON.stringify(baselines),
                              "RecordId" : component.get("v.recordId"),
                              "ModelId" : component.get("v.Model.Id")
                             });
        useraction.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){ 
                var result = response.getReturnValue();
                this.quoteDetail(component,event);
                this.SelectOptions(component,event);
                component.set("v.showModalSpinner",false);
            }
        });
        $A.enqueueAction(useraction);	
    },
    SelectionControl : function(component, event) {
        var selectedpro = event.getSource().get("v.name");
        var selectedval = event.getSource().get("v.value"); 
        if(selectedpro != null && selectedval == true) 
        {    
            var optionList = component.get("v.AvailableProduct"); 
            var newlst =[];
            for(var i in optionList){
                console.log(i);
                var option = optionList[i];
                if(option.ProductDetails.Id == selectedpro && option.Checkbox == selectedval)  
                    option.Checkbox = true;
                else
                    option.Checkbox = false;
                newlst.push(option);
            } 
            component.set("v.AvailableProduct",newlst);
        }    
    },
    Groupvalues : function(component, event) {
        component.set("v.showModalSpinner",true);
        var useraction = component.get("c.groupPicklist");        
        useraction.setParams({"RecordId" : component.get("v.recordId")});
        useraction.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){ 
                var result = response.getReturnValue();
                console.log(result);
                component.set("v.GroupTypes",result);
            }
        });
        $A.enqueueAction(useraction);	
    },
    UnGroupedLines : function(component, event) {
        component.set("v.showModalSpinner",true);
        var useraction = component.get("c.ungroupedQuoteLines");        
        useraction.setParams({"RecordId" : component.get("v.recordId"),
                              "ModelId" : component.get("v.Model.Id")
                             });
        useraction.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){ 
                var result = response.getReturnValue();
                console.log(result);
                component.set("v.AvailableOPtions",result);
                component.set("v.showModalSpinner",false); 
            }
        });
        $A.enqueueAction(useraction);	
    },
    TieWithGroups : function(component, event) {
        var baselines = component.get("v.AvailableOPtions");
        var useraction = component.get("c.tieToGroups");        
        useraction.setParams({"RecordId" : component.get("v.recordId"),
                              "GroupId" : component.get("v.GroupName"),
                              "Name" : component.get("v.Type"),
                              "Lines" : JSON.stringify(baselines)
                             });
        useraction.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){ 
                var result = response.getReturnValue();
                if(result == 'Success')
                {
                    component.set("v.GroupName",'');
                    component.set("v.GroupTypes",'');
                    component.set("v.Type",''); 
                    this.Groupvalues(component,event);
                    this.UnGroupedLines(component,event);
                    this.OptionGroup(component,event); 
                    
                }
            }
        });
        $A.enqueueAction(useraction);	
    },
    RemoveFromGroup : function(component, event) {
        var selectedopt = event.currentTarget.dataset.datavalue;
        var useraction = component.get("c.removeFromGroup");        
        useraction.setParams({"LineId" : selectedopt});
        useraction.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){ 
                var result = response.getReturnValue();
                if(result == 'Success')
                {
                    this.OptionGroup(component,event);
                }    
            }
        });
        $A.enqueueAction(useraction);
    },
    CancelSubPopUps : function(component, event) {
        component.set("v.AvailablePopUp", true);
        component.set("v.AddModelPopUp", false); 
        component.set("v.AddOptionsPopUp",false);
       // component.set("v.GropingPopUp",false); 
        component.set("v.Type",''); 
        component.set("v.ProductName",'');
        component.set("v.Categories",'');
        component.set("v.SelectedCategory",'');
        component.set("v.GroupName",'');
        component.set("v.GroupTypes",'');
    },
})