({
    doinit : function(component, event, helper) { 
        
        helper.quoteDetail(component,event);       
    },
    openModel: function(component, event, helper) {
        component.set("v.isOpen", true);
    },
    closeModel: function(component, event, helper) {
        component.set("v.isOpen", false);
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get("v.recordId"),                        
        });
        navEvt.fire();
        window.setTimeout(
            $A.getCallback(function() {          
                $A.get("e.force:refreshView").fire();    
            }), 1000
        );      
    },
    LotEdit: function(component, event, helper) {
        component.set("v.LotEdit", true);
    },
    ModelEdit: function(component, event, helper) {
        component.set("v.ModelEdit", true);
    },
  /*  PackageEdit: function(component, event, helper) {
      component.set("v.PackageEdit", true);
    },*/
    OptionEdit: function(component, event, helper) {
        var selectedopt = event.currentTarget.dataset.datavalue;console.log('selectedopt::11:'+selectedopt);
        var istrue = component.get("v.editoptionbool");
        if(istrue == false){ 
        component.set("v.editoptionbool",true);
        component.set("v.OptionsEdit", true); 
        component.set("v.EditId",selectedopt);
         }
        else{
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "message": "You can't edit two Options at a time!",
                "type":"Error"
            });
            toastEvent.fire();
        }
    }, 
    Cancel: function(component, event, helper) {
        component.set("v.editoptionbool",false); 
        component.set("v.LotEdit", false);
        component.set("v.ModelEdit", false);
        component.set("v.OptionsEdit", false); 
       // component.set("v.PackageEdit", false);
       // component.set('v.newSpinner', true); 
       component.set('v.Spinner',true);
        helper.OptionProduct(component, event, helper);
        window.setTimeout(
            $A.getCallback(function() {console.log('spinner');
                component.set('v.Spinner',false);
            }), 1000
        );
    },
    SaveLot : function(component, event, helper) {
       // alert('hello in savelot');
        helper.UpdateLot(component,event);
        component.set("v.LotEdit", false);
    },
    SaveModel : function(component, event, helper) {
        helper.UpdateModel(component,event);
        component.set("v.ModelEdit", false);
    },
    SaveOption : function(component, event, helper) {
        helper.UpdateOption(component,event);
        component.set("v.OptionsEdit", false);
    },
    GroupNames : function(component, event, helper) {
        helper.Groupvalues(component,event);
        helper.UnGroupedLines(component,event); 
        component.set("v.GroupingPopUp", true);
        component.set("v.AvailablePopUp", false);
    }, 
    GroupOption : function(component, event, helper) {
        component.set("v.editoptionbool",false);
        helper.GroupOption(component,event);
       // component.set("v.OptionsEdit", false); 
    },
    RemoveFromGroup : function(component, event, helper) {
        helper.RemoveFromGroup(component,event);
    }, 
    TieGroups : function(component, event, helper) {
        helper.TieWithGroups(component,event);
    },
    TieGroupsAndClose : function(component, event, helper) {
        helper.TieWithGroups(component,event);
        helper.CancelSubPopUps(component,event); 
    }, 
    AvailableModels : function(component, event, helper) {
        component.set("v.AvailablePopUp", false);
        component.set("v.AddModelPopUp", true);
        component.set("v.Type",'Model'); 
        helper.SelectModel(component,event); 
    },
    AvailableLots : function(component, event, helper) {
        component.set("v.AvailablePopUp", false);
        component.set("v.AddModelPopUp", true);
        component.set("v.Type",'Lot');  
        helper.SelectLot(component,event); 
    },
 /*   AvailablePackages : function(component, event, helper) {
        component.set("v.AvailablePopUp", false);
        component.set("v.AddModelPopUp", true);
        component.set("v.Type",'Package'); 
        helper.SelectPackage(component,event); 
    },*/
    AvailableOptions : function(component, event, helper) {
        component.set("v.ProductName",''); 
        helper.InsertOptionProducts(component,event);
    },
    SearchOptions : function(component, event, helper) {
        helper.SelectOptions(component,event);
    },  
    AvailableCategories : function(component, event, helper) {
        component.set("v.AvailablePopUp", false);
        component.set("v.AddModelPopUp", false);
        component.set("v.AddOptionsPopUp", true);
        helper.CategoryOptions(component,event); 
    },
    CancelSubPopup : function(component, event, helper) {
        helper.CancelSubPopUps(component,event);  
    }, 
    InsertLotModelPackage : function(component, event, helper) {
        helper.InsertLotModelPackage(component,event);  
    },
    InsertOptions : function(component, event, helper) {
        helper.InsertOptionProducts(component,event); 
        helper.CancelSubPopUps(component,event);
    }, 
    handleSelected : function(component, event, helper) {
        helper.SelectionControl(component,event);       
    },
    DeleteLot : function(component, event, helper) {
        helper.DeleteLot(component,event); 
        helper.DeletePopup(component,event);
    },
    DeleteLotYes : function(component, event, helper) {
        helper.DeleteLotYes(component,event);       
    },
    DeleteLotNo : function(component, event, helper) {
        helper.DeleteLotNo(component,event);       
    },
    DeleteModel : function(component, event, helper) {
        helper.DeleteModel(component,event);  
        helper.DeletePopup(component,event);  
    },
    DeleteModelYes : function(component, event, helper) {
        helper.DeleteModelYes(component,event);       
    },
    DeleteModelNo : function(component, event, helper) {
        helper.DeleteModelNo(component,event);       
    },
     DeleteYes : function(component, event, helper) { // lot popup Do you want to delete Lot 
       //	alert('In DeleteYes');
         helper.DeleteProductYes(component,event);       
    },
    DeleteNo : function(component, event, helper) {
        helper.DeleteProductNo(component,event);       
    },
/*    DeletePackage : function(component, event, helper) {
        helper.DeletePackage(component,event);       
    }, */
    DeleteOption : function(component, event, helper) {
        helper.DeleteOption(component,event);       
    }, 
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
        //component.set("v.Spinner", true); 
       // component.set("v.newSpinner", true);
    },
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        //component.set("v.Spinner", false);console.log('In hideSpinner');
       //component.set("v.newSpinner", false);
    } 
})