({
    // this function is called on component load
    doinit : function(component, event, helper) { 
       
        // this function is called to get the signature types
        helper.SignatureType(component,event);
    },
    // this function is called to check validation for wet signature
    validations : function(component, event, helper) { 
        helper.ValidationsCheck(component,event);
    },
    // this function is called after file has uploaded
    handleUploadFinished : function(component, event, helper) {
        helper.UploadFinished(component,event);    
    },
    // this function automatic call by aura:waiting event  
   showSpinner: function(component, event, helper) {
       component.set("v.Spinner", true); 
   },
   // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    },
    openModel: function(component, event, helper) {
         var recid=component.get("v.recordId");
        console.log('recidsss :::'+recid);
        var recexists=component.get("c.AllowWetsignature");
        recexists.setParams({"QuoteId":component.get("v.recordId")});
        recexists.setCallback(this,function(response){
            if(response.getState()==="SUCCESS"){
                console.log('response.getReturnValue::::'+response.getReturnValue());
                if(response.getReturnValue()===false){
                    console.log('Hello');
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Both LOT and Model Should Exist",
                        "message": "To do wetsignature you need to configure both Lot and Model for Quote.",
                        "type": "error"
                    });
                    toastEvent.fire();
                     component.set("v.isOpen", false);
                }
                else{
                    console.log('v.isOpen true');
                     component.set("v.isOpen", true);
                }
                console.log('@@@@@');
            }
        });
        $A.enqueueAction(recexists);
     
   },
   closeModel: function(component, event, helper) {
      component.set("v.isOpen", false);
      component.set("v.ErrorMessage","");
      component.set("v.HasError","false");
   },
    // this function is to close the quick action
    Cancel : function(component,event,helper){
     	$A.get("e.force:closeQuickAction").fire();
        component.set("v.isOpen", false);
    } 
})