({
    getDivRecs : function(component, event, helper) {
        var action=component.get("c.getDivisions");
        action.setCallback(this,function(response){
            var state=response.getState();
            if(state === "SUCCESS"){               
                var csvString = '';
                var responseList = response.getReturnValue();
                for(var key in responseList){
                    if(key==0)
                        csvString+= responseList[key];
                    else
                        csvString+= ','+responseList[key];
                }
                component.set("v.division",csvString);
            }
            else if(state === "ERROR"){
                 var errors = response.getError();
                 console.log("errors" + errors[0].message); 
                if(errors[0].message=='No Divisions'){
                  var dismissActionPanel = $A.get("e.force:closeQuickAction"); 
                        dismissActionPanel.fire(); 
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Error!",
                            "type": 'Error',
                            "message": "You don't have access for any divisions to create Community of Interest. Please contact your Admin."
                        });
                        toastEvent.fire();        
                }
              
            }
        }),$A.enqueueAction(action);
    },
    getlookupids : function(component, event, helper){
        var objecttype = event.getParam("sobjectType");
        if(objecttype == 'Community__c'){
            var Community = event.getParam("lookuprecordId");
            var Communityidval = component.get('v.Communityid');
            component.set("v.Communityid", Community);
        }
        else if(objecttype == 'Lead'){
            var leadlookup = event.getParam("lookuprecordId");            
            var leadidval=component.get('v.leadid');
            component.set("v.leadid", leadlookup);    
        }
            else if(objecttype == 'Account'){
                var accountlookup = event.getParam("lookuprecordId");            
                var accountidval=component.get('v.accid');
                component.set("v.accid", accountlookup);    
            } 
    },
    saveRecs: function(component, event, helper) {
        var Communityid = component.get("v.Communityid");
        var leadid=component.get("v.leadid");
        var accountid=component.get("v.accid");
        if(Communityid != ''){
            var action=component.get("c.coi_insert");
            action.setParams({"leadId":leadid,"CommunityId":Communityid,"accountId":accountid});
            action.setCallback(this,function(response){
                var state=response.getState();
                if(state==="SUCCESS"){
                    var dismissActionPanel = $A.get("e.force:closeQuickAction"); 
                    dismissActionPanel.fire();               
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "type": 'success',
                        "message": "Community Interest has created successfully."
                    });
                    toastEvent.fire();
                    
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    console.log("errors" + errors);                
                    console.log("errors" + errors[0].message); 
                    if(errors[0].message=='Community Interest already exists')
                    {
                        var dismissActionPanel = $A.get("e.force:closeQuickAction"); 
                        dismissActionPanel.fire(); 
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Error!",
                            "type": 'Error',
                            "message": "Community Interest already exists"
                        });
                        toastEvent.fire();                        
                    }                    
                }
            }); 
            $A.enqueueAction(action);
        }
        else{
            var dismissActionPanel = $A.get("e.force:closeQuickAction"); 
            dismissActionPanel.fire(); 
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "type": 'Error',
                "message": "Community is required to create a Community of Interest"
            });
            toastEvent.fire();
            
        }
    },
    
})