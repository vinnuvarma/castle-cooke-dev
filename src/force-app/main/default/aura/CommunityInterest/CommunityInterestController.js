({
    
    doInit : function(component, event, helper) {
        helper.getDivRecs(component, event, helper);
    },
    handlelookupEvent :function(component, event, helper) {
        helper.getlookupids(component, event, helper); 
    },
    handleClick :function(component, event, helper) {
        helper.saveRecs(component, event, helper);
    }, 
    closeaction :function(component, event, helper) {
        var dismissActionPanel = $A.get("e.force:closeQuickAction"); 
        dismissActionPanel.fire(); 
    },
     // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
   },
    
 // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    }
})