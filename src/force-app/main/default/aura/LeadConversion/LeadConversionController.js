({
    //Loads the community interest records on load of component
    loadComponent : function(component, event, helper) {
        var leadId = component.get("v.recordId");
        var action = component.get("c.getCommunityInterests");
        action.setParams({"leadId":leadId});
        action.setCallback(this,function(response){
            var loadResponse = response.getReturnValue();
            if(loadResponse.length > 0){
                component.set('v.homeInterests',loadResponse);
            }
            else{
                helper.commonToast(component,event,helper,"Warning!","warning","You cannot convert a lead without an active Community of Interest.");
            	$A.get("e.force:closeQuickAction").fire();
            }
        });
        $A.enqueueAction(action);
    },
    
    // closes the quick action component
    closeQuickAction : function(component,event,helper){
        $A.get("e.force:closeQuickAction").fire();
    },
    
    //triggers the convertLeadInfo in apex class and converts the lead accordingly
    convertLead : function(component,event,helper){
        var leadId = component.get("v.recordId");
        var errMsg = component.find("showError");
        var selectedInterestVal = component.get("v.selectedHomeInterest");
        var dispSpinner = component.find("showSpinner");
        $A.util.addClass(dispSpinner, 'slds-show');
        $A.util.removeClass(dispSpinner, 'slds-hide');
        if($A.util.isEmpty(selectedInterestVal)){
            component.set("v.disableCreate",true);
            $A.util.addClass(dispSpinner, 'slds-hide');
            $A.util.removeClass(dispSpinner, 'slds-show');
        }
        else{
            var action = component.get("c.convertLeadInfo");
            action.setParams({"leadId":leadId,"selectedInterest":component.get("v.selectedHomeInterest"),
                              "interestList":component.get('v.homeInterests')});
            action.setCallback(this,function(response){
                var loadResponse = response.getReturnValue();
                if(loadResponse.isError){               
                    $A.util.addClass(errMsg, 'slds-show');
                    $A.util.removeClass(errMsg, 'slds-hide');
                    component.set("v.displayError",loadResponse.ackMsg);
                }
                else{
                    helper.commonToast(component,event,helper,"Success!","success","Lead is converted successfully.");
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": loadResponse.successId,
                        "slideDevName": "detail"
                    });
                    navEvt.fire();
                }
                $A.util.addClass(dispSpinner, 'slds-hide');
                $A.util.removeClass(dispSpinner, 'slds-show');
            });
            $A.enqueueAction(action);
        }
    },
    
    changeInterest : function(component,event,helper){
        var errMsg = component.find("showError");
        $A.util.addClass(errMsg, 'slds-hide');
        $A.util.removeClass(errMsg, 'slds-show');
        var selectedInterestVal = component.get("v.selectedHomeInterest");
        component.set("v.disableCreate",false);
        if($A.util.isEmpty(selectedInterestVal)){
            component.set("v.disableCreate",true);
        }
    }
})