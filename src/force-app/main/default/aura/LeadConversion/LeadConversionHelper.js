({
    //common toast method to avoid repeated code
    commonToast : function(component,event,helper,title,type,ackMsg){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type":type,
            "message": ackMsg
        });
        toastEvent.fire();
    }
})