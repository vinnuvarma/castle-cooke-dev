({
	fetchdata : function(component, event, helper) {
        var myid = component.get("v.recordId");
        
		var action = component.get("c.Incentivesfiltered");
        action.setParams({ "quoteid" : myid });
        action.setCallback(this,function(a){
                
                var state = a.getState();
                
               
                if(state == "SUCCESS"){
                    console.log('*****'+a.getReturnValue());
                    var result = JSON.stringify(a.getReturnValue());
                    console.log(result);
                    component.set("v.innerclassvalues",a.getReturnValue());
                } else {
                   
                    console.log(state);
                }
            });
            
                    
            $A.enqueueAction(action);
	},
     success : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Success Message',
            message: 'quote incentive has been created!',
            duration:'500',
            type: 'success',
        });
        toastEvent.fire();
       // alert('In  sucesses');
      //  $A.get('e.force:refreshView').fire();
     },
    Error : function(component, event, Message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error!",
            "type": "error",
            "message": Message,
        });
        toastEvent.fire();
    }
    
})