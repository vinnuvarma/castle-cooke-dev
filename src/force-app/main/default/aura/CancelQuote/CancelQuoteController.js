({
	doinit : function(component, event, helper) { 
        helper.quoteDetail(component,event);  
        helper.CancelTypes(component, event);
    },
    openModel: function(component, event, helper) {
      component.set("v.isOpen", true);
   },
   closeModel: function(component, event, helper) {
      component.set("v.isOpen", false);
   },
   CancelQuote: function(component, event, helper) {
      helper.CancelQuote(component,event); 
   },
    
})